#include <Application.h>

#include "calculator.h"

int main(int argc, char *argv[])
{
    Application app(argc,argv);
    app.setApplicationName("calculator");
    if ( !app.init() ) return 1;
    Calculator calc;
    return app.exec();
}
