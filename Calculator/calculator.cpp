#include <cmath>

#include <QKeyEvent>
#include <QMouseEvent>

#include "Platform.h"

#include "calculator.h"

Calculator::Calculator()
{
    // set font size
    QFont f(font());
    f.setPointSize(Platform::get()->getDefaultFontSize() * 3/2);
    setFont(f);

    setupUi(this);

    clearAll();

    show();
}

void Calculator::digitClicked()
{
    QPushButton *clickedButton = qobject_cast<QPushButton*>(sender());
    int digitValue = clickedButton->text().toInt();
    if (display->text() == "0" && digitValue == 0.0)
        return;

    if (waitingForOperand)
    {
        display->clear();
        waitingForOperand = false;
    }

    display->setText(display->text() + value_to_str(digitValue));
}

void Calculator::unaryOperatorClicked()
{
    QPushButton *clickedButton = qobject_cast<QPushButton *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();
    double result = 0.0;
    if (sender() == squareRootButton) {
        if (operand < 0.0) {
            abortOperation();
            return;
        }
        result = sqrt(operand);
    }
    else
    if (sender() == powerButton)
    {
        result = pow(operand, 2.0);
    }
    else
    if (sender() == reciprocalButton)
    {
        if ( 0.0 == operand ) {
            abortOperation();
            return;
        }
        result = 1.0 / operand;
    }
    display->setText(value_to_str(result));
    waitingForOperand = true;
}

void Calculator::additiveOperatorClicked()
{
    double operand = display->text().toDouble();

    if ( pendingMultiplicativeOperator != NULL ) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        display->setText(value_to_str(factorSoFar));
        operand = factorSoFar;
        factorSoFar = 0.0;
        pendingMultiplicativeOperator = NULL;
    }

    if ( pendingAdditiveOperator != NULL ) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
	    return;
        }
        display->setText(value_to_str(sumSoFar));
    } else {
        sumSoFar = operand;
    }

    pendingAdditiveOperator = sender();
    waitingForOperand = true;
}

void Calculator::multiplicativeOperatorClicked()
{
    double operand = display->text().toDouble();

    if ( pendingMultiplicativeOperator != NULL ) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        display->setText(value_to_str(factorSoFar));
    } else {
        factorSoFar = operand;
    }

    pendingMultiplicativeOperator = sender();
    waitingForOperand = true;
}

void Calculator::equalClicked()
{
    double operand = display->text().toDouble();

    if ( pendingMultiplicativeOperator != NULL ) {
        if (!calculate(operand, pendingMultiplicativeOperator)) {
            abortOperation();
	    return;
        }
        operand = factorSoFar;
        factorSoFar = 0.0;
        pendingMultiplicativeOperator = NULL;
    }
    if ( pendingAdditiveOperator != NULL ) {
        if (!calculate(operand, pendingAdditiveOperator)) {
            abortOperation();
	    return;
        }
        pendingAdditiveOperator = NULL;
    } else {
        sumSoFar = operand;
    }

    display->setText(value_to_str(sumSoFar));
    sumSoFar = 0.0;
    waitingForOperand = true;
}

void Calculator::pointClicked()
{
    if (waitingForOperand)
        display->setText("0");
    if (!display->text().contains("."))
        display->setText(display->text() + tr("."));
    waitingForOperand = false;
}

void Calculator::changeSignClicked()
{
    QString text = display->text();
    double value = text.toDouble();

    if (value > 0.0) {
        text.prepend(tr("-"));
    } else if (value < 0.0) {
        text.remove(0, 1);
    }
    display->setText(text);
}

void Calculator::backspaceClicked()
{
    if (waitingForOperand)
        return;

    QString text = display->text();
    text.chop(1);
    if (text.isEmpty()) {
        text = "0";
        waitingForOperand = true;
    }
    display->setText(text);
}

void Calculator::clearAll()
{
    sumSoFar = 0.0;
    factorSoFar = 0.0;
    pendingAdditiveOperator = NULL;
    pendingMultiplicativeOperator = NULL;
    display->setText("0");
    waitingForOperand = true;
}

void Calculator::clearMemory()
{
    sumInMemory = 0.0;
}

void Calculator::readMemory()
{
    display->setText(value_to_str(sumInMemory));
    waitingForOperand = true;
}

void Calculator::setMemory()
{
    equalClicked();
    sumInMemory = display->text().toDouble();
}

void Calculator::addToMemory()
{
    equalClicked();
    sumInMemory += display->text().toDouble();
}

QString Calculator::value_to_str( double val )
{
    return QString::number( val, 'g', 12 );
}

void Calculator::abortOperation()
{
    clearAll();
    display->setText(tr("####"));
}

bool Calculator::calculate(double rightOperand, QObject* pendingOperator)
{
    if (pendingOperator == plusButton)
    {
        sumSoFar += rightOperand;
    }
    else
    if (pendingOperator == minusButton)
    {
        sumSoFar -= rightOperand;
    }
    else
    if (pendingOperator == timesButton)
    {
        factorSoFar *= rightOperand;
    }
    else
    if (pendingOperator == divisionButton)
    {
        if (rightOperand == 0.0)
            return false;
        factorSoFar /= rightOperand;
    }
    return true;
}

QWidget* Calculator::keyToButton( int key ) const
{
    QWidget* w = NULL;
    switch ( key )
    {
    case Qt::Key_1:
    case Qt::Key_Q:
        w = digitButton1;
        break;
    case Qt::Key_2:
    case Qt::Key_W:
        w = digitButton2;
        break;
    case Qt::Key_3:
    case Qt::Key_E:
        w = digitButton3;
        break;
    case Qt::Key_4:
    case Qt::Key_R:
    case Qt::Key_A:
        w = digitButton4;
        break;
    case Qt::Key_5:
    case Qt::Key_T:
    case Qt::Key_S:
        w = digitButton5;
        break;
    case Qt::Key_6:
    case Qt::Key_Y:
    case Qt::Key_D:
        w = digitButton6;
        break;
    case Qt::Key_7:
    case Qt::Key_U:
    case Qt::Key_Z:
        w = digitButton7;
        break;
    case Qt::Key_8:
    case Qt::Key_I:
    case Qt::Key_X:
        w = digitButton8;
        break;
    case Qt::Key_9:
    case Qt::Key_O:
    case Qt::Key_C:
        w = digitButton9;
        break;
    case Qt::Key_0:
    case Qt::Key_P:
    case Qt::Key_Space:
        w = digitButton0;
        break;
    default:
        w = NULL;
        break;
    }
    return w;
}

void Calculator::keyPressEvent(QKeyEvent* e)
{
    QWidget* w = keyToButton( e->key() );
    if ( w )
    {
        w->setFocus();
        QCoreApplication::postEvent( w, new QMouseEvent( QEvent::MouseButtonPress, QPoint(0,0), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier ) );
    }
}

void Calculator::keyReleaseEvent(QKeyEvent* e)
{
    QWidget* w = keyToButton( e->key() );
    if ( w )
    {
        w->setFocus();
        QCoreApplication::postEvent( w, new QMouseEvent( QEvent::MouseButtonRelease, QPoint(0,0), Qt::LeftButton, Qt::NoButton, Qt::NoModifier ) );
    }
    else
    {
        switch ( e->key() )
        {
        case Qt::Key_Escape:
        case Qt::Key_Back:
            close();
            break;
        default:
            break;
       }
    }
}
