#!/bin/sh

echo "suspend.sh: enter"

handler()
{
    kill $sleep
    echo "suspend.sh: killed"
    # kindle voyage: enable keys
    lipc-set-prop -q com.lab126.deviced fsrkeypadEnable 1
    exit 1
}

# kindle voyage: disable keys
lipc-set-prop -q com.lab126.deviced fsrkeypadEnable 0

trap handler SIGTERM

sleep 10 & sleep=$!; wait

if [ $? -eq 0 ]; then
    echo "sleeping... zzz-zzz-zzz-zzz :)"
    sync
    echo mem > /sys/power/state
    # kindle voyage: enable keys
    lipc-set-prop -q com.lab126.deviced fsrkeypadEnable 1
    echo "suspend.sh: normal exit"
else
    echo "suspend.sh: killed"
    exit 1
fi
