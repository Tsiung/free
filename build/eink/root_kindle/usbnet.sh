#!/bin/sh
driver_root=/lib/modules/3.0.35-lab126/kernel/drivers/usb/gadget
if [ ! -f $TMPDIR/dropbear_usb ]
then
    rmmod g_file_storage
    insmod $driver_root/g_ether.ko host_addr=46:0d:9e:67:69:e0 dev_addr=46:0d:9e:67:69:e1
    ifconfig usb0 192.168.2.101
    $ROOT/dropbear_on.sh
    touch $TMPDIR/dropbear_usb
    msgbox/msgbox -stylesheet eink.qss "usbnet" "usb network started!"
else
    rmmod g_ether
    insmod $driver_root/g_file_storage.ko
    rm -rf $TMPDIR/dropbear_usb
    $ROOT/dropbear_off.sh
    msgbox/msgbox -stylesheet eink.qss "usbnet" "usb network stopped!"
fi
