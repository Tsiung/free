#!/bin/sh

export ROOT=/mnt/us/vlasovsoft
LOG=/dev/null

# Uncomment to see log
# LOG=$ROOT/vlasovsoft.log.txt
# Uncomment to see debug from screen
# QWS_SCREEN_DEBUG=:debug
# Uncomment to see debug from mouse
# QWS_MOUSE_DEBUG=:debug
# Uncomment to see debug from keys
# QWS_KEYS_DEBUG=:debug

if [ "$(id -u)" != "0" ]; then 
    /var/local/mkk/su -s /bin/ash -c "$ROOT/launcher.sh 1 > $LOG 2>&1 &"
    exit
elif [ "$1" != "1" ]; then
    $ROOT/launcher.sh 1 > $LOG 2>&1 &
    exit
fi

QT=$ROOT/Qt

cd $ROOT

export LANG=en_US.UTF-8 # for launchpad
export TMPDIR=/tmp/vlasovsoft

export LD_LIBRARY_PATH=$QT/lib
export QT_PLUGIN_PATH=$QT/plugins
export QT_QWS_FONTDIR=$ROOT/fonts

export VLASOVSOFT_KEY=$ROOT/key
export VLASOVSOFT_KBD=$ROOT/kbd.txt
export VLASOVSOFT_FIFO1=$TMPDIR/fifo1
export VLASOVSOFT_FIFO2=$TMPDIR/fifo2
export VLASOVSOFT_I18N=$ROOT/i18n
export VLASOVSOFT_DICT=$ROOT/dictionary

export STYLESHEET=$ROOT/eink.qss

. $ROOT/settings.sh

model="Unknown"
version=`cat VERSION`
serial=`cat /proc/usid`
lead=`echo $serial | cut -c1-1`

USE_SUSPEND_MANAGER="1"
QWS_DISPLAY=transformed:KindleFb${QWS_SCREEN_DEBUG}
QWS_MOUSE_PROTO=KindleTS${QWS_MOUSE_DEBUG}
QWS_KEYBOARD=KindleKeys${QWS_KEYS_DEBUG}

case $lead in
B|9)
    case `echo $serial | cut -c3-4` in
        0F|11|10|12) 
            model="KT"
            ;;
        24|1B|1D|1F|1C|20) 
            model="KPW"
            ;;
        D4|5A|D5|D6|D7|D8|F2|17|60|F4|F9|62|61|5F) 
            model="KPW2"
            ;;
        C6|DD) 
            model="KT2"
            ;;
        13|54|2A|4F|52|53)
            model="KV"
            ;;
    esac ;;
*)
    case `echo $serial | cut -c4-6` in
        0G1|0G2|0G4|0G5|0G6|0G7|0KB|0KC|0KD|0KE|0KF|0KG|0LK|0LL) 
            model="KPW3"
            USE_SUSPEND_MANAGER="0"
            QWS_MOUSE_PROTO=KindleTSv2${QWS_MOUSE_DEBUG}
            ;;
        0GC|0GD|0GR|0GS|0GT|0GU)
            model="KOA"
            ;;
        0LM|0LN|0LP|0LQ|0P1|0P2|0P6|0P7|0P8|0S1|0S2|0S3|0S4|0S7|0SA)
            model="KOA2"
            USE_SUSPEND_MANAGER="0"
            ;;
        0DU|0K9|0KA)
            model="KT3"
            ;;
        0PP|0T1|0T2|0T3|0T4|0T5|0T6|0T7|0TJ|0TK|0TL|0TM|0TN|102|103|16Q|16R|16S|16T|16U|16V)
            model="KPW4"
            USE_SUSPEND_MANAGER="0"
            QWS_MOUSE_PROTO=KindleTSv2${QWS_MOUSE_DEBUG}
            ;;
        10L|0WF|0WG|0WH|0WJ|0VB)
            model="KT4"
            QWS_MOUSE_PROTO=KindleTSv2${QWS_MOUSE_DEBUG}
            ;;
    esac ;;
esac

if [ $model == "Unknown" ]; then
    echo "Unknown model! Exit."
    exit 1;
fi

export DEVICE=$model
export USE_SUSPEND_MANAGER
export QWS_DISPLAY
export QWS_MOUSE_PROTO
export QWS_KEYBOARD

echo "Serial: $serial"
echo "Model: $model"
echo "Version: $version"

rm -rf $TMPDIR
mkdir -p $TMPDIR
mkfifo $VLASOVSOFT_FIFO1
mkfifo $VLASOVSOFT_FIFO2

if [ $USE_SUSPEND_MANAGER == "1" ]; then
    echo "prevent kindle screensaver activation"
    lipc-set-prop com.lab126.powerd preventScreenSaver 1
fi

# switch to the standard screensaver and disable the ads screensaver (with swipe to unlock)
source /etc/upstart/functions
source /etc/upstart/blanket_functions
f_blanket_unload_module ad_screensaver
f_blanket_unload_module ad_screensaver_active
f_blanket_load_module screensaver

# copy scripts to temp folder so that they work when USB is connected and /mnt/us is unmounted
cp -f $ROOT/ktsuspend.sh $ROOT/ktresume.sh $ROOT/crfswin $ROOT/usleep /var/tmp/

/var/tmp/ktsuspend.sh

lipc-set-prop com.lab126.appmgrd start app://com.lab126.booklet.home
    
lipc-set-prop -q com.lab126.deviced fsrkeypadEnable 1 && lipc-set-prop -q com.lab126.deviced fsrkeypadPrevEnable 1 && lipc-set-prop -q com.lab126.deviced fsrkeypadNextEnable 1
    
$ROOT/launcher -qws -stylesheet $STYLESHEET
       
/var/tmp/ktresume.sh    
lipc-set-prop -q com.lab126.deviced fsrkeypadEnable 0 && lipc-set-prop -q com.lab126.deviced fsrkeypadPrevEnable 0 && lipc-set-prop -q com.lab126.deviced fsrkeypadNextEnable 0

if [ $USE_SUSPEND_MANAGER == "1" ]; then
    echo "restore kindle screensaver activation"
    lipc-set-prop com.lab126.powerd preventScreenSaver 0
fi
    
rm $VLASOVSOFT_FIFO1
rm $VLASOVSOFT_FIFO2

$ROOT/upgrade_batch.sh

