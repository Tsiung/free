#!/bin/sh
if lsmod | grep -q sdio_wifi_pwr ; then
    wlarm_le -i $INTERFACE down
    ifconfig $INTERFACE down
    /sbin/rmmod -r $WIFI_MODULE
    /sbin/rmmod -r sdio_wifi_pwr
fi

