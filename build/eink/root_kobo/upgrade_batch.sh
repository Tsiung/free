#!/bin/sh

echo "upgrade_batch.sh started..."

dirCurr=`pwd`
cd $ROOT/..

dirUpg=`pwd`/vlasovsoft.upgrade
dirNew=`pwd`/vlasovsoft.new
if [ -d $dirUpg/.adds/vlasovsoft ]; then

    # ==== remove cr3 cache

    echo "Remove cr3 cache"
    rm -rf $ROOT/cr3/data/cache

    # ==== backup current installation

    echo "Backup current installation"
    cp -r $ROOT $dirNew

    # ==== remove blacklist files and directories

    echo "Remove blacklist files"
    if [ -f $ROOT/upgrade/blacklist.txt ]; then
        for file in `cat $ROOT/upgrade/blacklist.txt`; do
            rm -rf $dirUpg/.adds/vlasovsoft/$file
        done
    fi

    # ==== upgrade

    echo "Upgrading"
    cp -r $dirUpg/.adds/vlasovsoft/* $dirNew

    # ==== remove

    echo "Cleanup"
    rm -rf $dirUpg

elif [ -d $dirNew  ]; then

    ver=`cat $ROOT/VERSION`
    dirBak=`pwd`/vlasovsoft_${ver}_bak

    echo "Rename $ROOT to $dirBak"
    mv $ROOT $dirBak

    echo "Rename $dirNew to $ROOT"
    mv $dirNew $ROOT
fi

cd $dirCurr

echo "upgrade_batch.sh finished."

