#-------------------------------------------------
#
# Project created by QtCreator 2018-03-09T11:21:40
#
#-------------------------------------------------

QT       += core gui network

include(../qws_common/qws_common.pri)
include(../../common/common.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qws_server
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp \
    window.cpp \
    inputpanel.cpp

HEADERS += \
        widget.h \
    window.h \
    inputpanel.h

