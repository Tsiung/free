#include <QPaintEvent>
#include <QMouseEvent>
#include <QLocalSocket>
#include <QPainter>
#include <QDateTime>
#include <QDebug>

#include "qws_proto.h"

#include "window.h"

Window::Window(QWidget* parent, QLocalSocket *s, QWSScreenData* d, void* id)
    : QWidget(parent)
    , sock_(s)
    , data_(d)
    , id_(id)
    , shmSeq(0)
{
}

void Window::setClientGeometry(const QRect& r)
{
    clientGeom = r;
    QRect servGeom(clientToScreen(parentWidget()->size(), data_->getRotation(), clientGeom));
    setGeometry(servGeom);
}

void Window::updateGeometry()
{
    QRect servGeom = clientToScreen(parentWidget()->size(), data_->getRotation(), clientGeom);
    setGeometry(servGeom);
    update();
}

void Window::attachSharedMemory(const QSize& s, quint64 pid, int seq)
{
    if ( seq > shmSeq )
    {
        if ( shm.isAttached() ) shm.detach();
        shmSeq = seq;
        shm.setKey( shm_key( pid, id_, shmSeq ) );
        shm.attach(QSharedMemory::ReadOnly);
    }

    QImage imgNew((const uchar*)shm.data(),s.width(),s.height(),QWS_SCREEN_FORMAT);
    img.swap(imgNew);
}

void Window::paintEvent(QPaintEvent*)
{
    QPainter p(this);
    p.save();
    switch( data_->getRotation() )
    {
    case 90:
        p.translate(0,img.width());
        p.rotate(-90);
        break;
    case 180:
        p.translate(img.width(),img.height());
        p.rotate(180);
        break;
    case 270:
        p.translate(img.height(),0);
        p.rotate(90);
        break;
    }
    {
        ShmLocker lock(&shm);
        p.drawImage(0,0,img);
    }
    p.restore();
    if ( size() != parentWidget()->size() ) // not full screen
    {
        p.drawRect(rect().adjusted(0,0,-1,-1));
    }
}

void Window::mousePressEvent(QMouseEvent* e)
{
    sendMouseEvent( screenToClient(size(), data_->getRotation(), e->pos()),
                    screenToClient(parentWidget()->size(), data_->getRotation(), mapToParent(e->pos())),
                    e->buttons() );
}

void Window::mouseReleaseEvent(QMouseEvent* e)
{
    sendMouseEvent( screenToClient(size(), data_->getRotation(), e->pos()),
                    screenToClient(parentWidget()->size(), data_->getRotation(), mapToParent(e->pos())),
                    e->buttons() );
}

void Window::mouseMoveEvent(QMouseEvent* e)
{
    sendMouseEvent( screenToClient(size(), data_->getRotation(), e->pos()),
                    screenToClient(parentWidget()->size(), data_->getRotation(), mapToParent(e->pos())),
                    e->buttons() );
}

void Window::sendMouseEvent(const QPoint& pt, const QPoint& pg, int buttons)
{
    int cmd = qwsMouseEvent;
    sock_write(sock_, &cmd, sizeof(cmd));
    sock_write(sock_, &id_, sizeof(id_));
    sock_write(sock_, &buttons, sizeof(int));
    sock_write(sock_, &pt, sizeof(QPoint));
    sock_write(sock_, &pg, sizeof(QPoint));
    sock_->flush();
}

void Window::sendKeyEvent(QEvent::Type t, int key, Qt::KeyboardModifier mod, ushort ch)
{
    int cmd = qwsKeyEvent;
    sock_write(sock_, &cmd, sizeof(cmd));
    sock_write(sock_, &id_, sizeof(id_));
    sock_write(sock_, &t, sizeof(t));
    sock_write(sock_, &key, sizeof(key));
    sock_write(sock_, &mod, sizeof(mod));
    sock_write(sock_, &ch, sizeof(ch));
    sock_->flush();
}

void Window::sendKeyEvent(QEvent::Type t, int key, Qt::KeyboardModifiers mods, const QString& str)
{
    Qt::KeyboardModifier mod = static_cast<Qt::KeyboardModifier>(static_cast<Qt::KeyboardModifiers::Int>(mods));
    ushort ch = str.isEmpty()? 0 : str.at(0).unicode();
    sendKeyEvent( t, key, mod, ch );
}
