#ifndef WINDOW_H
#define WINDOW_H

#include <QRect>
#include <QWidget>
#include <QSharedMemory>
#include <QImage>

#include "qws_proto.h"

class QLocalSocket;

class Window: public QWidget
{
    friend class InputPanel;
    friend class Widget;

Q_OBJECT

    QLocalSocket* sock_;
    QWSScreenData* data_;
    void* id_;
    QSharedMemory shm;
    QImage img;
    QRect clientGeom;
    int shmSeq;

public:
    Window(QWidget* parent, QLocalSocket* s, QWSScreenData* d, void* id);
    void* getId() const { return id_; }
    QLocalSocket* getSocket() const { return sock_; }
    void setClientGeometry(const QRect&);
    void updateGeometry();
    void attachSharedMemory( const QSize& s, quint64 pid, int seq );

protected:
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);

private:
    void sendMouseEvent( const QPoint& pt, const QPoint& pg, int buttons );
    void sendKeyEvent( QEvent::Type t, int key, Qt::KeyboardModifier mod, ushort ch );
    void sendKeyEvent( QEvent::Type t, int key, Qt::KeyboardModifiers mods, const QString& str );
};

#endif // WINDOW_H
