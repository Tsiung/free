#include <QScreen>
#include <QGuiApplication>
#include <QDebug>

#include "qwsscreen.h"

QWSScreen::QWSScreen()
    : data(false)
{
}

QWSScreen::~QWSScreen()
{
}

QRect QWSScreen::geometry() const
{
    return QRect(QPoint(0,0),data.getSize());
}

Qt::ScreenOrientation QWSScreen::orientation() const
{
    return Qt::PortraitOrientation;
    int rotation = data.getRotation();
    switch( rotation )
    {
    case 0:
        return Qt::PortraitOrientation;
    case 1:
        return Qt::LandscapeOrientation;
    case 2:
        return Qt::InvertedPortraitOrientation;
    case 3:
        return Qt::InvertedLandscapeOrientation;
    }
    return Qt::PrimaryOrientation;
}

void QWSScreen::geometryChanged(const QRect &)
{
    QList<QWindow*> windows = QGuiApplication::allWindows();
    for (int i = 0; i < windows.size(); ++i) {
        QWindow *w = windows.at(i);

        // Skip non-platform windows, e.g., offscreen windows.
        if ( !w->handle() )
            continue;

        if ( !w->screen() || w->screen()->handle() != this )
            continue;

        if ( w->windowState() & Qt::WindowFullScreen )
            w->setGeometry(w->screen()->handle()->geometry() );
     }
}
