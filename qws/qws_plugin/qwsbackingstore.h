#ifndef QBACKINGSTORE_QWS_H
#define QBACKINGSTORE_QWS_H

#include <qpa/qplatformbackingstore.h>
#include <qpa/qplatformwindow.h>

#include <QImage>
#include <QLocalSocket>
#include <QSharedMemory>

class QWSBackingStore : public QPlatformBackingStore
{
public:
    QWSBackingStore(QWindow *window, QLocalSocket* s);
    ~QWSBackingStore();

    QPaintDevice* paintDevice() Q_DECL_OVERRIDE;
    void flush(QWindow *window, const QRegion &region, const QPoint &offset) Q_DECL_OVERRIDE;
    void resize(const QSize &size, const QRegion &staticContents) Q_DECL_OVERRIDE;

    void beginPaint(const QRegion &);
    void endPaint();

private:
    QLocalSocket* sock;
    QImage img;
    QSharedMemory shm;
    int shmSeq;
};

#endif
