#include "qwsintegration.h"
#include "qwsbackingstore.h"
#include "qwsplatformwindow.h"
#include "qwsplatforminputcontext.h"
#include "qwsscreen.h"
#include "qws_proto.h"

#include <QtGui/private/qpixmap_raster_p.h>
#include <QtGui/private/qguiapplication_p.h>

#include <QtFontDatabaseSupport/private/qfreetypefontdatabase_p.h>
#include <QtEventDispatcherSupport/private/qgenericunixeventdispatcher_p.h>

#include <qpa/qplatformwindow.h>

QT_BEGIN_NAMESPACE

class QCoreTextFontEngine;

static inline unsigned parseOptions(const QStringList&)
{
    return 0;
}

QWSIntegration::QWSIntegration(const QStringList &parameters)
    : m_options(parseOptions(parameters))
    , input(NULL)
    , fdb(NULL)
    , ict(NULL)
    , screen(NULL)
{
    screenAdded(screen = new QWSScreen());
    QObject::connect( QGuiApplication::primaryScreen(), SIGNAL(geometryChanged(QRect)), screen, SLOT(geometryChanged(QRect)) );
}

QWSIntegration::~QWSIntegration()
{
    delete fdb;
    delete input;
    destroyScreen(screen);
}

bool QWSIntegration::hasCapability(QPlatformIntegration::Capability cap) const
{
    switch (cap) {
    case ThreadedPixmaps: return true;
    case MultipleWindows: return true;
    case NonFullScreenWindows: return true;
    default: return QPlatformIntegration::hasCapability(cap);
    }
}

QPlatformFontDatabase* QWSIntegration::fontDatabase() const
{
    if (!fdb) {
        fdb = new QFreeTypeFontDatabase;
    }
    return fdb;
}

QPlatformWindow *QWSIntegration::createPlatformWindow(QWindow *window) const
{
    Q_UNUSED(window);

    if ( input == NULL ) {
        input = new QWSInput();
    }

    QPlatformWindow *w = new QWSPlatformWindow(window, input->socket());
    w->requestActivateWindow();
    return w;
}

QPlatformBackingStore *QWSIntegration::createPlatformBackingStore(QWindow *window) const
{
    return new QWSBackingStore(window, input->socket());
}

QAbstractEventDispatcher *QWSIntegration::createEventDispatcher() const
{
    return createUnixEventDispatcher();
}

QPlatformInputContext *QWSIntegration::inputContext() const
{
    qDebug() << "QWSIntegration::inputContext";
    if (!ict) {
        ict = new QWSPlatformInputContext(input->socket());
    }
    return ict;
}

QWSIntegration *QWSIntegration::instance()
{
    return static_cast<QWSIntegration *>(QGuiApplicationPrivate::platformIntegration());
}

QT_END_NAMESPACE
