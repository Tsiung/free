#ifndef QWSPLATFORMWINDOW_H
#define QWSPLATFORMWINDOW_H

#include <qpa/qplatformwindow.h>

class QWindow;
class QLocalSocket;

class QWSPlatformWindow : public QPlatformWindow
{
    QLocalSocket* sock;

public:
    QWSPlatformWindow(QWindow* w, QLocalSocket* s);
    ~QWSPlatformWindow();

    void setVisible(bool visible);
    void setGeometry(const QRect& rect);
    void setWindowState(Qt::WindowState state);
    void raise();
    void lower();
    void setParent(const QPlatformWindow*);
    void requestActivateWindow();
};

#endif // QWSPLATFORMWINDOW_H
