#ifndef QWSSCREEN_H
#define QWSSCREEN_H

#include <qpa/qplatformscreen.h>

#include <QtCore/QSharedMemory>

#include "qws_proto.h"

class QWSScreen : public QObject, public QPlatformScreen
{
Q_OBJECT
    QWSScreenData data;

public:
    QWSScreen();
    ~QWSScreen();
    QRect geometry() const Q_DECL_OVERRIDE;
    int depth() const Q_DECL_OVERRIDE { return 32; }
    QImage::Format format() const Q_DECL_OVERRIDE { return QImage::Format_ARGB32_Premultiplied; }
    Qt::ScreenOrientation orientation() const;

private slots:
    void geometryChanged(const QRect&);

};

#endif // QWSSCREEN_H
