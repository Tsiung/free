#ifndef QPLATFORMINTEGRATION_QWS_H
#define QPLATFORMINTEGRATION_QWS_H

#include <qpa/qplatformintegration.h>

#include <QtCore/QObject>

#include <QLocalSocket>
#include <QSharedMemory>

#include "qwsinput.h"
#include "qws_proto.h"

class QWSScreen;
class QWSPlatformInputContext;

class QWSIntegration : public QObject, public QPlatformIntegration
{
    Q_OBJECT

public:
    QWSIntegration(const QStringList &parameters);
    ~QWSIntegration();

    bool hasCapability(QPlatformIntegration::Capability cap) const Q_DECL_OVERRIDE;
    QPlatformFontDatabase *fontDatabase() const Q_DECL_OVERRIDE;

    QPlatformWindow *createPlatformWindow(QWindow *window) const Q_DECL_OVERRIDE;
    QPlatformBackingStore *createPlatformBackingStore(QWindow *window) const Q_DECL_OVERRIDE;
    QAbstractEventDispatcher *createEventDispatcher() const Q_DECL_OVERRIDE;
    QPlatformInputContext *inputContext() const Q_DECL_OVERRIDE;

    unsigned options() const { return m_options; }

    static QWSIntegration *instance();

private:
    unsigned m_options;
    mutable QWSInput* input;
    mutable QPlatformFontDatabase* fdb;
    mutable QPlatformInputContext* ict;
    QWSScreen* screen;
};

#endif
