#include <QByteArray>
#include <QDataStream>
#include <QLocalSocket>
#include <QGuiApplication>
#include <QWindow>
#include <QScreen>
#include <QDebug>

#include <qpa/qplatformscreen.h>
#include <qpa/qwindowsysteminterface.h>

#include "qws_proto.h"

#include "qwsplatformwindow.h"

QWSPlatformWindow::QWSPlatformWindow(QWindow *w, QLocalSocket* s)
    : QPlatformWindow(w)
    , sock(s)
{
    int cmd = qwsWindowCreate;
    void* id = w;

    QRect r(geometry());

    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
    sock->flush();

    cmd = qwsWindowGeometry;

    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
    ::sock_write(sock, &r, sizeof(QRect));
    sock->flush();

    qDebug() << "cratePlatformWindow(" << w << ")";
}

QWSPlatformWindow::~QWSPlatformWindow()
{
    int cmd = qwsWindowDestroy;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
    sock->flush();

    qDebug() << "destroyPlatformWindow(" << window() << ")";
}

void QWSPlatformWindow::setVisible(bool visible)
{
    QPlatformWindow::setVisible(visible);
    int cmd = qwsWindowVisible;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
    ::sock_write(sock, &visible, sizeof(bool));
    sock->flush();
    if ( visible && ( window()->windowState() & Qt::WindowFullScreen ) )
    {
        setGeometry(screen()->geometry());
    }
    if ( !visible && window()->isActive() )
    {
        // Current activated window is invisible, so find and activate
        // the visible window
        const QWindowList list = QGuiApplication::topLevelWindows();
        QWindowList::const_reverse_iterator i(list.rbegin());
        while ( i != list.rend() )
        {
            const QWindow* w = *i;
            if ( w->isVisible() && w->isTopLevel() )
            {
                (*i)->handle()->requestActivateWindow();
                break;
            }
            else
            {
                i++;
            }
        }
    }
}

void QWSPlatformWindow::setGeometry(const QRect &rect)
{
    QPlatformWindow::setGeometry(rect);
    int cmd = qwsWindowGeometry;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(cmd));
    ::sock_write(sock, &id, sizeof(id));
    ::sock_write(sock, &rect, sizeof(rect));
    sock->flush();
    QWindowSystemInterface::handleGeometryChange(window(), rect);

    qDebug() << "==== setGeometry(" << window() << "," << rect << ")";
}

void QWSPlatformWindow::setWindowState(Qt::WindowState state)
{
    QPlatformWindow::setWindowState(state);
    int cmd = qwsWindowState;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
    ::sock_write(sock, &state, sizeof(Qt::WindowState));
    sock->flush();

    qDebug() << "setWindowState(" << window() << "," << state << ")";
}

void QWSPlatformWindow::raise()
{
    int cmd = qwsWindowRaise;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
}

void QWSPlatformWindow::lower()
{
    int cmd = qwsWindowLower;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(int));
    ::sock_write(sock, &id, sizeof(void*));
}

void QWSPlatformWindow::setParent(const QPlatformWindow *)
{
    qDebug() << "setParent!";
}

void QWSPlatformWindow::requestActivateWindow()
{
    QPlatformWindow::requestActivateWindow();
    int cmd = qwsWindowActivate;
    void* id = window();
    ::sock_write(sock, &cmd, sizeof(cmd));
    ::sock_write(sock, &id, sizeof(id));
}
