#ifndef QWSPLATFORMINPUTCONTEXT_H
#define QWSPLATFORMINPUTCONTEXT_H

#include <qpa/qplatforminputcontext.h>

class QLocalSocket;

class QWSPlatformInputContext : public QPlatformInputContext
{
    QLocalSocket* sock;
    bool visible;

public:
    QWSPlatformInputContext(QLocalSocket* s);

    void showInputPanel();
    void hideInputPanel();
    bool isInputPanelVisible() const { return visible; }
    void setFocusObject(QObject *object);
    bool hasCapability(Capability) const { return false; }

private:
    void sendShowInputPanel(bool show);

};

#endif // QWSPLATFORMINPUTCONTEXT_H
