#include <QLocalSocket>
#include <QDebug>

#include "qwsplatforminputcontext.h"

#include "qws_proto.h"

QWSPlatformInputContext::QWSPlatformInputContext(QLocalSocket* s)
    : sock(s)
    , visible(false)
{
}

void QWSPlatformInputContext::showInputPanel()
{
    qDebug() << "QWSPlatformInputContext::showInputPanel()";
    sendShowInputPanel(true);
    visible = true;
}

void QWSPlatformInputContext::hideInputPanel()
{
    qDebug() << "QWSPlatformInputContext::hideInputPanel()";
    sendShowInputPanel(false);
    visible = false;
}

void QWSPlatformInputContext::setFocusObject(QObject *object)
{
    QPlatformInputContext::setFocusObject(object);
    qDebug() << "QWSPlatformInputContext::setFocusObject(" << object << ")";
}

void QWSPlatformInputContext::sendShowInputPanel(bool show)
{
    visible = show;
    int cmd = qwsInputPanel;
    ::sock_write(sock, &cmd, sizeof(cmd));
    ::sock_write(sock, &show, sizeof(show));
    sock->flush();
}
