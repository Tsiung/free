TARGET = qws

TEMPLATE = lib
CONFIG += plugin

QT += \
    core-private gui-private network \
    eventdispatcher_support-private fontdatabase_support-private

SOURCES =   main.cpp \
            qwsintegration.cpp \
            qwsbackingstore.cpp \
    qwsplatformwindow.cpp \
    qwsinput.cpp \
    qwsscreen.cpp \
    qwsplatforminputcontext.cpp

HEADERS =   qwsintegration.h \
            qwsbackingstore.h \
    qwsplatformwindow.h \
    qwsinput.h \
    qwsscreen.h \
    qwsplatforminputcontext.h

include(../qws_common/qws_common.pri)

OTHER_FILES += qws.json

PLUGIN_TYPE = platforms
PLUGIN_CLASS_NAME = QWSIntegrationPlugin
