#ifndef QWSINPUT_H
#define QWSINPUT_H

#include <QObject>
#include <QLocalSocket>
#include <QTimer>

class QWSInput : public QObject
{
Q_OBJECT
    QLocalSocket* sock;

public:
    QWSInput();

    QLocalSocket* socket() { return sock; }

private slots:
    void readyRead();
    void disconnect();

signals:
    void rotation();
};

#endif // QWSINPUT_H
