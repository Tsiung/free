#include <QScreen>
#include <QThread>
#include <QDateTime>
#include <QCoreApplication>

#include <QDebug>

#include <qpa/qplatformscreen.h>

#include "qwsbackingstore.h"
#include "qwsintegration.h"
#include "qws_proto.h"

QWSBackingStore::QWSBackingStore(QWindow *window, QLocalSocket* s)
    : QPlatformBackingStore(window)
    , sock(s)
    , shmSeq(0)
{
}

QWSBackingStore::~QWSBackingStore()
{
}

QPaintDevice *QWSBackingStore::paintDevice()
{
    return &img;
}

void QWSBackingStore::flush(QWindow *window, const QRegion &region, const QPoint &offset)
{
    qDebug() << "==== QWSBackingStore::flush ";

    Q_UNUSED(offset)
    for ( int i=0 ; i<region.rectCount(); ++i  )
    {
        QRect r(region.rects()[i]);
        int cmd = qwsWindowFlush;
        void* id = window;
        ::sock_write(sock, &cmd, sizeof(int));
        ::sock_write(sock, &id, sizeof(void*));
        ::sock_write(sock, &r, sizeof(QRect));
        sock->flush();
    }
}

void QWSBackingStore::resize(const QSize& size, const QRegion&)
{    
    qDebug() << "==== QWSBackingStore::resize " << size;
    if ( img.size() != size )
    {
        qint64 pid = QCoreApplication::applicationPid();
        if ( img.size() != size.transposed() )
        {
            if ( shm.isAttached() )
            {
                shm.detach();
            }
            shm.setKey( shm_key( pid, window(), ++shmSeq ) );
            shm.create( 4*size.width()*size.height() );
        }

        if ( shm.isAttached() )
        {
            QImage imgNew((uchar*)shm.data(), size.width(), size.height(), QWS_SCREEN_FORMAT );
            img.swap(imgNew);

            int cmd = qwsBackingStore;
            void* id = window();
            ::sock_write(sock, &cmd, sizeof(int));
            ::sock_write(sock, &id, sizeof(void*));
            ::sock_write(sock, &pid, sizeof(pid));
            ::sock_write(sock, &shmSeq, sizeof(shmSeq));
            ::sock_write(sock, &size, sizeof(size));
            sock->flush();
        }
        else
        {
            QImage imgNew;
            img.swap(imgNew);

            QCoreApplication::quit();
        }
    }
}

void QWSBackingStore::beginPaint(const QRegion&)
{
    qDebug() << "==== QWSBackingStore::beginPaint ";
    shm.lock();
}

void QWSBackingStore::endPaint()
{
    qDebug() << "==== QWSBackingStore::endPaint ";
    shm.unlock();
}
