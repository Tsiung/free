#ifndef QWS_PROTO_H
#define QWS_PROTO_H

#include <QWindow>
#include <QSharedMemory>

#define QWS_SERVER "vlasovsoft_qws"
#define QWS_SHM    "vlasovsoft_qws_shm"
#define QWS_WIDTH 500
#define QWS_HEIGHT 600
#define QWS_SCREEN_FORMAT QImage::Format_ARGB32_Premultiplied

class QLocalSocket;

enum QWSCommand {
    qwsNone,
    qwsWindowCreate,     // client->server
    qwsWindowDestroy,    // client->server
    qwsWindowVisible,    // client->server
    qwsWindowGeometry,   // client->server
    qwsWindowState,      // client->server
    qwsWindowFlush,      // client->server    
    qwsWindowRaise,      // client->server
    qwsWindowLower,      // client->server
    qwsWindowActivate,   // client->server
    qwsBackingStore,     // client->server
    qwsInputPanel,       // client->server
    qwsMouseEvent,       // server->client
    qwsKeyEvent,         // server->client
    qwsRotationEvent     // server->client
};

class ShmLocker
{
    QSharedMemory* shm;

public:
    ShmLocker( QSharedMemory* s )
        : shm(s)
    {
        shm->lock();
    }
    ~ShmLocker()
    {
        shm->unlock();
    }
};

struct QWSScreenInfo {
    int width;
    int height;
    int rotation;
};

class QWSScreenData {
    mutable QSharedMemory shm;
    QWSScreenInfo* data;

public:
    QWSScreenData( bool server );
    ~QWSScreenData();
    QSize getSize() const;
    void setSize( const QSize size );
    int getRotation() const;
    void setRotation( int val );
};

QString shm_key( quint64 pid, void* id, int seq );
void sock_read( QLocalSocket* sock, void* data, int size );
void sock_write( QLocalSocket* sock, const void* data, int size  );

QPoint clientToScreen( const QSize& s, int rot, const QPoint& pt );
QRect clientToScreen( const QSize& s, int rot, const QRect& r );

QPoint screenToClient( const QSize& s, int rot, const QPoint& pt );
QRect screenToClient( const QSize& s, int rot, const QRect& r );

#endif // QWS_PROTO_H
