#include <QLocalSocket>

#include "qws_proto.h"

void sock_read( QLocalSocket* sock, void* data, int size )
{
    if ( QLocalSocket::ConnectedState == sock->state() )
    {
        int cnt = 0;
        while ( cnt < size )
        {
            if ( sock->bytesAvailable() > 0 )
                cnt += sock->read((char*)data+cnt, size-cnt);
            else
                sock->waitForReadyRead();
        }
    }
}

void sock_write( QLocalSocket* sock, const void* data, int size )
{
    if ( QLocalSocket::ConnectedState == sock->state() )
    {
        int cnt = 0;
        while ( cnt < size )
        {
            cnt += sock->write((const char*)data+cnt, size-cnt);
            sock->waitForBytesWritten();
        }
    }
}


QString shm_key(quint64 pid, void* id, int seq)
{
    return QString::number(pid, 16) + "-" + QString::number((quintptr)id, 16) + "-" + QString::number(seq);
}

QWSScreenData::QWSScreenData(bool server)
    : shm(QWS_SHM)
    , data(NULL)
{
    if ( server )
    {
        if ( !shm.create(sizeof(QWSScreenInfo)) )
        {
            qDebug() << "Cant't create shm!";
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        if ( !shm.attach() )
        {
            qDebug() << "Cant't attach shm!";
            exit(EXIT_FAILURE);
        }
    }

    data = (QWSScreenInfo*)shm.data();
}

QWSScreenData::~QWSScreenData()
{
    qDebug() << "Destroy shared memory!";
}

QSize QWSScreenData::getSize() const
{
    ShmLocker l(&shm);
    QSize s(data->width,data->height);
    return s;
}

void QWSScreenData::setSize(const QSize size)
{
    ShmLocker locker(&shm);
    data->width = size.width();
    data->height = size.height();
}

int QWSScreenData::getRotation() const
{
    ShmLocker locker(&shm);
    int result = data->rotation;
    return result;
}

void QWSScreenData::setRotation(int val)
{
    ShmLocker locker(&shm);
    data->rotation = val;
}

QPoint clientToScreen(const QSize& s, int rot, const QPoint &pt)
{
    QPoint res(pt);
    switch( rot )
    {
    case 90:
        res = QPoint(pt.y(), s.height()-pt.x());
        break;
    case 180:
        res = QPoint(s.width()-pt.x(), s.height()-pt.y());
        break;
    case 270:
        res = QPoint(s.width()-pt.y(), pt.x());
        break;
    }
    return res;
}

QRect clientToScreen(const QSize &s, int rot, const QRect &r)
{
    QRect res(r);
    switch( rot )
    {
    case 90:
        res = QRect( clientToScreen( s, rot, QPoint( r.left()+r.width(), r.top() ) ), QSize( r.height(), r.width() ) );
        break;
    case 180:
        res = QRect( clientToScreen( s, rot, QPoint( r.left()+r.width(), r.top()+r.height() ) ), QSize( r.width(), r.height() ) );
        break;
    case 270:
        res = QRect( clientToScreen( s, rot, QPoint( r.left(), r.top()+r.height() ) ), QSize( r.height(), r.width() ) );
        break;
    }
    return res;
}

QPoint screenToClient(const QSize &s, int rot, const QPoint &pt)
{
    QPoint res(pt);
    switch( rot )
    {
    case 90:
        res = QPoint(s.height()-pt.y(), pt.x());
        break;
    case 180:
        res = QPoint(s.width()-pt.x(), s.height()-pt.y());
        break;
    case 270:
        res = QPoint(pt.y(), s.width()-pt.x());
        break;
    }
    return res;
}

QRect screenToClient(const QSize &s, int rot, const QRect &r)
{
    QRect res(r);
    switch( rot )
    {
    case 90:
        res = QRect( screenToClient( s, rot, QPoint( r.left(), r.top()+r.height() ) ), QSize( r.height(), r.width() ) );
        break;
    case 180:
        res = QRect( clientToScreen( s, rot, QPoint( r.left()+r.width(), r.top()+r.height() ) ), QSize( r.width(), r.height() ) );
        break;
    case 270:
        res = QRect( screenToClient( s, rot, QPoint( r.left()+r.width(), r.top() ) ), QSize( r.height(), r.width() ) );
        break;
    }
    return res;
}
