#include "myobject.h"

MyObject::MyObject(QObject *parent)
    : QObject(parent)
    , sock(new QLocalSocket(this))
{    
    connect(sock, SIGNAL(readyRead()), this, SLOT(readyRead()));
    sock->connectToServer("vlasovsoft_qws");
}

void MyObject::readyRead()
{
    qDebug() << "readyRead()";
}
