#include <QDebug>
#include <QThread>
#include <QMessageBox>

#include "widget.h"
#include "ui_widget.h"

extern QLocalSocket* sock;

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_2_clicked()
{
    ui->label->setText("1");
}

void Widget::on_pushButton_clicked()
{
    ui->label->setText("2");
}

void Widget::on_pushButton_3_clicked()
{
    QMessageBox::information(this, "QWS", "Hello, QWS !!! Hello, QWS !!!", QMessageBox::Ok);
}

void Widget::on_pushButton_4_clicked()
{
    close();
}
