#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QObject>
#include <QLocalSocket>

class MyObject : public QObject
{
    Q_OBJECT

QLocalSocket* sock;

public:
    MyObject(QObject *parent = nullptr);

private slots:
    void readyRead();

};

#endif // MYOBJECT_H
