TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS = common

kobo|kindle|qvfb {
    SUBDIRS += SuspendManager
}

SUBDIRS += Launcher \
    Msgbox \
    Showpic \
    cr3 \
    Calculator \
    Notepad \
    Dictionary \
    FileManager \
    Konsole \
    Opds \
    WiFiServer \
    Upgrade

kindle {
    SUBDIRS += QtKindlePlugins
}
kobo {
    SUBDIRS += fmon QtKoboPlugins
}
obreey {
    SUBDIRS += QtIvPlugins
}

