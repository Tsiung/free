#ifndef STYLUSPLUGIN_H
#define STYLUSPLUGIN_H

#include <QMouseDriverPlugin>
#include <QWSMouseHandler>

class StylusPlugin : public QMouseDriverPlugin
{
    Q_OBJECT
    
public:
    StylusPlugin(QObject* parent = 0);

    QStringList keys() const;
    QWSMouseHandler* create(const QString & key, const QString & device);
};


#endif // STYLUSPLUGIN_H
