#ifndef KINDLESCREEN_H
#define KINDLESCREEN_H

#include <QThread>
#include <QRect>
#include <QRegion>
#include <QTimer>
#include <QScreen>

QT_BEGIN_NAMESPACE

QT_MODULE(Gui)

class IvScreen : public QTimer, public QScreen
{
    Q_OBJECT

public:
    IvScreen();
    virtual ~IvScreen();

    virtual bool initDevice();
    virtual void shutdownDevice();

    virtual void setDirty ( const QRect& rect );
    virtual bool connect ( const QString& displaySpec );
    void disconnect() {}
    virtual void setMode(int, int, int)	{}

private slots:
    void update();

private:
    bool isDebug;
    QRect updateRect;

private:
    bool isFullUpdate() const;
};

QT_END_NAMESPACE

#endif // IV_SCREEN_H
