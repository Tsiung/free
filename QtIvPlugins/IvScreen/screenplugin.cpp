#include "screenplugin.h"
#include "IvScreen.h"

ScreenPlugin::ScreenPlugin() : QScreenDriverPlugin()
{
    qDebug("ScreenPlugin::ScreenPlugin()");
}

QStringList ScreenPlugin::keys() const
{
    qDebug("ScreenPlugin::keys()");
    QStringList list;
    list << QLatin1String("IvScreen");
    return list;
}

QScreen* ScreenPlugin::create(const QString& driver, int /*displayId*/)
{
    qDebug("ScreenPlugin::create()");
    if (driver.toLower() == QLatin1String("ivscreen"))
    {
        return new IvScreen();
    }
    return NULL;
}

Q_EXPORT_PLUGIN2(IvScreen, ScreenPlugin)
