#include <inkview.h>

#include "IvInput.h"
#include "IvInputEvent.h"
#include "IvKeys.h"

IvKeys::IvKeys(const QString& /*driver*/, const QString & device, QObject* parent)
    : QObject(parent)
    , QWSKeyboardHandler(device)
    , keyOkRepeatCount(0)
    , keyNextRepeatCount(0)
    , keyPrevRepeatCount(0)
{
    IvInput::instanse()->setKeyboardHandler(this);
}

IvKeys::~IvKeys()
{
    IvInput::destroy();
}

void IvKeys::customEvent( QEvent* event )
{
    IvInputEvent* pEvent = static_cast<IvInputEvent*>(event);
    if (    KEY_OK   == pEvent->param1 
         || KEY_PREV == pEvent->param1 
         || KEY_NEXT == pEvent->param1 )
    {
        processExtendedKey( pEvent );
    }
    else
    {
        processNormalKey( pEvent );
    }
}

void IvKeys::processNormalKey( IvInputEvent* pEvent )
{
    bool isPress = false;
    bool isRepeat = false;
    switch( pEvent->type() )
    {
    case IvInputEvent::evtKeyPress:
        isPress = true;
        break;
    case IvInputEvent::evtKeyRepeat:
        isPress  = true;
        isRepeat = true;
        break;
    case IvInputEvent::evtKeyRelease:
        break;
    default:
        break;
    }
    int unicode = -1;
    int keycode = 0;
    switch( pEvent->param1 )
    {
    case KEY_UP:
        keycode = Qt::Key_Up;
        break;
    case KEY_DOWN:
        keycode = Qt::Key_Down;
        break;
    case KEY_LEFT:
        keycode = Qt::Key_Left;
        break;
    case KEY_RIGHT:
        keycode = Qt::Key_Right;
        break;
    case KEY_MENU: // F2
        keycode = Qt::Key_Menu;
        break;
    case KEY_BACK: // F3
        keycode = Qt::Key_Escape;
        break;
    default:
        break;
    }
    if ( keycode )
    {
        processKeyEvent(unicode, keycode, Qt::NoModifier, isPress, isRepeat);
    }
}

void IvKeys::processExtendedKey( IvInputEvent* pEvent )
{
    switch( pEvent->param1 )
    {
    case KEY_OK:
        processExtendedKey( pEvent->type(), &keyOkRepeatCount, Qt::Key_Select, Qt::Key_Menu );
        break;
    case KEY_NEXT:
        processExtendedKey( pEvent->type(), &keyNextRepeatCount, Qt::Key_PageDown, Qt::Key_Home );
        break;
    case KEY_PREV:
        processExtendedKey( pEvent->type(), &keyNextRepeatCount, Qt::Key_PageUp, Qt::Key_Back );
        break;
    }
}

void IvKeys::processExtendedKey( int eventType, int* repeatCount, int codeShort, int codeLong )
{
    int keyCode = 0;
    switch ( eventType )
    {
    case IvInputEvent::evtKeyPress:
        *repeatCount = 0;
        break;
    case IvInputEvent::evtKeyRelease:
        if ( 0 == *repeatCount )
        {
            keyCode = codeShort;
        }
        break;
    case IvInputEvent::evtKeyRepeat:
        ++(*repeatCount);
        if ( 1 == *repeatCount )
        {
            keyCode = codeLong;
        }
        break;
    default:
        break;
    }
    if ( keyCode )
    {
        processKeyEvent(-1, keyCode, Qt::NoModifier, true, false);
        processKeyEvent(-1, keyCode, Qt::NoModifier, false, false);
    }
}
