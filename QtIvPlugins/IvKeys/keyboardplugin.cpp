#include "keyboardplugin.h"
#include "IvKeys.h"

KeyboardPlugin::KeyboardPlugin(QObject* parent) : QKbdDriverPlugin(parent)
{}

QStringList KeyboardPlugin::keys() const
{
    QStringList list;
    list << QLatin1String("IvKeys");
    return list;
}

QWSKeyboardHandler* KeyboardPlugin::create(const QString & key, const QString & device)
{
    if (key.toLower() == QLatin1String("ivkeys"))
    {
        return new IvKeys(key, device);
    }

    return 0;
}

Q_EXPORT_PLUGIN2(IvKeys, KeyboardPlugin)
