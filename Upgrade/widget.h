#ifndef WIDGET_H
#define WIDGET_H

#include <QThread>
#include <QStateMachine>

#include "WidgetCommon.h"

namespace Ui {
class Widget;
}

class QKeyEvent;
class QCloseEvent;
class Worker;

class Widget : public WidgetCommon
{
Q_OBJECT
    
public:
    Widget();
    ~Widget();

private slots:
    void progress(const QString& str);
    void activity();
    void finished();
    void on_btnClose_clicked();
    void on_btnSettings_clicked();
    void checkRemoteVersion();
    void upgrade();

private:
    Ui::Widget *ui;
    QThread thread;
    Worker* w;
    QStateMachine machine;
};

#endif // WIDGET_H
