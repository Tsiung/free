<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>QuaGzipFile</name>
    <message>
        <location filename="../quazip/quagzipfile.cpp" line="60"/>
        <source>QIODevice::Append is not supported for GZIP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quazip/quagzipfile.cpp" line="66"/>
        <source>Opening gzip for both reading and writing is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quazip/quagzipfile.cpp" line="74"/>
        <source>You can open a gzip either for reading or for writing. Which is it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quazip/quagzipfile.cpp" line="80"/>
        <source>Could not gzopen() file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuaZIODevice</name>
    <message>
        <location filename="../quazip/quaziodevice.cpp" line="147"/>
        <source>QIODevice::Append is not supported for QuaZIODevice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../quazip/quaziodevice.cpp" line="152"/>
        <source>QIODevice::ReadWrite is not supported for QuaZIODevice</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuaZipFile</name>
    <message>
        <location filename="../quazip/quazipfile.cpp" line="247"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.ui" line="20"/>
        <source>Allow test builds</source>
        <translation>Разрешить тестовые сборки</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="30"/>
        <source>Current build: %1
Click the &apos;next&apos; button to check a new build...</source>
        <translation>Текущая сборка: %1
Нажмите кнопку &apos;дальше&apos; для проверки обновления...</translation>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.cpp" line="33"/>
        <location filename="../worker.cpp" line="63"/>
        <source>Checking a new build...</source>
        <translation>Проверка новой сборки...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="85"/>
        <source>Downloading...</source>
        <translation>Загружаю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="100"/>
        <source>Extracting...</source>
        <translation>Распаковываю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="104"/>
        <source>Extract success!</source>
        <translation>Архив успешно распакован!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="105"/>
        <source>Upgrading...</source>
        <translation>Обновляю...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="115"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="207"/>
        <source>Upgrade success!</source>
        <translation>Обновление завершено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="208"/>
        <source>Please restart launcher.</source>
        <translation>Пожалуйста перезапустите лаунчер.</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="49"/>
        <location filename="../worker.cpp" line="145"/>
        <source>New build is found: %1</source>
        <translation>Найдена новая сборка: %1</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="50"/>
        <location filename="../worker.cpp" line="146"/>
        <source>Click the &apos;next&apos; button to upgrade...</source>
        <translation>Для обновления нажмите кнопку &apos;дальше&apos;...</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="151"/>
        <source>No new builds are available!</source>
        <translation>Новых сборок не найдено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="174"/>
        <source>Download success!</source>
        <translation>Обновление успешно загружено!</translation>
    </message>
    <message>
        <location filename="../worker.cpp" line="191"/>
        <source>Error: %1 - %2</source>
        <translation>Ошибка %1 - %2</translation>
    </message>
</context>
</TS>
