#include <QDebug>
#include <QUrl>
#include <QDir>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QRegExp>
#include <QProcess>

#include "JlCompress.h"

#include "Platform.h"
#include "Config.h"
#include "Log.h"
#include "json.h"
#include "params.h"
#include "worker.h"

using namespace QtJson;

Worker::Worker(QObject *parent)
    : QObject(parent)
    , version(STR(VERSION))
    , downloaded(0)
    , extracted(0)
    , test(false)
    , proc(NULL)
{
    nm = new QNetworkAccessManager(this);
}

void Worker::checkLocalVersion()
{
    emit progress(tr("Checking a new build..."));

    QDir dir( zipFilePath() );
    if ( dir.exists() )
    {
        QRegExp rex("(\\d\\d\\d\\d\\.\\d\\d\\.\\d\\d)_([a-zA-Z0-9]+)\\.zip");
        QStringList files = dir.entryList( QStringList(), QDir::Files | QDir::NoSymLinks );
        foreach ( const QString& file, files )
        {
            if ( rex.exactMatch( file ) )
            {
                QStringList cap( rex.capturedTexts() );
                if ( cap.at(2) == platform() && cap.at(1) > version )
                {
                    version = cap.at(1);
                    downloaded = 1;
                    emit progress(tr("New build is found: %1").arg(version));
                    emit progress(tr("Click the 'next' button to upgrade..."));
                    emit newBuildFound();
                    return;
                }
            }
        }
    }

    emit noLocalBuild();
}

void Worker::checkRemoteVersion()
{
    emit progress(tr("Checking a new build..."));

    QUrl url("http://pbchess.vlasovsoft.net/ver.php");

    QUrl postData;

    postData.addQueryItem("platform",platform());

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QNetworkReply* reply = nm->post(request, postData.encodedQuery());

    setupReply(reply);
    QObject::connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadDefault()));
    QObject::connect(reply, SIGNAL(finished()), this, SLOT(checkVersionFinished()));
}

void Worker::upgrade()
{
    if ( !isDownloaded() )
    {
        emit progress(tr("Downloading..."));
        currFile.setFileName(zipFileFullName() + ".tmp");
        if (currFile.open( QIODevice::WriteOnly | QIODevice::Truncate ) )
        {
            QNetworkRequest request(QUrl(QString("http://pbchess.vlasovsoft.net/files/") + (test? "test/":"") +zipFileName()));
            QNetworkReply* reply = nm->get( request );

            setupReply(reply);
            QObject::connect(reply, SIGNAL(finished()), this, SLOT(downloadFileFinished()));
            QObject::connect(reply, SIGNAL(readyRead()), this, SLOT(downloadFileReadyRead()));
        }
    }
    else
    if ( !isExtracted() )
    {
        emit progress(tr("Extracting..."));

        JlCompress::extractDir(zipFileFullName(), upgradePath());
        QFile(zipFileFullName()).remove();
        emit progress(tr("Extract success!"));
        emit progress(tr("Upgrading..."));
        proc = new QProcess;
        QObject::connect(proc, SIGNAL(finished(int)), this, SLOT(processFinished(int)));
        QObject::connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(processReadyReadStandardOutput()));
        QObject::connect(proc, SIGNAL(readyReadStandardError()), this, SLOT(processReadyReadStandardError()));
        QStringList arguments;
        arguments << QString::fromLocal8Bit(qgetenv("ROOT")) + QDir::separator() + "upgrade_batch.sh";
        proc->start("sh", arguments);
        if ( !proc->waitForStarted() )
        {
            emit progress(tr("Error!"));
        }
    }
}

void Worker::checkVersionFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    reply->deleteLater();

    bool found = false;
    bool ok = true;
    JsonObject a = QtJson::parse(buffer, ok).toMap();
    if ( ok )
    {
        QString rver = a["version"].toString();
        QString tver = a["test_version"].toString();
        if ( rver > version ) {
            found = true;
            version = rver;
            test = false;
        }
        if ( g_pConfig->readBool(CFG_ALLOW_TEST_BUILDS,false) && tver > version ) {
            found = true;
            version = tver;
            test = true;
        }
    }
    if ( found )
    {
        emit progress(tr("New build is found: %1").arg(version));
        emit progress(tr("Click the 'next' button to upgrade..."));
        emit newBuildFound();
    }
    else
    {
        emit progress(tr("No new builds are available!"));
        emit finished();
    }
}

void Worker::downloadFileReadyRead()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    currFile.write(reply->readAll());
}

void Worker::downloadFileFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    reply->deleteLater();
    currFile.close();
    if ( QNetworkReply::NoError != reply->error() )
    {
        currFile.remove();
    }
    else
    {
        currFile.rename(zipFileFullName());
        emit progress(tr("Download success!"));
        downloaded = true;
        upgrade();
    }
}

void Worker::readyReadDefault()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    buffer.append(reply->readAll());
    emit activity();
}

void Worker::error(QNetworkReply::NetworkError code)
{
    if ( currFile.isOpen() ) currFile.close();
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    emit progress(tr("Error: %1 - %2").arg(code).arg(reply->errorString()));
    emit finished();
}

void Worker::processReadyReadStandardOutput()
{
    g_pLog->write(1,Log::MT_I,QString::fromLocal8Bit(proc->readAllStandardOutput()));
}

void Worker::processReadyReadStandardError()
{
    g_pLog->write(1,Log::MT_I,QString::fromLocal8Bit(proc->readAllStandardError()));
}

void Worker::processFinished(int)
{
    emit progress(tr("Upgrade success!"));
    emit progress(tr("Please restart launcher."));
    extracted = 1;
    proc->deleteLater();
    proc = NULL;
}

QString Worker::platform() const
{
#if defined(KOBO)
    return "kobo";
#elif defined(KINDLE)
    return "kindle";
#elif defined(POCKETBOOK)
    return "pb";
#elif defined(OBREEY)
    return "obreey";
#elif defined(QVFB)
    return "qvfb";
#else
    #error "Unknown platform"
#endif
}

QString Worker::zipFileName() const
{
    return QString("%1_%2.zip").arg(version).arg(platform());
}

QString Worker::zipFileFullName() const
{
    return zipFilePath() + QDir::separator() + zipFileName();
}

QString Worker::upgradePath() const
{
    return QString::fromLocal8Bit(qgetenv("ROOT")) + QDir::separator() + ".." + QDir::separator() + "vlasovsoft.upgrade";
}

QString Worker::zipFilePath() const
{
    return QString::fromLocal8Bit(qgetenv("ROOT")) + QDir::separator() + "..";
}

void Worker::setupReply(QNetworkReply *reply)
{
    buffer.clear();
    QObject::connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
}
