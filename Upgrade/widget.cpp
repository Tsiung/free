#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QThread>

#include "Log.h"
#include "Config.h"
#include "params.h"
#include "QtUtils.h"
#include "Platform.h"
#include "EnterTextLineDialog.h"

#if defined(Q_WS_QWS)
#include "wifidialog.h"
#endif

#include "settingsdialog.h"
#include "worker.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)
{
    ui->setupUi(this);
    initTopLevelWidget(this);

    ui->textBrowser->setReadOnly(true);
    ui->textBrowser->setPlainText(tr("Current build: %1\nClick the 'next' button to check a new build...").arg(STR(VERSION)));

    int iconSize = mm_to_px(10);
    ui->btnClose->setIconSize(QSize(iconSize,iconSize));
    ui->btnSettings->setIconSize(QSize(iconSize,iconSize));
    ui->btnNext->setIconSize(QSize(iconSize,iconSize));

    w = new Worker;
    w->moveToThread(&thread);

    QObject::connect(&thread, SIGNAL(finished()), w, SLOT(deleteLater()));
    QObject::connect(w, SIGNAL(progress(QString)), this, SLOT(progress(QString)));
    QObject::connect(w, SIGNAL(activity()), this, SLOT(activity()));
    QObject::connect(w, SIGNAL(finished()), this, SLOT(finished()));

    QState* stateInitial = new QState();
    stateInitial->assignProperty(ui->btnNext, "enabled", true);
    stateInitial->assignProperty(ui->btnSettings, "enabled", true);

    QState* stateVCheckLocalStarted = new QState();
    stateVCheckLocalStarted->assignProperty(ui->btnNext, "enabled", false);
    stateVCheckLocalStarted->assignProperty(ui->btnSettings, "enabled", false);

    QState* stateVCheckRemoteStarted = new QState();
    stateVCheckRemoteStarted->assignProperty(ui->btnNext, "enabled", false);
    stateVCheckRemoteStarted->assignProperty(ui->btnSettings, "enabled", false);

    QState* stateVCheckSuccess = new QState();
    stateVCheckSuccess->assignProperty(ui->btnNext, "enabled", true);

    QState* stateUpgradeStarted = new QState();
    stateUpgradeStarted->assignProperty(ui->btnNext, "enabled", false);

    // transitions
    stateInitial->addTransition(ui->btnNext, SIGNAL(clicked()), stateVCheckLocalStarted);
    stateVCheckLocalStarted->addTransition(w, SIGNAL(newBuildFound()), stateVCheckSuccess);
    stateVCheckLocalStarted->addTransition(w, SIGNAL(noLocalBuild()), stateVCheckRemoteStarted);
    stateVCheckRemoteStarted->addTransition(w, SIGNAL(newBuildFound()), stateVCheckSuccess);
    stateVCheckSuccess->addTransition(ui->btnNext, SIGNAL(clicked()), stateUpgradeStarted);

    machine.addState(stateInitial);
    machine.addState(stateVCheckLocalStarted);
    machine.addState(stateVCheckRemoteStarted);
    machine.addState(stateVCheckSuccess);
    machine.addState(stateUpgradeStarted);
    machine.setInitialState(stateInitial);

    QObject::connect(stateVCheckLocalStarted, SIGNAL(entered()), w, SLOT(checkLocalVersion()));
    QObject::connect(stateVCheckRemoteStarted, SIGNAL(entered()), this, SLOT(checkRemoteVersion()));
    QObject::connect(stateUpgradeStarted, SIGNAL(entered()), this, SLOT(upgrade()));

    machine.start();
    thread.start();
}

Widget::~Widget()
{
    thread.quit();
    thread.wait();
    delete ui;
}

void Widget::progress(const QString &str)
{
    ui->textBrowser->append(str);
}

void Widget::activity()
{
    Platform::get()->networkActivity();
}

void Widget::finished()
{
}

void Widget::on_btnClose_clicked()
{
    close();
}

void Widget::on_btnSettings_clicked()
{
    SettingsDialog dlg(this);
    dlg.exec();
}

void Widget::checkRemoteVersion()
{
    if ( ::activateNetwork(this) )
        QMetaObject::invokeMethod( w, "checkRemoteVersion" );
    else
        close();
}

void Widget::upgrade()
{
    if ( w->isDownloaded() || ::activateNetwork(this) )
        QMetaObject::invokeMethod( w, "upgrade" );
    else
        close();
}
