#include "widget.h"
#include "Application.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.setApplicationName("upgrade");
    if ( !app.init() ) return 1;
    Widget widget;
    widget.show();
    app.exec();
    return 0;
}
