#include <QDebug>
#include <QTextDocumentWriter>
#include <QKeyEvent>

#include "Config.h"
#include "QtUtils.h"
#include "messagebox.h"
#include "Platform.h"

#include "FileOpenDialog.h"
#include "FileSaveDialog.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : pCodec(QTextCodec::codecForName(g_pConfig->readString("encoding", "utf8").c_str()))
    , ui(new Ui::Widget)

{
    QTextCodec::setCodecForCStrings ( pCodec );

    ui->setupUi(this);

    initTopLevelWidget(this);

    connect( ui->btnNew, SIGNAL(clicked()), this, SLOT(onNew()) );
    connect( ui->btnOpen, SIGNAL(clicked()), this, SLOT(onOpen()) );
    connect( ui->btnSave, SIGNAL(clicked()), this, SLOT(onSave()) );
    connect( ui->btnUndo, SIGNAL(clicked()), this, SLOT(onUndo()) );
    connect( ui->btnRedo, SIGNAL(clicked()), this, SLOT(onRedo()) );
    connect( ui->btnCopy, SIGNAL(clicked()), this, SLOT(onCopy()) );
    connect( ui->btnCut, SIGNAL(clicked()), this, SLOT(onCut()) );
    connect( ui->btnPaste, SIGNAL(clicked()), this, SLOT(onPaste()) );
    connect( ui->btnZoomOut, SIGNAL(clicked()), this, SLOT(onZoomOut()) );
    connect( ui->btnZoomIn, SIGNAL(clicked()), this, SLOT(onZoomIn()) );
    connect( ui->btnClose, SIGNAL(clicked()), this, SLOT(onCloseTab()) );
    connect( ui->btnExit, SIGNAL(clicked()), this, SLOT(close()) );
    connect( ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(onCurrentTabChanged(int)));

    openFiles();
    if ( 0 == ui->tabWidget->count() )
    {
        addTab();
    }

#if defined(Q_WS_QWS)
    ui->verticalLayout->insertWidget(1, getVirtualKeyboard());
#endif

    show();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::load( const QString& fileName )
{
    if ( !fileName.isEmpty() )
    {
        QFile f( fileName );
        if (f.open(QFile::ReadOnly))
        {
            QByteArray data = f.readAll();
            QString str(data);
            addTab( fileName );
            currentDocWidget()->edit->setPlainText(str);
            QTextCursor textCursor = currentDocWidget()->edit->textCursor();
            textCursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
            currentDocWidget()->edit->setTextCursor(textCursor);
            currentDocWidget()->edit->ensureCursorVisible();
        }
    }
}

void Widget::closeEvent(QCloseEvent* e)
{
    bool isCancel = false;
    QFile file(Platform::get()->getRootPath() + QDir::separator() + "files.ini");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    ui->tabWidget->setCurrentIndex(0);
    while ( ui->tabWidget->count() > 0 && !isCancel )
    {
        DocumentWidget* w = currentDocWidget();
        QString fileName = w->fileName;
        isCancel = onCloseTab( false );
        if ( !isCancel )
        {
            if ( !fileName.isEmpty() )
                out << fileName << endl;
        }
    }
    if ( isCancel )
        e->ignore();

    file.close();
}

void Widget::addTab( const QString& fileName )
{
    DocumentWidget* page = new DocumentWidget(this);
    page->fileName = fileName;
    QVBoxLayout* pLayout = new QVBoxLayout(page);
    QFont font(page->edit->font());
    font.setPointSize( g_pConfig->readInt("font.size", 14) );
    page->edit->setFont(font);
    page->edit->installEventFilter(this);
    page->edit->setWordWrapMode( QTextOption::NoWrap );
    page->edit->setFocusPolicy( Qt::StrongFocus );
    pLayout->addWidget( page->edit );
    ::activate_scroller(page->edit);
    QString tabName = fileName.isEmpty() ?
        "unnamed" :
        QFileInfo(fileName).baseName();
    ui->tabWidget->addTab( page, tabName );
    ui->tabWidget->setCurrentIndex( ui->tabWidget->count() - 1 );
}

DocumentWidget* Widget::docWidget( int index )
{
    return static_cast<DocumentWidget*>( ui->tabWidget->widget( index ) );
}

DocumentWidget* Widget::currentDocWidget()
{
    return static_cast<DocumentWidget*>( ui->tabWidget->currentWidget() );
}

void Widget::onNew()
{
    addTab();
}

void Widget::onOpen()
{
   QString path = g_pConfig->readQString("default_path");
   if ( path.isEmpty() )
       path = Platform::get()->getRootPath();
   QString filter = g_pConfig->readQString("default_filter");
   FileOpenDialog dlg(this, path, filter, g_pConfig->readInt("hidden", 0) != 0);
   if ( dlg.exec() )
   {
       QString fileName = dlg.getFileName();
       load(fileName);
   }
}

void Widget::onSave()
{
    DocumentWidget* doc = currentDocWidget();
    bool isSetFileName = false;
    if ( doc->fileName.isEmpty() )
    {
        QString path(g_pConfig->readQString("default_path"));
        FileSaveDialog dlg(this, path, ".txt");
        dlg.setModelFilters( dlg.modelFilters() | QDir::Hidden ); 
        if ( dlg.exec() )
        {
            doc->fileName = dlg.getFileName();
            isSetFileName = true;
        }
    }
    if ( !doc->fileName.isEmpty() )
    {
        QTextDocumentWriter writer(doc->fileName);
        writer.setFormat("plaintext");
        writer.setCodec(pCodec);
        writer.write(doc->edit->document());
        doc->edit->document()->setModified(false);
        if ( isSetFileName )
        {
            ui->tabWidget->setTabText( ui->tabWidget->currentIndex(),
                                       QFileInfo(doc->fileName).baseName() );
        }
    }
}

void Widget::onUndo()
{
    currentDocWidget()->edit->undo();
}

void Widget::onRedo()
{
    currentDocWidget()->edit->redo();
}

void Widget::onCopy()
{
    currentDocWidget()->edit->copy();
}

void Widget::onCut()
{
    currentDocWidget()->edit->cut();
}

void Widget::onPaste()
{
    currentDocWidget()->edit->paste();
}

void Widget::onZoomOut()
{
    int fontSize = g_pConfig->readInt("font.size", 14);
    if ( fontSize > 5 )
    {
        fontSize --;
        for ( int i = 0; i < ui->tabWidget->count(); ++i )
        {
            QFont f( docWidget(i)->edit->font() );
            f.setPointSize( fontSize );
            docWidget(i)->edit->setFont(f);
        }
        g_pConfig->writeInt("font.size", fontSize);
    }
}

void Widget::onZoomIn()
{
    int fontSize = g_pConfig->readInt("font.size", 14);
    if ( fontSize < 100 )
    {
        fontSize ++;
        for ( int i = 0; i < ui->tabWidget->count(); ++i )
        {
            QFont f( docWidget(i)->edit->font() );
            f.setPointSize( fontSize );
            docWidget(i)->edit->setFont(f);
        }
        g_pConfig->writeInt("font.size", fontSize);
    }
}

bool Widget::onCloseTab( bool keepLast )
{
    QPlainTextEdit* edit = currentDocWidget()->edit;
    bool isCancel = false;
    if ( edit->document()->isModified() )
    {
        QMessageBox::StandardButton result =
                ::questionBox( this, tr("Changed"), tr("Save changes?"),
                               QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel,
                               QMessageBox::Yes);
        if ( QMessageBox::Yes == result )
        {
            onSave();
        }
        else
        if ( QMessageBox::Cancel == result )
            isCancel = true;
    }
    if ( !isCancel )
    {
        ui->tabWidget->removeTab( ui->tabWidget->currentIndex() );
        if ( keepLast && 0 == ui->tabWidget->count() )
        {
            addTab();
        }
    }
    return isCancel;
}

void Widget::onCurrentTabChanged( int index )
{
    if ( index >= 0 )
    {
        docWidget(index)->edit->setFocus();
    }
}

void Widget::openFiles()
{
    QFile file(Platform::get()->getRootPath() + QDir::separator() + "files.ini");
    if( file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        while(!in.atEnd())
        {
            QString fileName(in.readLine().trimmed());
            load(fileName);
        }
    }
}
