#ifndef KOBOTS_H
#define KOBOTS_H

#include <QWSMouseHandler>
#include <QSocketNotifier>
#include <QWSServer>

#include <fcntl.h>
#include <linux/input.h>

#include "state.h"

#include "KoboDevice.h"

class KoboTS : public QObject, public QWSMouseHandler
{
    Q_OBJECT

public:
    KoboTS(const QString & driver = QString(), const QString & device = QString(), QObject* parent = 0);
    virtual ~KoboTS();

    virtual void suspend();
    virtual void resume();

private slots:
    void activity(int);

private:
    KoboDevice dev;

    int _fd;
    QSocketNotifier* _sn;

    Qt::MouseButton buttons;
    State state;
    int trId;

    bool _debug;
    bool isInputCaptured;

private:
    void captureInput();
    void releaseInput();
    void xPosition(int val);
    void yPosition(int val);
};

#endif // KOBOTS_H
