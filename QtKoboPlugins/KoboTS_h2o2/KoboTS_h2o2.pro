QT += core gui
TARGET = KoboTS_h2o2
TEMPLATE = lib
CONFIG += plugin

SOURCES += tsplugin.cpp kobots.cpp
HEADERS += tsplugin.h kobots.h state.h
INCLUDEPATH += $$PWD/../../SuspendManager $$PWD/../../common
LIBS        += -L../../SuspendManager -lSuspendManager
