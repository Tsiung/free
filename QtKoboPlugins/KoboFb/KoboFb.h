#ifndef KOBO_FB_H
#define KOBO_FB_H

#include <QTimer>
#include <QLinuxFbScreen>

#include "KoboDevice.h"
#include "mxcfb.h"

class QPoint;
class QRegion;
class QRect;

QT_BEGIN_NAMESPACE

QT_MODULE(Gui)

class KoboFb : public QTimer, public QLinuxFbScreen
{
    Q_OBJECT

public:
    KoboFb();
    virtual ~KoboFb();

    virtual bool connect(const QString& displaySpec);
    virtual void setDirty ( const QRect& rect );

private slots:
    void update();

private:
    KoboDevice dev;
    int fb;
    Mxcfb einkFb;
    bool isDebug;
    QRect updateRect;
};

QT_END_NAMESPACE

#endif // KOBO_FB_H
