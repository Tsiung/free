#include "QtUtils.h"
#include "cr3widget.h"

#include "selection.h"
#include "ui_selection.h"

Selection::Selection(CR3View* v)
    : QWidget(NULL)
    , ui(new Ui::Selection)
    , view(v)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    ui->setupUi(this);
    ui->btnCite->setEnabled(false);

    QSize s(sizeHint());
    s.setWidth(view->width());
    resize(s);
    move(0, view->height()-s.height());
    raise();

    int iconSize = mm_to_px(10);
    ui->btnClose->setIconSize(QSize(iconSize,iconSize));
    ui->btnCite->setIconSize(QSize(iconSize,iconSize));
    ui->btnPrevLine->setIconSize(QSize(iconSize,iconSize));
    ui->btnNextLine->setIconSize(QSize(iconSize,iconSize));

    view->setSelectionMode(true);
    view->getDocView()->setViewMode(DVM_SCROLL);

    QObject::connect(view, SIGNAL(selected()), this, SLOT(onSelected()));

    view->update();
}

Selection::~Selection()
{
    delete ui;
}

void Selection::closeEvent(QCloseEvent *event)
{
    view->clearSelection();
    view->setSelectionMode(false);
    view->getDocView()->setViewMode(DVM_PAGES);
    view->update();
}

void Selection::on_btnClose_clicked()
{
    close();
}

void Selection::on_btnNextLine_clicked()
{
    view->doCommand(DCMD_LINEDOWN, 1);
    view->update();
}

void Selection::on_btnPrevLine_clicked()
{
    view->doCommand(DCMD_LINEUP, 1);
    view->update();
}

void Selection::onSelected()
{
    ui->btnCite->setEnabled( !view->getSelectionText().isEmpty() );
}

void Selection::on_btnCite_clicked()
{
    emit citation();
}
