<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.ui" line="20"/>
        <location filename="../bookmarklistdlg.ui" line="26"/>
        <source>Bookmarks</source>
        <translation>Bladwijzer</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="86"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="434"/>
        <source>Error while opening document </source>
        <translation>Fout bij openen bestand</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.ui" line="20"/>
        <source>Document properties</source>
        <translation>Eigenschappen bestand</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="64"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="132"/>
        <source>Archive name</source>
        <translation>Archief naam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="133"/>
        <source>Archive path</source>
        <translation>Pad naar Archief</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="134"/>
        <source>Archive size</source>
        <translation>Archief grootte</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="135"/>
        <source>File name</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="136"/>
        <source>File path</source>
        <translation>Pad naar bestand</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="137"/>
        <source>File size</source>
        <translation>Bestandsgrootte</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="138"/>
        <source>File format</source>
        <translation>Bestandsformaat</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="140"/>
        <source>File info</source>
        <translation>Bestand info</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="142"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="143"/>
        <source>Author(s)</source>
        <translation>Schrijver(s)</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="144"/>
        <source>Series name</source>
        <translation>Naam serie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="145"/>
        <source>Series number</source>
        <translation>Serie nummer</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="146"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="147"/>
        <source>Genres</source>
        <translation>Genres</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="148"/>
        <source>Translator</source>
        <translation>Vertaler</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="149"/>
        <source>Book info</source>
        <translation>Boek info</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="151"/>
        <source>Document author</source>
        <translation>Schrijver bestand</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="152"/>
        <source>Document date</source>
        <translation>Datum bestand</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="153"/>
        <source>Document source URL</source>
        <translation>Bronbestand URL</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="154"/>
        <source>OCR by</source>
        <translation>Gescand door</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="155"/>
        <source>Document version</source>
        <translation>Versie bestand</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="156"/>
        <source>Document info</source>
        <translation>Bestand info</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="158"/>
        <source>Publication name</source>
        <translation>Naam publicatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="159"/>
        <source>Publisher</source>
        <translation>Uitgever</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="160"/>
        <source>Publisher city</source>
        <translation>Uitgegeven in</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="161"/>
        <source>Publication year</source>
        <translation>Jaar van uitgave</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="162"/>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="163"/>
        <source>Publication info</source>
        <translation>Uitgave info</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="165"/>
        <source>Custom info</source>
        <translation>Aangepaste info</translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="../gotodialog.ui" line="14"/>
        <location filename="../gotodialog.ui" line="20"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="52"/>
        <source>Page</source>
        <translation>Bladzijde</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="96"/>
        <source>Go</source>
        <translation>Ga</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="103"/>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="498"/>
        <source>Navigation</source>
        <translation>Navigatie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Bookmark created</source>
        <translation>Bladwijzer aangemaakt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Not found</source>
        <translation>Niet gevonden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Search pattern is not found in document</source>
        <translation>Zoekterm is niet gevonden in het document</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Do you really want to delete the current document?</source>
        <translation>Wilt U het huidige bestand echt verwijderen?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Removed</source>
        <translation>Verwijderd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Document was removed</source>
        <translation>Bestand is verwijderd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Citation is saved</source>
        <translation>Aanhaling werd bewaard</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="894"/>
        <source>Pages remain: %1</source>
        <translation>Resterende paginas: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="902"/>
        <source>Auto paging is off.</source>
        <translation>De automatische paginatie staat uit.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>Auto paging is on.</source>
        <translation>De automatische paginatie staat aan.</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="41"/>
        <source>Open file</source>
        <translation>Open bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <location filename="../mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Next page</source>
        <translation>Volgende pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>Previous page</source>
        <translation>Vorige pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="72"/>
        <source>Forward</source>
        <translation>Vooruit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="77"/>
        <source>Table of contents</source>
        <translation>Inhoudsopgave</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>Recent books</source>
        <translation>Recente boeken</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <location filename="../mainwindow.ui" line="98"/>
        <source>Add bookmark</source>
        <translation>Toevoegen bladwijzer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <source>File properties</source>
        <translation>Eigenschappen bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Find text</source>
        <translation>Zoek tekst</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>Show menu</source>
        <translation>Toon menu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Empty</source>
        <translation>Leeg</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Front light</source>
        <translation>Verlichting</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>Screen rotation</source>
        <translation>Schermrotatie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="153"/>
        <source>Toggle frontlight</source>
        <translation>Wissel verlichting</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Frontlight &gt;</source>
        <translation>Verlichting &gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="163"/>
        <source>Frontlight &lt;</source>
        <translation>Verlichting &lt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="168"/>
        <source>First page</source>
        <translation>Eerste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Last page</source>
        <translation>Laatste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="178"/>
        <source>Next chapter</source>
        <translation>Volgende hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Previous chapter</source>
        <translation>Vorige hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="188"/>
        <source>Next 10 pages</source>
        <translation>10 paginas vooruit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="193"/>
        <source>Previous 10 pages</source>
        <translation>10 paginas terug</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="198"/>
        <source>Zoom In</source>
        <translation>Inzoomen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Zoom Out</source>
        <translation>Uitzoomen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <location filename="../mainwindow.ui" line="211"/>
        <source>Position</source>
        <translation>Positie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="216"/>
        <source>Toggle inversion</source>
        <translation>Wissel inversie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Toggle header</source>
        <translation>Wissel koptekst</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Suspend</source>
        <translation>Pauzeren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>Screen rotation 0</source>
        <translation>Schermrotatie 0</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Screen rotation 90</source>
        <translation>Schermrotatie 90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="241"/>
        <source>Screen rotation 180</source>
        <translation>Schermrotatie 180</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="246"/>
        <source>Screen rotation 270</source>
        <translation>Schermrotatie 270</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="251"/>
        <source>Screen rotation +90</source>
        <translation>Schermrotatie +90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Screen rotation -90</source>
        <translation>Schermrotatie -90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>Screen rotation +180</source>
        <translation>Schermrotatie +180</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>Delete current document</source>
        <translation>Verwijder huidige bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="271"/>
        <source>Dictionary</source>
        <translation>Woordenboek</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="276"/>
        <source>Refresh screen</source>
        <translation>Scherm verversen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="281"/>
        <source>Screen shot</source>
        <translation>Schermafdruk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Select</source>
        <translation>Selecteer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="291"/>
        <source>Pages remain</source>
        <translation>Resterende paginas</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="296"/>
        <source>Auto paging</source>
        <translation>Automatische paginatie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <source>Open last book</source>
        <translation>Heropen het laatst geopende boek</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../navigationbar.cpp" line="92"/>
        <location filename="../navigationbar.cpp" line="109"/>
        <source>%1-%2 (total %3)</source>
        <translation>%1-%2 (totaal %3)</translation>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <location filename="../openfiledlg.ui" line="20"/>
        <location filename="../openfiledlg.ui" line="26"/>
        <source>Open file</source>
        <translation>Open bestand</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.ui" line="26"/>
        <location filename="../recentdlg.ui" line="32"/>
        <source>Recent books</source>
        <translation>Recente boeken</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Removed</source>
        <translation>Verwijderd</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Document was removed</source>
        <translation>Document is verwijderd</translation>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <location filename="../recentremovedlg.ui" line="14"/>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="20"/>
        <source>Delete record only</source>
        <translation>Verwijder deze regel</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="27"/>
        <source>Delete record and file</source>
        <translation>Verwijder deze regel en het bestand</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="47"/>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="20"/>
        <location filename="../searchdlg.ui" line="26"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="51"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="61"/>
        <source>Case sensitive</source>
        <translation>Hoofdlettergevoelig</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="68"/>
        <source>Search forward</source>
        <translation>Vooruit zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="81"/>
        <source>Search backward</source>
        <translation>Terug zoeken</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="42"/>
        <source>Window</source>
        <translation>Scherm</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Startup</source>
        <translation>Opstarten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="83"/>
        <source>Recent book</source>
        <translation>Laatste boek</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="88"/>
        <source>Recent books list</source>
        <translation>Lijst recente boeken</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="93"/>
        <source>Open file</source>
        <translation>Open bestand</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="98"/>
        <source>Do nothing</source>
        <translation>Doe niets</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Formatting</source>
        <translation>Formattering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="114"/>
        <source>Disable txt auto formatting</source>
        <translation>Uitschakelen auto formattering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="121"/>
        <source>Internal styles</source>
        <translation>Interne stijlen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="128"/>
        <source>Embedded fonts</source>
        <translation>Ingebakken lettertypes</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="138"/>
        <source>Launcher settings</source>
        <translation>Launcher instellingen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <source>Page</source>
        <translation>Pagina</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="191"/>
        <source>Header</source>
        <translation>Koptekst</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="197"/>
        <location filename="../settings.ui" line="369"/>
        <source>Show</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="598"/>
        <source>Font</source>
        <translation>Lettertype</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="612"/>
        <source>Face</source>
        <translation>Beeld</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="236"/>
        <location filename="../settings.ui" line="635"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="268"/>
        <source>Elements</source>
        <translation>Elementen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="274"/>
        <source>Chapter marks</source>
        <translation>Hoofdstuk markering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="281"/>
        <source>Book name</source>
        <translation>Naam boek</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="288"/>
        <source>Clock</source>
        <translation>Klok</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>Position percent</source>
        <translation>Positie percentage</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="302"/>
        <source>Page number/count</source>
        <translation>Pagina nummer/aantal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="317"/>
        <source>Battery status</source>
        <translation>Batterij status</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="324"/>
        <source>In percent</source>
        <translation>in procenten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="363"/>
        <source>Footer</source>
        <translation>Voettekst</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="390"/>
        <source>Misc</source>
        <translation>Diversen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="407"/>
        <source>View Mode</source>
        <translation>Beeld opmaak</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="421"/>
        <source>One page</source>
        <translation>Een pagina</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="426"/>
        <source>Two pages</source>
        <translation>Twee pagina&apos;s</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="434"/>
        <source>Update interval</source>
        <translation>Bijwerken iedere</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="521"/>
        <source>Auto paging, sec</source>
        <translation>Automatische paginatie, sec</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="542"/>
        <source>Negative</source>
        <translation>Negatief</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="569"/>
        <source>Styles</source>
        <translation>Stijlen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="671"/>
        <source>Gamma</source>
        <translation>Gamma</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="716"/>
        <source>Aa</source>
        <translation>Aa</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="730"/>
        <location filename="../settings.ui" line="762"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="735"/>
        <source>On for large fonts</source>
        <translation>Aan voor grote lettertypes</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="740"/>
        <source>On for all fonts</source>
        <translation>Aan voor alle lettertypes</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="748"/>
        <source>Hinting</source>
        <translation>Hinting</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="767"/>
        <source>Bytecode</source>
        <translation>Bytecode</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="772"/>
        <source>Auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="790"/>
        <source>Bold</source>
        <translation>Vet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="803"/>
        <source>Kerning</source>
        <translation>Overhang</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="813"/>
        <source>Layout</source>
        <translation>Layout</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="821"/>
        <source>Hyphenation</source>
        <translation>Afbrekingen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="841"/>
        <source>Spacing</source>
        <translation>Spatiering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="880"/>
        <location filename="../settings.ui" line="1390"/>
        <source>Space</source>
        <translation>Spatie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="921"/>
        <source>Floating punctuation</source>
        <translation>Vloeiende leestekens</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="942"/>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="948"/>
        <source>Top</source>
        <translation>Boven</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="974"/>
        <source>Bottom</source>
        <translation>Onder</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1000"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1026"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1073"/>
        <source>Control</source>
        <translation>Controle</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1102"/>
        <source>Tap zones</source>
        <translation>Klik zones</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1115"/>
        <source>Short tap</source>
        <translation>Korte klik</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1120"/>
        <source>Long tap</source>
        <translation>Lange klik</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1149"/>
        <source>Swipes</source>
        <translation>Vegen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1156"/>
        <source>Left to right</source>
        <translation>van links naar rechts</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1161"/>
        <source>Right to left</source>
        <translation>van rechts naar links</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1166"/>
        <source>Top to bottom</source>
        <translation>van boven naar beneden</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1171"/>
        <source>Bottom to top</source>
        <translation>van beneden naar boven</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1196"/>
        <source>Keys</source>
        <translation>Toetsen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1439"/>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1446"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="179"/>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="181"/>
        <source>Algorythmic</source>
        <translation>Algoritme</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="189"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation>Filmquiz bracht knappe ex-yogi van de wijs.</translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.ui" line="20"/>
        <location filename="../tocdlg.ui" line="26"/>
        <source>Table of contents</source>
        <translation>Inhoudsopgave</translation>
    </message>
</context>
</TS>
