<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk_SK">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.ui" line="20"/>
        <location filename="../bookmarklistdlg.ui" line="26"/>
        <source>Bookmarks</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Position</source>
        <translation>Pozícia</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="86"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="434"/>
        <source>Error while opening document </source>
        <translation>Chyba pri otváraní dokumentu</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.ui" line="20"/>
        <source>Document properties</source>
        <translation>Vlastnosti dokumentu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="64"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="132"/>
        <source>Archive name</source>
        <translation>Meno archívu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="133"/>
        <source>Archive path</source>
        <translation>Cesta k archívu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="134"/>
        <source>Archive size</source>
        <translation>Veľkosť archívu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="135"/>
        <source>File name</source>
        <translation>Názov súboru</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="136"/>
        <source>File path</source>
        <translation>Cesta k súboru</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="137"/>
        <source>File size</source>
        <translation>Veľkosť súboru</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="138"/>
        <source>File format</source>
        <translation>Typ súboru</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="140"/>
        <source>File info</source>
        <translation>Info o súbore</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="142"/>
        <source>Title</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="143"/>
        <source>Author(s)</source>
        <translation>Autor(i)</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="144"/>
        <source>Series name</source>
        <translation>Meno série</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="145"/>
        <source>Series number</source>
        <translation>Číslo série</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="146"/>
        <source>Date</source>
        <translation>Dátum</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="147"/>
        <source>Genres</source>
        <translation>Žáner</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="148"/>
        <source>Translator</source>
        <translation>Prekladač</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="149"/>
        <source>Book info</source>
        <translation>Info o knihe</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="151"/>
        <source>Document author</source>
        <translation>Autor dokumentu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="152"/>
        <source>Document date</source>
        <translation>Dátum dokumentu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="153"/>
        <source>Document source URL</source>
        <translation>Zdroj dokumentu URL</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="154"/>
        <source>OCR by</source>
        <translation>OCR by</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="155"/>
        <source>Document version</source>
        <translation>Verzia dokumentu</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="156"/>
        <source>Document info</source>
        <translation>Info o dokumente</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="158"/>
        <source>Publication name</source>
        <translation>Názov publikácie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="159"/>
        <source>Publisher</source>
        <translation>Vydavateľ</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="160"/>
        <source>Publisher city</source>
        <translation>Vydavateľ mesto</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="161"/>
        <source>Publication year</source>
        <translation>Vydavateľ rok</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="162"/>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="163"/>
        <source>Publication info</source>
        <translation>Info o publikácii</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="165"/>
        <source>Custom info</source>
        <translation>Vlastné informácie</translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="../gotodialog.ui" line="14"/>
        <location filename="../gotodialog.ui" line="20"/>
        <source>Position</source>
        <translation>Pozícia</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="52"/>
        <source>Page</source>
        <translation>Strana</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="96"/>
        <source>Go</source>
        <translation>Choď na</translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="103"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="498"/>
        <source>Navigation</source>
        <translation>Navigácia</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Bookmark created</source>
        <translation>Vytvorená záložka</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Not found</source>
        <translation>Nenájdené</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Search pattern is not found in document</source>
        <translation>Hľadaný výraz nebol nájdený</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Delete</source>
        <translation>Vymazať</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Do you really want to delete the current document?</source>
        <translation>Naozaj si prajete si vymazať tento dokument?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Removed</source>
        <translation>Odstránené</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>Document was removed</source>
        <translation>Dokument bol odstránený</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Citation is saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="894"/>
        <source>Pages remain: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="902"/>
        <source>Auto paging is off.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>Auto paging is on.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="41"/>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <location filename="../mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Zavrieť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Next page</source>
        <translation>Nová strana</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>Previous page</source>
        <translation>Predchádzajúca strana</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>Back</source>
        <translation>Späť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="72"/>
        <source>Forward</source>
        <translation>Dopredu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="77"/>
        <source>Table of contents</source>
        <translation>Osnova</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>Recent books</source>
        <translation>Prechádzajúce knihy</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <location filename="../mainwindow.ui" line="98"/>
        <source>Add bookmark</source>
        <translation>Pridať záložku</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="103"/>
        <source>Bookmarks</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <source>File properties</source>
        <translation>Vlastnosti súboru</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <location filename="../mainwindow.ui" line="116"/>
        <source>Find text</source>
        <translation>Nájdi text</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="121"/>
        <source>Show menu</source>
        <translation>Zobraz menu</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Empty</source>
        <translation>Prázdny
</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Front light</source>
        <translation>Osvetlenie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <source>Screen rotation</source>
        <translation>Otočenie obrazovky</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="153"/>
        <source>Toggle frontlight</source>
        <translation>Zapnúť osvetlenie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Frontlight &gt;</source>
        <translation>Osvetlenie &gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="163"/>
        <source>Frontlight &lt;</source>
        <translation>Osvetlenie &lt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="168"/>
        <source>First page</source>
        <translation>Prvá strana</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Last page</source>
        <translation>Posledná strana</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="178"/>
        <source>Next chapter</source>
        <translation>Ďalšia kapitola</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Previous chapter</source>
        <translation>Predchádzajúca kapitola</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="188"/>
        <source>Next 10 pages</source>
        <translation>Dalšich 10 strán</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="193"/>
        <source>Previous 10 pages</source>
        <translation>Predošlých 10 strán</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="198"/>
        <source>Zoom In</source>
        <translation>Priblížiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Zoom Out</source>
        <translation>Oddialiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <location filename="../mainwindow.ui" line="211"/>
        <source>Position</source>
        <translation>Pozícia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="216"/>
        <source>Toggle inversion</source>
        <translation>Zapnúť inverzne</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Toggle header</source>
        <translation>Zapnúť hlavičku</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="226"/>
        <source>Suspend</source>
        <translation>Uspať</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>Screen rotation 0</source>
        <translation>Otočenie obrazovky 0</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Screen rotation 90</source>
        <translation>Otočenie obrazovky 90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="241"/>
        <source>Screen rotation 180</source>
        <translation>Otočenie obrazovky 180</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="246"/>
        <source>Screen rotation 270</source>
        <translation>Otočenie obrazovky 270</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="251"/>
        <source>Screen rotation +90</source>
        <translation>Otočenie obrazovky +90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Screen rotation -90</source>
        <translation>Otočenie obrazovky -90</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>Screen rotation +180</source>
        <translation>Otočenie obrazovky +180</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>Delete current document</source>
        <translation>Vymazať aktuálny dokument</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="271"/>
        <source>Dictionary</source>
        <translation>Slovník</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="276"/>
        <source>Refresh screen</source>
        <translation>Refrešovať obrazovku</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="281"/>
        <source>Screen shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="291"/>
        <source>Pages remain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="296"/>
        <source>Auto paging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <source>Open last book</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../navigationbar.cpp" line="92"/>
        <location filename="../navigationbar.cpp" line="109"/>
        <source>%1-%2 (total %3)</source>
        <translation>%1-%2 (celkom %3)</translation>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <location filename="../openfiledlg.ui" line="20"/>
        <location filename="../openfiledlg.ui" line="26"/>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.ui" line="26"/>
        <location filename="../recentdlg.ui" line="32"/>
        <source>Recent books</source>
        <translation>Nedávne knihy</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Removed</source>
        <translation>Odstránené</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="221"/>
        <source>Document was removed</source>
        <translation>Dokument bol odstránený</translation>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <location filename="../recentremovedlg.ui" line="14"/>
        <source>Delete</source>
        <translation>Vymazať</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="20"/>
        <source>Delete record only</source>
        <translation>Vymazať len záznam</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="27"/>
        <source>Delete record and file</source>
        <translation>Vymazať záznam a súbor</translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="47"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="20"/>
        <location filename="../searchdlg.ui" line="26"/>
        <source>Search</source>
        <translation>Vyhladať</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="51"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="61"/>
        <source>Case sensitive</source>
        <translation>Rozlyšovať veľkosť</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="68"/>
        <source>Search forward</source>
        <translation>Hladať dopredu</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="81"/>
        <source>Search backward</source>
        <translation>Hladať dozadu</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Nastavenie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="42"/>
        <source>Window</source>
        <translation>Okno</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Startup</source>
        <translation>Pri spustení</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="83"/>
        <source>Recent book</source>
        <translation>Nedávna kniha</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="88"/>
        <source>Recent books list</source>
        <translation>Zoznam nedávnych kníh</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="93"/>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="98"/>
        <source>Do nothing</source>
        <translation>Nerob nič</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Formatting</source>
        <translation>Formátovanie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="114"/>
        <source>Disable txt auto formatting</source>
        <translation>Zrušiť auto formátovanie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="121"/>
        <source>Internal styles</source>
        <translation>Interné stýly</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="128"/>
        <source>Embedded fonts</source>
        <translation>Platené fonty</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="138"/>
        <source>Launcher settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <source>Page</source>
        <translation>Strana</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="191"/>
        <source>Header</source>
        <translation>Hlavička</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="197"/>
        <location filename="../settings.ui" line="369"/>
        <source>Show</source>
        <translation>Zobraz</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="598"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="612"/>
        <source>Face</source>
        <translation>Tvár</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="236"/>
        <location filename="../settings.ui" line="635"/>
        <source>Size</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="268"/>
        <source>Elements</source>
        <translation>Elementy</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="274"/>
        <source>Chapter marks</source>
        <translation>Značky kapitoly</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="281"/>
        <source>Book name</source>
        <translation>Názov knihy</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="288"/>
        <source>Clock</source>
        <translation>Hodiny</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>Position percent</source>
        <translation>Pozícia percentá</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="302"/>
        <source>Page number/count</source>
        <translation>Strana knihy/počet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="317"/>
        <source>Battery status</source>
        <translation>Stav batérie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="324"/>
        <source>In percent</source>
        <translation>V percentách</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="363"/>
        <source>Footer</source>
        <translation>Spodok strany</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="390"/>
        <source>Misc</source>
        <translation>Všeobecné</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="407"/>
        <source>View Mode</source>
        <translation>Mód prezerania</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="421"/>
        <source>One page</source>
        <translation>Jedna strana</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="426"/>
        <source>Two pages</source>
        <translation>Dve strany</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="434"/>
        <source>Update interval</source>
        <translation>Interval obnovy</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="521"/>
        <source>Auto paging, sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="542"/>
        <source>Negative</source>
        <translation>Negatív</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="569"/>
        <source>Styles</source>
        <translation>Štýly</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="671"/>
        <source>Gamma</source>
        <translation>Jas</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="716"/>
        <source>Aa</source>
        <translation>Aa</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="730"/>
        <location filename="../settings.ui" line="762"/>
        <source>Disabled</source>
        <translation>Vypnuté</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="735"/>
        <source>On for large fonts</source>
        <translation>Zapnuté pre veľké ronty</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="740"/>
        <source>On for all fonts</source>
        <translation>Pre všetky fonty</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="748"/>
        <source>Hinting</source>
        <translation>Pomocník</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="767"/>
        <source>Bytecode</source>
        <translation>Bytecode</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="772"/>
        <source>Auto</source>
        <translation>Automaticky</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="790"/>
        <source>Bold</source>
        <translation>Hrubé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="803"/>
        <source>Kerning</source>
        <translation>Prekladanie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="813"/>
        <source>Layout</source>
        <translation>Layout</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="821"/>
        <source>Hyphenation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="841"/>
        <source>Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="880"/>
        <location filename="../settings.ui" line="1390"/>
        <source>Space</source>
        <translation>Medzera</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="921"/>
        <source>Floating punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="942"/>
        <source>Margins</source>
        <translation>Okraje</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="948"/>
        <source>Top</source>
        <translation>Vrch</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="974"/>
        <source>Bottom</source>
        <translation>Spodok</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1000"/>
        <source>Left</source>
        <translation>Vľavo</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1026"/>
        <source>Right</source>
        <translation>Vpravo</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1073"/>
        <source>Control</source>
        <translation>Ovládanie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1102"/>
        <source>Tap zones</source>
        <translation>Zóny tabulátorov</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1115"/>
        <source>Short tap</source>
        <translation>Krátky tabulátor</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1120"/>
        <source>Long tap</source>
        <translation>Dlhý tabulátor</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1149"/>
        <source>Swipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1156"/>
        <source>Left to right</source>
        <translation>Zľava do prava</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1161"/>
        <source>Right to left</source>
        <translation>Z prava do ľava</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1166"/>
        <source>Top to bottom</source>
        <translation>Z hora dolu</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1171"/>
        <source>Bottom to top</source>
        <translation>Z dola hore</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1196"/>
        <source>Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1439"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1446"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="179"/>
        <source>Off</source>
        <translation>Vyp</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="181"/>
        <source>Algorythmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="189"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.ui" line="20"/>
        <location filename="../tocdlg.ui" line="26"/>
        <source>Table of contents</source>
        <translation type="unfinished">Obsah</translation>
    </message>
</context>
</TS>
