#include "Application.h"
#include "widget.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.setApplicationName("wifi_server");
    if ( !app.init() ) return 1;
    Widget w;
    return app.exec();
}
