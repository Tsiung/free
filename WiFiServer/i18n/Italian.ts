<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>WiFi Server</source>
        <translation>Server WiFi</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="20"/>
        <source>&lt;b&gt;WiFi Server&lt;/b&gt;</source>
        <translation>&lt;b&gt;Server WiFi&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="44"/>
        <source>WiFi Server is started...</source>
        <translation>Il server WiFi si sta avviando...</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="48"/>
        <source>IP Address: </source>
        <translation>Indirizzo IP: </translation>
    </message>
</context>
</TS>
