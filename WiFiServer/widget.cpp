#include <netdb.h>

#include <QTimer>

#include "Platform.h"
#include "QtUtils.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)
    , fifo(qgetenv("VLASOVSOFT_FIFO1"))

{
    ui->setupUi(this);
    initTopLevelWidget(this);

    QObject::connect(&activityTimer, SIGNAL(timeout()), this, SLOT(activity()));
    activityTimer.setInterval(30000);
    activityTimer.start();

    QTimer::singleShot(0, this, SLOT(init()));

    showFullScreen();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnExit_clicked()
{
    close();
}

void Widget::init()
{
    if ( !activateNetwork(this) ) {
        close();
    }

    ui->textBrowser->append(tr("WiFi Server is started..."));

    char ip[NI_MAXHOST];
    if ( Platform::get()->isNetworkActive( ip ) )
        ui->textBrowser->append(tr("IP Address: ") + "<b>" + ip + "</b>");
}

void Widget::activity()
{
    // prevent wifi off
    Platform::get()->networkActivity();
    // prevent sleep mode
    ::writeFifoCommand(fifo, "activity");
}
