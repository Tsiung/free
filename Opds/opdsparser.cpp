#include <QUrl>
#include <QDebug>
#include <QApplication>

#include "opdsparser.h"

OpdsParser::OpdsParser(QVector<OpdsParser::Item>& items_, const QString& bu)
    : items(items_)
    , baseUrl(bu)
    , inEntry(false)
    , inContent(false)
    , inTitle(false)
{
}

bool OpdsParser::startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &attributes)
{   
    Q_UNUSED(namespaceURI)
    Q_UNUSED(qName)

    if ( inEntry )
    {
        if ( inContent )
        {
            item.content += "<" + localName + ">";
        }
        else
        if ( localName == "title" )
        {
            inTitle = true;
        }
        else if ( localName == "content" )
        {
            item.cType = attributes.value("type");
            inContent = true;
        }
        else
        if ( localName == "link" )
        {
            QString type(attributes.value("type"));
            QString rel(attributes.value("rel"));
            if ( type.startsWith("application/atom+xml") )
                type = "atom";
            else
            if (    type == "application/fb2+zip"
                 || type == "application/fb-ebook"
                 || type == "text/fb2+xml")
                type = "fb2";
            else
            if ( type == "application/html+zip" )
                type = "html";
            else
            if ( type == "application/txt+zip" )
                type = "txt";
            else
            if ( type == "application/rtf+zip" )
                type = "rtf";
            else
            if ( type == "application/epub+zip" )
                type = "epub";
            else
                type.clear();

            if (    !type.isEmpty()
                 && item.urls.find(type) == item.urls.end() )
            {
                QString u(attributes.value("href"));
                QUrl url(makeAbsoluteUrl(u));
                item.urls.insert( type, url.toString() );
            }
        }
    }
    else
    if ( localName == "entry" )
    {
        item = Item();
        inEntry = true;
    }
    else
    if ( localName == "link" )
    {
        QString type(attributes.value("type"));
        QString rel(attributes.value("rel"));
        if (    type.startsWith("application/atom+xml")
             && rel == "next" )
        {
            QString u(attributes.value("href"));
            nextUrl = makeAbsoluteUrl(u);
        }
    }

    return true;
}

bool OpdsParser::endElement(const QString &namespaceURI, const QString &localName, const QString &qName)
{
    Q_UNUSED(namespaceURI)
    Q_UNUSED(qName)

    if ( inEntry )
    {
        if ( inTitle )
        {
            if ( localName == "title" )
                inTitle = false;
        }
        if ( inContent )
        {
            if ( localName != "content" )
                item.content += "</" + localName + ">";
            else
                inContent = false;
        }
        if ( localName == "entry" )
        {
            items.push_back(item);
            inEntry = false;
        }
    }

    return true;
}

bool OpdsParser::characters(const QString &str)
{
    if ( inEntry )
    {
        if ( inTitle )
        {
            item.name += str;
        }
        else
        if ( inContent )
        {
            item.content += str;
        }
    }

    return true;
}

bool OpdsParser::endDocument()
{
    if ( nextUrl.isValid() )
    {
        Item item;
        item.name = qApp->translate("opdsparser", "===> Next");
        item.urls.insert("next", nextUrl.toString());
        items.push_back(item);
    }
    return QXmlDefaultHandler::endDocument();
}

bool OpdsParser::fatalError(const QXmlParseException&)
{
    return false;
}

QString OpdsParser::errorString() const
{
    return QString();
}

QUrl OpdsParser::makeAbsoluteUrl(const QUrl &u) const
{
    return u.isRelative() ? baseUrl.resolved( u ) : u;
}
