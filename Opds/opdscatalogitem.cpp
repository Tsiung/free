#include "opdscatalogitem.h"

OpdsCatalogItem::OpdsCatalogItem ( const QString& str )
{
    QStringList l( str.split( '|' ) );
    if ( l.size() >= 2 )
    {
        setText(l.at(0).trimmed());
        urls.insert("atom", l.at(1).trimmed());
    }
}

OpdsCatalogItem::OpdsCatalogItem ( const QString& n, const QString& u )
    : QListWidgetItem(n)
{
    urls.insert("atom", u);
}

OpdsCatalogItem::OpdsCatalogItem(const QString &n, const QMap<QString, QString> &u)
    : QListWidgetItem(n)
    , urls(u)
{
}
