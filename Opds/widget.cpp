#include <QDir>
#include <QKeyEvent>
#include <QDebug>
#include <QTextStream>

#include "Config.h"
#include "QtUtils.h"
#include "opdscatalog.h"
#include "Platform.h"
#include "editcatalog.h"
#include "opdsitemwidget.h"
#include "opdscatalogitem.h"
#include "opdsbookwidget.h"

#include "widget.h"
#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)
{
    ui->setupUi(this);
    initTopLevelWidget(this);
    loadCatalogs();
    show();
}

Widget::~Widget()
{
    saveCatalogs();
    delete ui;
}

void Widget::loadCatalogs()
{
    QFile file( Platform::get()->getUserSettingsPath() + QDir::separator() + "catalogs.txt" );
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        QTextStream in(&file);
        in.setCodec("UTF-8");
        while(!in.atEnd())
        {
            QString line = in.readLine();
            OpdsCatalogItem* pItem = new OpdsCatalogItem( line );
            ui->listOpdsCatalogs->addItem( pItem );
        }
    }
}

void Widget::saveCatalogs()
{
    if ( 0 == ui->listOpdsCatalogs->count() ) return;
    QFile file( Platform::get()->getUserSettingsPath() + QDir::separator() + "catalogs.txt" );
    if ( file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        QTextStream out(&file);
        for ( int i=0; i<ui->listOpdsCatalogs->count(); ++i )
        {
            OpdsCatalogItem* pItem = static_cast<OpdsCatalogItem*>(ui->listOpdsCatalogs->item(i));
            QMap<QString,QString>::Iterator iter(pItem->urls.find("atom"));
            if ( iter != pItem->urls.end() )
                out << pItem->text() << '|' << iter.value() << '\n';
        }
    }
}

void Widget::keyPressEvent(QKeyEvent *e)
{
    switch ( e->key() )
    {
    case Qt::Key_Back:
        on_btnBack_clicked();
        break;
    case Qt::Key_Home:
        on_btnHome_clicked();
        break;
    default:
        break;
    }
}

void Widget::on_btnAdd_clicked()
{
    QString name, url;
    EditCatalog dlg(this, name, url);
    if ( dlg.exec() )
    {
        ui->listOpdsCatalogs->addItem( new OpdsCatalogItem( name, url ) );
    }
}

void Widget::on_btnEdit_clicked()
{
    OpdsCatalogItem* pItem = static_cast<OpdsCatalogItem*>(ui->listOpdsCatalogs->currentItem());    
    if ( pItem )
    {
        QMap<QString,QString>::Iterator iter( pItem->urls.find("atom") );
        if ( iter != pItem->urls.end() )
        {
            QString name(pItem->text());
            QString url(iter.value());
            EditCatalog dlg(this, name, url);
            if ( dlg.exec() )
            {
                pItem->setText(name);
                iter.value() = url;
            }
        }
    }
}

void Widget::on_listOpdsCatalogs_itemClicked(QListWidgetItem *item)
{
    if ( item )
    {
       navigate(static_cast<OpdsCatalogItem*>(item));
    }
}

void Widget::on_listOpdsCatalogs_itemActivated(QListWidgetItem *item)
{
    if ( item )
    {
       navigate(static_cast<OpdsCatalogItem*>(item));
    }
}

void Widget::navigate(OpdsCatalogItem* pItem)
{
    if ( pItem->urls.isEmpty() ) return;

    if ( !::activateNetwork( this ) ) {
        return;
    }

    bool isBook = false;
    QMap<QString, QString>::const_iterator i = pItem->urls.begin();
    for ( ; i != pItem->urls.end(); ++i )
    {
        if (    i.key() == "fb2"
             || i.key() == "html"
             || i.key() == "txt"
             || i.key() == "rtf"
             || i.key() == "epub" )
        {
            OpdsBookWidget* p = new OpdsBookWidget( this, pItem );
            p->setContent( pItem->cType, pItem->content );
            ui->stackedWidget->addWidget( p );
            ui->stackedWidget->setCurrentWidget( p );
            isBook = true;
            break;
        }
    }

    if ( !isBook )
    {
        QMap<QString,QString>::Iterator iter(pItem->urls.find("atom"));
        if ( iter != pItem->urls.end() )
        {
            OpdsItemWidget* p = new OpdsItemWidget( this, iter.value() );
            ui->stackedWidget->addWidget( p );
            ui->stackedWidget->setCurrentWidget( p );
            QObject::connect( p, SIGNAL(sigNavigate(OpdsCatalogItem*)), this, SLOT(navigate(OpdsCatalogItem*)) );
        }
    }
}

void Widget::on_btnBack_clicked()
{
    if ( ui->stackedWidget->count() > 1 )
    {
        ui->stackedWidget->removeWidget( ui->stackedWidget->currentWidget() );
        ui->stackedWidget->setCurrentIndex( ui->stackedWidget->count() - 1 );
    }
}

void Widget::on_btnHome_clicked()
{
    while ( ui->stackedWidget->count() > 1 )
    {
        ui->stackedWidget->removeWidget( ui->stackedWidget->currentWidget() );
        ui->stackedWidget->setCurrentIndex( ui->stackedWidget->count() - 1 );
    }
}

void Widget::on_btnRemove_clicked()
{
    int row = ui->listOpdsCatalogs->currentRow();
    if ( row >= 0 )
    {
        QListWidgetItem* pItem = ui->listOpdsCatalogs->takeItem(row);
        delete pItem;
    }
}
