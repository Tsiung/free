#ifndef OPDSBOOKWIDGET_H
#define OPDSBOOKWIDGET_H

#include <QWidget>

namespace Ui {
class OpdsBookWidget;
}

class QListWidgetItem;
class OpdsCatalogItem;

class OpdsBookWidget : public QWidget
{
    Q_OBJECT

    OpdsCatalogItem* pItem;

public:
    OpdsBookWidget(QWidget *parent, OpdsCatalogItem* p);
    ~OpdsBookWidget();

    void setContent( const QString& type, const QString& content );

private:
    Ui::OpdsBookWidget *ui;

private slots:
    void listItemClicked(QListWidgetItem *item);
};

#endif // OPDSBOOKWIDGET_H
