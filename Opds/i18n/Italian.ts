<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadDialog</name>
    <message>
        <location filename="../downloaddialog.ui" line="14"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../downloaddialog.ui" line="53"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>EditCatalog</name>
    <message>
        <location filename="../editcatalog.ui" line="14"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../editcatalog.ui" line="20"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../editcatalog.ui" line="30"/>
        <source>Url</source>
        <translation>URL</translation>
    </message>
</context>
<context>
    <name>OpdsBookWidget</name>
    <message>
        <location filename="../opdsbookwidget.cpp" line="101"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../opdsbookwidget.cpp" line="101"/>
        <source>Download error!</source>
        <translation>Errore download!</translation>
    </message>
</context>
<context>
    <name>OpdsItemWidget</name>
    <message>
        <location filename="../opdsitemwidget.ui" line="21"/>
        <source>Loading...</source>
        <translation>Caricamento...</translation>
    </message>
    <message>
        <location filename="../opdsitemwidget.cpp" line="66"/>
        <location filename="../opdsitemwidget.cpp" line="76"/>
        <source>Error!</source>
        <translation>Errore!</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="44"/>
        <source>OPDS Catalogs</source>
        <translation>Cataloghi OPDS</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="56"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="63"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="70"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="87"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="94"/>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="101"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>opdsparser</name>
    <message>
        <location filename="../opdsparser.cpp" line="147"/>
        <source>===&gt; Next</source>
        <translation type="unfinished">===&gt; Prossimo</translation>
    </message>
</context>
</TS>
