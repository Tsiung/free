#include <QDir>
#include <QKeyEvent>
#include <QDebug>

#include "QtUtils.h"
#include "Platform.h"
#include "opdsparser.h"
#include "opdscatalogitem.h"

#include "opdsitemwidget.h"
#include "ui_opdsitemwidget.h"

OpdsItemWidget::OpdsItemWidget(QWidget* parent, const QString& url)
    : QWidget(parent)
    , downloader(this)
    , pNext(NULL)
    , success(false)
    , ui(new Ui::OpdsItemWidget)
{
    ui->setupUi(this);
    initTopLevelWidget(this);

    download(url);

    QObject::connect( ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(listItemClicked(QListWidgetItem*)) );
    QObject::connect( ui->listWidget, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(listItemClicked(QListWidgetItem*)) );
}

OpdsItemWidget::~OpdsItemWidget()
{
    downloader.cancel();
    delete ui;
}

void OpdsItemWidget::downloadFinished()
{
    // success! parse xml
    QVector<OpdsParser::Item> items;
    OpdsParser handler(items, downloader.getTarget());
    QXmlSimpleReader reader;
    reader.setContentHandler(&handler);
    reader.setErrorHandler(&handler);
    QString path( Platform::get()->getRootPath() + QDir::separator() + "opds.xml" );
    success = reader.parse(new QXmlInputSource(new QFile( path )));

    // remove last item
    int count = ui->listWidget->count();
    if ( count > 0 )
    {
        delete (ui->listWidget->takeItem( count-1 ));
    }

    if ( !items.empty() )
    {
        // fill list
        foreach (const OpdsParser::Item& item, items)
        {
            OpdsCatalogItem* pItem = new OpdsCatalogItem( item.name, item.urls );
            pItem->content = item.content;
            pItem->cType   = item.cType;
            ui->listWidget->addItem( pItem );
        }
    }
    else
    {
        ui->listWidget->addItem(tr("Error!"));
    }
}

void OpdsItemWidget::downloadError()
{
    qDebug("downloadError()");
    QListWidgetItem* pItem = ui->listWidget->item(0);
    if ( pItem )
    {
        pItem->setText(tr("Error!"));
    }
}

void OpdsItemWidget::downloadProgress(int curr, int total)
{
    if ( total > 0 )
    {
        // find item to display progress
        int size = ui->listWidget->count();
        if ( size > 0 )
        {
            QListWidgetItem* pItem = ui->listWidget->item(size-1);
            pItem->setText(QString("Progress: %1").arg(curr * 100 / total));
        }
    }
}

void OpdsItemWidget::listItemClicked(QListWidgetItem *item)
{
    if ( success )
    {
        OpdsCatalogItem* pItem = static_cast<OpdsCatalogItem*>(item);
        QMap<QString,QString>::const_iterator iter(pItem->urls.find("next"));
        if ( iter != pItem->urls.end() )
        {
            download( iter.value() );
        }
        else
        {
            emit sigNavigate( static_cast<OpdsCatalogItem*>(item) );
        }
    }
}

void OpdsItemWidget::download( const QString& url )
{
    downloader.setFileName(Platform::get()->getRootPath() + QDir::separator() + "opds.xml");
    downloader.setTarget(url);
    downloader.download();
}
