<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>SettingsDlg</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Dialoog</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="14"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="20"/>
        <source>Show hidden files</source>
        <translation>Toon verborgen bestanden</translation>
    </message>
</context>
<context>
    <name>WaitDlg</name>
    <message>
        <location filename="../waitdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../waitdlg.ui" line="20"/>
        <source>Please wait</source>
        <translation>Even geduld a.u.b.</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="17"/>
        <source>Open file</source>
        <translation>Open bestand</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="180"/>
        <location filename="../widget.cpp" line="285"/>
        <source>Create folder</source>
        <translation>Maak folder</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="185"/>
        <location filename="../widget.ui" line="188"/>
        <location filename="../widget.cpp" line="294"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <source>Select all</source>
        <translation>Selecteer alles</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="198"/>
        <source>Clear selection</source>
        <translation>Verwijder selectie</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="203"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="208"/>
        <source>Cut</source>
        <translation>Knippen</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="213"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="218"/>
        <location filename="../widget.cpp" line="396"/>
        <source>Rename</source>
        <translation>Hernoemen</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="223"/>
        <location filename="../widget.cpp" line="416"/>
        <source>Clone</source>
        <translation>Klonen</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="228"/>
        <source>Add bookmark</source>
        <translation>Toevoegen bladwijzer</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="233"/>
        <source>Remove bookmark</source>
        <translation>Verwijder bladwijzer</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="238"/>
        <source>Set as home</source>
        <translation>Maak default home</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="233"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="294"/>
        <source>Are you sure?</source>
        <translation>Weet U het zeker?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="447"/>
        <source>Bookmark</source>
        <translation>Bladwijzer</translation>
    </message>
</context>
</TS>
