<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES" sourcelanguage="en">
<context>
    <name>SettingsDlg</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Quadre</translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdlg.ui" line="20"/>
        <source>Show hidden files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaitDlg</name>
    <message>
        <location filename="../waitdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Quadre</translation>
    </message>
    <message>
        <location filename="../waitdlg.ui" line="20"/>
        <source>Please wait</source>
        <translation>Si us plau, espera</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="17"/>
        <source>Open file</source>
        <translation>Obrir arxiu</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="180"/>
        <location filename="../widget.cpp" line="285"/>
        <source>Create folder</source>
        <translation>Nova carpeta</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="185"/>
        <location filename="../widget.ui" line="188"/>
        <location filename="../widget.cpp" line="294"/>
        <source>Delete</source>
        <translation>Esborrar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <source>Select all</source>
        <translation>Selecciona-ho tot</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="198"/>
        <source>Clear selection</source>
        <translation>Eliminar selecció</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="203"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="208"/>
        <source>Cut</source>
        <translation>Retallar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="213"/>
        <source>Paste</source>
        <translation>Enganxar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="218"/>
        <location filename="../widget.cpp" line="396"/>
        <source>Rename</source>
        <translation>Canviar nom</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="223"/>
        <location filename="../widget.cpp" line="416"/>
        <source>Clone</source>
        <translation>Clonar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="228"/>
        <source>Add bookmark</source>
        <translation>Afegir marcador</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="233"/>
        <source>Remove bookmark</source>
        <translation>Eliminar marcador</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="238"/>
        <source>Set as home</source>
        <translation>Establir com a inici</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="233"/>
        <source>Bookmarks</source>
        <translation>Marcadors</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="294"/>
        <source>Are you sure?</source>
        <translation>Estàs segur?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="447"/>
        <source>Bookmark</source>
        <translation>Marcador</translation>
    </message>
</context>
</TS>
