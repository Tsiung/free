#include <QDebug>
#include <QTextDocumentWriter>
#include <QKeyEvent>
#include <QProcess>

#include "Config.h"
#include "QtUtils.h"
#include "messagebox.h"
#include "Platform.h"

#include "waitdlg.h"
#include "EnterTextLineDialog.h"
#include "widget.h"
#include "settingsdlg.h"

#include "ui_widget.h"

Widget::Widget()
    : ui(new Ui::Widget)
    , iconFile(iconPath("file.png"))
    , iconFolder(iconPath("folder.png"))
{
    ui->setupUi(this);
    int val = g_pConfig->readInt( "list_icon_size", 0 );
    if ( val > 0 )
    {
        ui->fileList->setIconSize( QSize( val, val ) );
    }
    val = g_pConfig->readInt( "list_font_size", 0 );
    if ( val > 0 )
    {
        QFont fnt(ui->fileList->font());
        fnt.setPixelSize( val );
        ui->fileList->setFont(fnt);
    }

    initTopLevelWidget(this);

    setupModelFilter();
    model.setIconProvider(this);
    model.setNameFilterDisables(false);
    model.setRootPath("/");
    ui->fileList->setModel( &model );
    ui->fileList->setRootIndex(model.index("/"));
    ui->fileList->setSelectionMode( QAbstractItemView::SingleSelection );

    QObject::connect( ui->fileList, SIGNAL(clicked(QModelIndex)), this, SLOT(onClicked(QModelIndex)));
    QObject::connect( ui->fileList, SIGNAL(activated(QModelIndex)), this, SLOT(onClicked(QModelIndex)));
    QObject::connect(&idleTimer,SIGNAL(timeout()),this,SLOT(onIdle()));

    ui->btnUp->setIcon( QIcon( iconPath("up.png") ) );
    ui->btnCancel->setIcon( QIcon( iconPath("close.png") ) );
    ui->btnMode->setIcon( QIcon( iconPath( "checkbox.png" ) ) );
    ui->btnMenu->setIcon( QIcon( iconPath( "menu.png" ) ) );
    ui->btnHome->setIcon( QIcon( iconPath( "home.png" ) ) );
    ui->btnSettings->setIcon( QIcon( iconPath( "settings.png" ) ) );

    restoreBookmarks();

    mainMenu = createMainMenu();

    idleTimer.setSingleShot(true);

    home();

    ui->fileList->setFocus();

    show();
}

Widget::~Widget()
{
    delete ui;
    storeBookmarks();
}

QIcon Widget::icon( IconType type ) const
{
    switch ( type )
    {
    case QFileIconProvider::Folder:
        return iconFolder;
        break;
    case QFileIconProvider::File:
        return iconFile;
        break;
    default:
        return QFileIconProvider::icon( type );
    }
}

QIcon Widget::icon ( const QFileInfo& info ) const
{
    if ( info.isFile() )
        return iconFile;
    else
    if ( info.isDir() )
        return iconFolder;
    else
        return QFileIconProvider::icon( info );
}

void Widget::on_btnCancel_clicked()
{
    close();
}

void Widget::onClicked(const QModelIndex &index)
{
    if ( ui->fileList->selectionMode() != QAbstractItemView::SingleSelection )
        return;

    if ( index.isValid() )
    {
        if ( model.isDir( index ) )
        {
            changeDir( index );
            clearSelection();
        }
    }
}

QString Widget::iconPath(const QString &fn)
{
    return Platform::get()->getRootPath() + QDir::separator() + "images" + QDir::separator() +  fn;
}

void Widget::makeFileList(QSet<QString> &files)
{
    if ( QAbstractItemView::NoSelection == ui->fileList->selectionMode() )
    {
        QModelIndex idx( ui->fileList->currentIndex() );
        if ( idx.isValid() )
        {
            files.insert( model.fileInfo( idx ).absoluteFilePath() );
        }
    }
    else
    {
        foreach( const QModelIndex &index, ui->fileList->selectionModel()->selectedIndexes() )
        {
            QFileInfo fi( model.fileInfo( index ) );
            files.insert( fi.absoluteFilePath() );
        }
    }
}

QMenu* Widget::createMainMenu()
{
    QMenu* menu = new QMenu(this);
    menu->addAction( ui->actionSelectAll );
    menu->addAction( ui->actionClearSelection );
    menu->addSeparator();
    menu->addAction( ui->actionCopy );
    menu->addAction( ui->actionCut );
    menu->addAction( ui->actionPaste );
    menu->addSeparator();
    menu->addAction( ui->createFolder );
    menu->addAction( ui->actionRename );
    menu->addAction( ui->actionClone );
    menu->addAction( ui->actionDelete );
    menu->addSeparator();
    menu->addAction( ui->actionAddBookmark );
    menu->addAction( ui->actionRemoveBookmark );
    menu->addAction( ui->actionSetAsHome );
    return menu;
}

void Widget::runCommand(const QString &cmd)
{
    QProcess* process = new QProcess();
    process->start( cmd );
    WaitDlg dlg(this);
    QObject::connect( process, SIGNAL(finished(int)), &dlg, SLOT(finished(int)) );
    qDebug() << cmd;
    dlg.exec();
    clearSelection();
}

void Widget::home()
{
#if defined(KOBO)
    QString defHome("/mnt/onboard");
#elif defined(KINDLE)
    QString defHome("/mnt/us");
#else
    QString defHome(QFile::decodeName(qgetenv("HOME")));
#endif
    QString homePath( g_pConfig->readQString( "home_path", defHome ) );
    if ( !homePath.isEmpty() )
        changeDir(homePath);
}

void Widget::changeDir(const QModelIndex &path, bool keepBookmarks)
{
    QString absPath( model.fileInfo( path ).absoluteFilePath() );
    QDir::setCurrent( absPath );

    if ( !keepBookmarks )
        ui->cbBookmarks->setCurrentIndex(0);

    ui->fileList->setRootIndex(path);
    ui->btnMode->setChecked(false);

    updatePathText();
}

void Widget::changeDir(const QString &path, bool keepBookmarks)
{
    changeDir(model.index(path), keepBookmarks);
}

QString Widget::getBookmarksFilePath() const
{
    return Platform::get()->getRootPath() + QDir::separator() + "bookmarks.txt";
}

void Widget::storeBookmarks()
{
    QFile file( getBookmarksFilePath() );
    if ( file.open( QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text ) )
    {
        QTextStream out(&file);
        for ( int i=1; i<ui->cbBookmarks->count(); ++i )
        {
            out << ui->cbBookmarks->itemText(i) << ":" << ui->cbBookmarks->itemData(i).toString() << '\n';
        }
    }
}

void Widget::restoreBookmarks()
{
    ui->cbBookmarks->addItem(tr("Bookmarks"));
    QFile file( getBookmarksFilePath() );
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QStringList lst( in.readLine().split(':') );
            if ( 2 == lst.size() )
            {
                QFileInfo fi( lst.at(1) );
                if ( fi.isDir() )
                    ui->cbBookmarks->addItem( lst.at(0), lst.at(1) );
            }
        }
    }
    ui->cbBookmarks->setCurrentIndex(0);
}

void Widget::setupModelFilter()
{
    QDir::Filters filter = QDir::AllDirs | QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks;
    if ( g_pConfig->readBool("hidden_files", true) )
        filter = filter | QDir::Hidden;
    model.setFilter(filter);
}

void Widget::on_btnUp_clicked()
{
    QModelIndex oldRoot(ui->fileList->rootIndex());
    QModelIndex newRoot(model.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        changeDir( newRoot );
    }
}

void Widget::on_btnMode_toggled(bool checked)
{
    ui->fileList->setSelectionMode( checked? QAbstractItemView::MultiSelection : QAbstractItemView::SingleSelection );
    clearSelection();
}

void Widget::on_btnMenu_clicked()
{
    QSize size( getSizePx( QSize(20, 20), 5 ) ); // 0.5 cm
    int max = qMax( size.width(), size.height() );
    mainMenu->exec( QPoint( max,max ) );
}

void Widget::on_createFolder_triggered()
{
    EnterTextLineDialog dlg(this, tr("Create folder"), tr("Create folder"));
    if ( dlg.exec() )
    {
        model.mkdir( ui->fileList->rootIndex(), dlg.getValue() );
    }
}

void Widget::on_actionDelete_triggered()
{
    if ( QMessageBox::Yes == ::questionBox( this, tr("Delete"), tr("Are you sure?"), QMessageBox::Yes|QMessageBox::No, QMessageBox::No ) )
    {
        QSet<QString> files;
        makeFileList(files);
        if ( !files.isEmpty() )
        {
            QString cmd("rm -rf ");
            foreach( QString file, files )
            {
                cmd += "\"" + file + "\" ";
            }
            runCommand(cmd);
        }
        ui->btnMode->setChecked( false );
    }
}

void Widget::onIdle()
{
    bool idxValid = ui->fileList->currentIndex().isValid() && ui->fileList->currentIndex() != ui->fileList->rootIndex();
    bool hasSelection = ui->fileList->selectionModel()->hasSelection();

    ui->actionClearSelection->setVisible( hasSelection || idxValid );
    ui->actionDelete->setVisible( hasSelection || idxValid );
    ui->actionCopy->setVisible( hasSelection || idxValid );
    ui->actionCut->setVisible( hasSelection || idxValid );
    ui->actionRename->setVisible( idxValid );
    ui->actionClone->setVisible( idxValid );
    ui->actionPaste->setVisible( !files.isEmpty() );
    ui->cbBookmarks->setVisible( ui->cbBookmarks->count() > 1 );
    ui->actionRemoveBookmark->setVisible( ui->cbBookmarks->currentIndex() > 0 );
}

bool Widget::event(QEvent *e)
{
    int timerId = e->type() == QEvent::Timer ? static_cast<QTimerEvent*>(e)->timerId() : -1;
    if ( -1 == timerId || timerId != idleTimer.timerId() )
    {
        idleTimer.start(50);
    }
    return WidgetCommon::event(e);
}

void Widget::resizeEvent(QResizeEvent* event)
{
    Q_UNUSED(event)
    updatePathText();
}

void Widget::on_actionSelectAll_triggered()
{
    ui->btnMode->setChecked(true);
    ui->fileList->selectAll();
}

void Widget::on_actionClearSelection_triggered()
{
    ui->btnMode->setChecked(false);
    clearSelection();
}

void Widget::on_actionCopy_triggered()
{
    files.clear();
    makeFileList( files );
    clearSelection();
    ui->btnMode->setChecked(false);
    isCut = false;
}

void Widget::on_actionCut_triggered()
{
    files.clear();
    makeFileList( files );
    clearSelection();
    ui->btnMode->setChecked(false);
    isCut = true;
}

void Widget::on_actionPaste_triggered()
{
    if ( !files.isEmpty() )
    {
        QString cmd;
        cmd = isCut? "mv ":"cp -r ";
        foreach( QString file, files )
        {
            cmd += "\"" + file + "\" ";
        }
        cmd += "\"" + model.filePath(ui->fileList->rootIndex());
        cmd += "\"";
        runCommand(cmd);
    }
}

void Widget::on_actionRename_triggered()
{
    QModelIndex curr( ui->fileList->currentIndex() );
    if ( curr.isValid() )
    {
        QFileInfo fi( model.fileInfo( curr ) );
        QString name( fi.fileName() );
        EnterTextLineDialog dlg(this, tr("Rename"), tr("Rename"), name);
        if ( dlg.exec() )
        {
            QString cmd( "mv " );
            cmd += "\"" + fi.absoluteFilePath();
            cmd += "\" \"";
            cmd += fi.absolutePath() + QDir::separator() + dlg.getValue();
            cmd += "\"";
            runCommand(cmd);
        }
    }
}

void Widget::on_actionClone_triggered()
{
    QModelIndex curr( ui->fileList->currentIndex() );
    if ( curr.isValid() )
    {
        QFileInfo fi( model.fileInfo( curr ) );
        QString name( fi.fileName() );
        EnterTextLineDialog dlg(this, tr("Clone"), tr("Clone"), name);
        if ( dlg.exec() )
        {
            QString cmd( "cp -r " );
            cmd += "\"" +fi.absoluteFilePath();
            cmd += "\" \"";
            cmd += fi.absolutePath() + QDir::separator() + dlg.getValue();
            cmd += "\"";
            runCommand(cmd);
        }
    }
}

void Widget::on_btnHome_clicked()
{
    home();
}

void Widget::on_cbBookmarks_currentIndexChanged(int index)
{
    if ( index > 0 )
    {
        QString path( ui->cbBookmarks->itemData( index ).toString() );
        changeDir( path, true );
        ui->fileList->setFocus();
    }
}

void Widget::on_actionAddBookmark_triggered()
{
    QFileInfo fi( model.fileInfo( ui->fileList->rootIndex() ) );
    EnterTextLineDialog dlg(this, tr("Bookmark"), tr("Bookmark"), fi.fileName());
    if ( dlg.exec() )
    {
        QFileInfo fi( model.fileInfo( ui->fileList->rootIndex() ) );
        ui->cbBookmarks->addItem(dlg.getValue(), fi.absoluteFilePath());
    }
}

void Widget::on_actionRemoveBookmark_triggered()
{
    int index = ui->cbBookmarks->currentIndex();
    if ( index > 0 )
    {
        ui->cbBookmarks->setCurrentIndex(0);
        ui->cbBookmarks->removeItem(index);
    }
}

void Widget::on_actionSetAsHome_triggered()
{
    g_pConfig->writeQString( "home_path", model.fileInfo( ui->fileList->rootIndex() ).absoluteFilePath() );
}

void Widget::clearSelection()
{
    ui->fileList->clearSelection();
}

void Widget::updatePathText()
{
    QFontMetrics fm( ui->lblCurrPath->font() );
    QString path( model.fileInfo( ui->fileList->rootIndex() ).absoluteFilePath() );
    QString elidedPath( fm.elidedText( path, Qt::ElideLeft, ui->lblCurrPath->width() ) );
    ui->lblCurrPath->setText( elidedPath );
}

void Widget::on_btnSettings_clicked()
{
    SettingsDlg dlg(this);
    if (dlg.exec())
    {
        setupModelFilter();
    }
}
