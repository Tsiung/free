TARGET = file_manager
TEMPLATE = app
COMMON = ../common

include(../common/common.pri)
include(../common/i18n.pri)

SOURCES += main.cpp widget.cpp waitdlg.cpp \
    settingsdlg.cpp
HEADERS  += widget.h waitdlg.h \
    settingsdlg.h
FORMS    += widget.ui waitdlg.ui \
    settingsdlg.ui

