#ifndef SETTINGSDLG_H
#define SETTINGSDLG_H

#include "Dialog.h"

namespace Ui {
class SettingsDlg;
}

class SettingsDlg : public Dialog
{
    Q_OBJECT

public:
    SettingsDlg(QWidget *parent = 0);
    ~SettingsDlg();

    void accept();

private:
    Ui::SettingsDlg *ui;
};

#endif // SETTINGSDLG_H
