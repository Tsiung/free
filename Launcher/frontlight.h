#ifndef H_FRONTLIGHT
#define H_FRONTLIGHT

void front_light_set_level( int value, int temp );
void front_light_startup();
void front_light_cleanup();

#endif // H_FRONTLIGHT
