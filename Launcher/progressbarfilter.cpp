#include <QMouseEvent>
#include <QProgressBar>

#include "progressbarfilter.h"

static int round( float val )
{
    return (val > 0.0) ? (val + 0.5) : (val - 0.5);
}

ProgressBarFilter::ProgressBarFilter(QObject *parent, int s)
    : QObject(parent)
    , pixelsOrigin(0)
    , progressOrigin(0)
    , step(s)
{
}

bool ProgressBarFilter::eventFilter(QObject* obj, QEvent* e)
{
    bool result = false;
    if (    QEvent::MouseButtonPress   == e->type()
         || QEvent::MouseButtonRelease == e->type()
         || QEvent::MouseMove          == e->type() )
    {
        QProgressBar* pb = qobject_cast<QProgressBar*>(obj);
        if ( pb && pb->isEnabled() )
        {
            QMouseEvent* me = static_cast<QMouseEvent*>(e);
            switch ( e->type() )
            {
            case QEvent::MouseButtonPress:
                pixelsOrigin   = me->pos().x();
                progressOrigin = pb->value();
                break;
            case QEvent::MouseButtonRelease:
                me->accept();
                break;
            case QEvent::MouseMove:
                {
                    int deltaPixels   = me->pos().x() - pixelsOrigin;
                    int deltaProgress = round( 1.0 * (pb->maximum() - pb->minimum() + 1) * deltaPixels / pb->width() );
                    int val = progressOrigin + deltaProgress;
                    if ( step > 1 ) val = val / step * step;
                    pb->setValue(val);
                }
                break;
            default:
                break;
            }
        }
        result = true;
    }
    return result;
}
