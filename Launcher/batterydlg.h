#ifndef BATTERYDLG_H
#define BATTERYDLG_H

#include "Dialog.h"

namespace Ui {
class BatteryDlg;
}

class BatteryDlg : public Dialog
{
    Q_OBJECT

public:
    BatteryDlg(QWidget* parent, bool flag);
    ~BatteryDlg();

private:
    Ui::BatteryDlg *ui;
};

#endif // BATTERYDLG_H
