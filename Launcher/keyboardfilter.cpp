#include <QtGlobal>

#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "SuspendManager.h"
#include "frontlight.h"

#include "keyboardfilter.h"

bool KeyboardFilter::filter( int unicode, int keycode, int modifiers, bool isPress, bool autoRepeat )
{
    Q_UNUSED(unicode)
    Q_UNUSED(modifiers)
    switch ( keycode )
    {
    case Qt::Key_PowerOff: // sleep cover
    case Qt::Key_F2:
        if (    !autoRepeat
             && g_pConfig->readInt("sleepcover", 1) != 0 )
        {
            if ( isPress )
            {
                g_pLog->write(2, Log::MT_I, "Sleep cover button pressed");
                SuspendManager::instanse()->event( SuspendManager::evSleepCoverPressed );
            }
            else
            {
                g_pLog->write(2, Log::MT_I, "Sleep cover button released");
                SuspendManager::instanse()->event( SuspendManager::evSleepCoverReleased );
            }
        }
        return true;
    case Qt::Key_PowerDown: // power button
    case Qt::Key_F1:
        if ( !isPress && !autoRepeat )
        {
            g_pLog->write(2, Log::MT_I, "Power button");
            SuspendManager::instanse()->event( SuspendManager::evPowerBtn );
        }
        return true;
    default:
        SuspendManager::instanse()->activity();
        return false;
    }
}

