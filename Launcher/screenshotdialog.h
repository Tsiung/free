#ifndef SCREENSHOTDIALOG_H
#define SCREENSHOTDIALOG_H

#include "Dialog.h"

namespace Ui {
class ScreenShotDialog;
}

class ScreenShotDialog : public Dialog
{
    Q_OBJECT

public:
    explicit ScreenShotDialog(QWidget *parent = 0);
    ~ScreenShotDialog();

    int delay() const;
    QString file() const;
    QString path() const;

    void accept();

    static QString get_path();

private:
    Ui::ScreenShotDialog *ui;
};

#endif // SCREENSHOTDIALOG_H
