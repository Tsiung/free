<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>BatteryDlg</name>
    <message>
        <source>Close</source>
        <translation>Tancar</translation>
    </message>
    <message>
        <source>Battery is charged!</source>
        <translation>Bateria carregada!</translation>
    </message>
    <message>
        <source>Battery is low!</source>
        <translation>Bateria baixa!</translation>
    </message>
</context>
<context>
    <name>FrontlightDlg</name>
    <message>
        <source>Frontlight</source>
        <translation>Llum</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Calidesa</translation>
    </message>
</context>
<context>
    <name>Launcher</name>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Tancar</translation>
    </message>
</context>
<context>
    <name>ScreenShotDialog</name>
    <message>
        <source>&lt;b&gt;Screen Shot&lt;/b&gt;</source>
        <translation>&lt;b&gt;Captura de pantalla&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delay</source>
        <translation>Retard</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Fitxer</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Ruta</translation>
    </message>
    <message>
        <source>Screen shot</source>
        <translation>Captura de pantalla</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Off</source>
        <translation>Desactivat</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Tancar</translation>
    </message>
    <message>
        <source>Debug</source>
        <translation>Depuració</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <source>Frontlight</source>
        <translation>Llum</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation>Orientació</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>Log level</source>
        <translation>Nivell de registre</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Autoinici</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>Dormir</translation>
    </message>
    <message>
        <source>Enable sleepcover</source>
        <translation>Activar Sleepcover</translation>
    </message>
    <message>
        <source>Wallpaper</source>
        <translation>Fons de pantalla</translation>
    </message>
    <message>
        <source>Blank screen</source>
        <translation>Pantalla en blanc</translation>
    </message>
    <message>
        <source>Random image</source>
        <translation>Imatge aleatòria</translation>
    </message>
    <message>
        <source>Book cover</source>
        <translation>Portada del llibre</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Temps d&apos;espera</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
</context>
<context>
    <name>UsbDialog</name>
    <message>
        <source>Charging</source>
        <translation>Carregant</translation>
    </message>
    <message>
        <source>UsbNet</source>
        <translation>UsbNet</translation>
    </message>
    <message>
        <source>USB mode</source>
        <translation>Mode USB</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Failed to start application!</source>
        <translation>No s&apos;ha pogut iniciar l&apos;aplicació!</translation>
    </message>
    <message>
        <source>Abnormal application termination!</source>
        <translation>Tancament anormal de l&apos;aplicació!</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Fet</translation>
    </message>
    <message>
        <source>Screen shot is ready!</source>
        <translation>Captura de pantalla llesta!</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Failed to create screenshot!</source>
        <translation>No s&apos;ha pogut crear la captura de pantalla!</translation>
    </message>
</context>
</TS>
