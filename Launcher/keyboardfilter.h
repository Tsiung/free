#ifndef KEYBOARDFILTER_H
#define KEYBOARDFILTER_H

#include <QWSServer>

class KeyboardFilter: public QWSServer::KeyboardFilter
{
public:  
    virtual bool filter( int unicode, int keycode, int modifiers, bool isPress, bool autoRepeat );
};

#endif // KEYBOARDFILTER_H
