#ifndef BLANKWIDGET_H
#define BLANKWIDGET_H

#include <QWidget>
#include <QTimer>

class BlankWidget : public QWidget
{
    Q_OBJECT

    QTimer timer;

public:
    BlankWidget(QWidget *parent = 0);
};

#endif // BLANKWIDGET_H
