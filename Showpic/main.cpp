#include <QFile>
#include <Application.h>

#include "widget.h"

int main(int argc, char *argv[])
{
    if ( argc < 3 ) return 1;
    Application app(argc,argv);
    app.setApplicationName("showpic");
    if ( !app.init() ) return 1;
    int time = QString(argv[1]).toInt();
    QString file( QFile::decodeName( argv[2] ) );
    Widget w(NULL, time, file);
    w.showFullScreen();
    return app.exec();
}
