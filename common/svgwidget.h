#ifndef SVGWIDGET_H
#define SVGWIDGET_H

#include <QSvgWidget>

class SvgWidget : public QSvgWidget
{
    QSize size;

public:
    SvgWidget(QWidget * parent = 0)
        : QSvgWidget(parent)
    {}
    void setSizeHint(const QSize& s)
    { size = s; }
    virtual QSize sizeHint() const
    { return size; }
};

#endif // SVGWIDGET_H
