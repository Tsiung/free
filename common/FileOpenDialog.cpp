#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "FileOpenDialog.h"
#include "ui_FileOpenDialog.h"

FileOpenDialog::FileOpenDialog(QWidget* parent, const QString& path, const QString& exts, bool hidden)
    : Dialog(parent)
    , ui(new Ui::FileOpenDialog)
{
    ui->setupUi(this);
    init();

    if ( hidden ) fsModel.setFilter(fsModel.filter() | QDir::Hidden);
    QStringList filters;
    filters = exts.split("|", QString::SkipEmptyParts);
    fsModel.setNameFilters(filters);
    fsModel.setNameFilterDisables(false);
    fsModel.setRootPath("/");
    ui->treeView->setModel(&fsModel);
    ui->treeView->setRootIndex(fsModel.index(path));
    ui->treeView->setHeaderHidden(true);
    ui->treeView->setColumnHidden(1,true);
    ui->treeView->setColumnHidden(2,true);
    ui->treeView->setColumnHidden(3,true);

    QObject::connect(ui->treeView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)) );
    QObject::connect(ui->treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(expand(QModelIndex)) );
}

FileOpenDialog::~FileOpenDialog()
{
    delete ui;
}

void FileOpenDialog::keyPressEvent(QKeyEvent *e)
{
    switch( e->key() )
    {
    case Qt::Key_Menu:
        ui->btnUp->setFocus();
        break;
    default:
        break;
    }
}

void FileOpenDialog::accept()
{
    QModelIndex index = ui->treeView->selectionModel()->currentIndex();
    select( index );
}

void FileOpenDialog::onBtnUp()
{
    QModelIndex oldRoot(ui->treeView->rootIndex());
    QModelIndex newRoot(fsModel.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        ui->treeView->setRootIndex( newRoot );
        ui->treeView->setCurrentIndex( oldRoot );
    }
}

void FileOpenDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
        {
            ui->treeView->setExpanded( index, !ui->treeView->isExpanded( index ) );
        }
        else
        {
            QFileInfo fInfo = fsModel.fileInfo(index);
            if ( fInfo.isFile() )
            {
                fileName = fInfo.absoluteFilePath();
                QDialog::accept();
            }
        }
    }
}

void FileOpenDialog::expand(const QModelIndex &index)
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
            ui->treeView->setExpanded( index, !ui->treeView->isExpanded( index ) );
    }
}
