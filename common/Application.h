#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <QApplication>
#include <QDebug>

class Application : public QApplication
{
    Q_OBJECT

public:
    Application( int & argc, char ** argv );
    ~Application();

    bool init();

protected:
    virtual void customInit() {}

#if defined(ANDROID) || defined(SIMPLE) || defined(IOS) || defined(WINRT)
protected slots:
    virtual void stateChanged( Qt::ApplicationState ) {}
#endif
};

#endif /* APPLICATION_H_ */
