#ifndef FILE_OPEN_DIALOG_H
#define FILE_OPEN_DIALOG_H

#include <QFileSystemModel>
#include <QTreeView>

#include "Dialog.h"

class QKeyEvent;

namespace Ui {
class FileOpenDialog;
}

class FileOpenDialog: public Dialog
{
    Q_OBJECT

    Ui::FileOpenDialog* ui;

    QFileSystemModel fsModel;
    QString fileName;
    QString ext;

public:
    FileOpenDialog(QWidget* parent, const QString& path, const QString& exts, bool hidden=false);
    ~FileOpenDialog();

    const QString& getFileName() const
    { return fileName; }

private:
    virtual void keyPressEvent( QKeyEvent* e );

private slots:
    virtual void accept();
    void onBtnUp();
    void select( const QModelIndex& index );
    void expand( const QModelIndex& index );
};

#endif // FILE_OPEN_DIALOG_H
