#include "QtUtils.h"

#include "EnterTextLineDialog.h"
#include "ui_EnterTextLineDialog.h"

EnterTextLineDialog::EnterTextLineDialog(QWidget* parent, const QString& title, const QString& prompt, const QString& value)
    : Dialog(parent)
    , ui(new Ui::EnterTextLineDialog)
{
    ui->setupUi(this);
    init();
    setWindowTitle(title);
    ui->lblPrompt->setText(prompt);
    ui->leValue->setText(value);
#if defined(QT_KEYPAD_NAVIGATION)
    QTimer::singleShot(0, this, SLOT(onSelectText()));
#endif
}

EnterTextLineDialog::~EnterTextLineDialog()
{
    delete ui;
}

QString EnterTextLineDialog::getValue() const
{
    return ui->leValue->text();
}

void EnterTextLineDialog::onSelectText()
{
    ui->leValue->selectAll();
}
