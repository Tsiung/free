#ifndef H__UTILS
#define H__UTILS

#define PSEP '/'
#define PSEPS "/"
#define SIZE(arr) (sizeof(arr)/sizeof(arr[0]))
#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
#define FORMAT(x,y) __attribute__((format(printf, x, y)));
#else
#define FORMAT(x,y)
#endif

#endif

