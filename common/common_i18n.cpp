#include <qglobal.h>

const char* trans[] = {
    QT_TRANSLATE_NOOP("QDialogButtonBox","OK"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&OK"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Save"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&Save"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Open"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Cancel"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&Cancel"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Close"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&Close"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Apply"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Reset"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Discard"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&Yes"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","&No"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Abort"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Retry"),
    QT_TRANSLATE_NOOP("QDialogButtonBox","Ignore"),   
    
    QT_TRANSLATE_NOOP("QErrorMessage","&OK"),
    
    QT_TRANSLATE_NOOP("QMessageBox","OK"),
    
    QT_TRANSLATE_NOOP("QGnomeTheme","&OK"),
    QT_TRANSLATE_NOOP("QGnomeTheme","&Save"),
    QT_TRANSLATE_NOOP("QGnomeTheme","&Cancel"),
    QT_TRANSLATE_NOOP("QGnomeTheme","&Close"),
        
    QT_TRANSLATE_NOOP("QPlatformTheme","OK"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Open"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Save"),
    QT_TRANSLATE_NOOP("QPlatformTheme","&Yes"),
    QT_TRANSLATE_NOOP("QPlatformTheme","&No"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Abort"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Retry"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Ignore"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Close"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Cancel"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Discard"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Help"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Apply"),
    QT_TRANSLATE_NOOP("QPlatformTheme","Reset"),

    QT_TRANSLATE_NOOP("QAndroidPlatformTheme","Yes"),
    QT_TRANSLATE_NOOP("QAndroidPlatformTheme","No")
};

