#include <QtGlobal>
#include <QString>
#include <QFontMetrics>
#include <QDesktopWidget>
#include <QComboBox>

#include "compat.h"

int font_metrics_width(const QFontMetrics& m, const QString& str)
{
#if QT_VERSION >= QT_VERSION_CHECK(5,11,0)
    return m.horizontalAdvance(str);
#else
    return m.width(str);
#endif
}

void set_current_text(QComboBox& cb, const QString& str)
{
#if QT_VERSION >= 0x050000
    cb.setCurrentText(str);
#else
    if ( cb.currentIndex() > 0 )
    {
        cb.setItemText(cb.currentIndex(), str);
    }
#endif
}

