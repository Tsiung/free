#include <QApplication>
#include <QTranslator>

#include "Config.h"
#include "Factory.h"
#include "Singleton.h"
#include "LanguageDescriptor.h"

int getLangIndex()
{
    // translations
    Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();

    int index = 0;
    int lang = g_pConfig->readInt("lang", 0);

    if ( 0 == lang ) // system
    {
        // try to use system language
        QString langName = Platform::get()->getSystemLang();
        if ( !langName.isEmpty() )
        {
            Factory<LanguageDescriptor>::Iterator i( ldf.begin() );
            for ( ; i != ldf.end() && langName != i->name; i++, index++ )
            {}
            if ( i == ldf.end() )
                index = 1; // Not found, fallback to English
        }
        else
        {
            index = 1; // English
        }
    }
    else
    {
        index = lang;  // Manual language selection
    }

    return index;
}

QString getLangName()
{
    int lang = g_pConfig->readInt( "lang", -1 );
    QString locale;
    if ( lang > 0 )
    {
        Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();
        locale = ldf.at(lang).name;
    }
    else
    {
        locale = Platform::get()->getSystemLang();
    }
    return locale;
}

QString getLangForWebLink()
{
    QString lang(::getLangName());
    return (lang == "Russian" || lang == "Ukrainian") ? "ru" : "en";
}

void loadTranslations()
{
    static QTranslator *trans1 = NULL, *trans2 = NULL;

    // translations
    Factory<LanguageDescriptor>& ldf = Singleton<Factory<LanguageDescriptor> >::instance();

    if ( ldf.isEmpty() )
    {
        QString path = Platform::get()->getTranslationsPath() + QDir::separator() + "languages.txt";
        ldf.init( path, false );
    }

    if ( !ldf.isEmpty() )
    {
        int index = getLangIndex();
        // common translator
        if ( !trans1 )
        {
            trans1 = new QTranslator(qApp);
            qApp->installTranslator(trans1);
        }
        trans1->load(Platform::get()->getTranslationsPath() + QDir::separator() + "common" +  QDir::separator() + ldf.at(index).getTransFileName());
        // app translator
        if ( !trans2 )
        {
            trans2 = new QTranslator(qApp);
            qApp->installTranslator(trans2);
        }
        trans2->load(Platform::get()->getTranslationsPath() + QDir::separator() + qApp->applicationName() +  QDir::separator() + ldf.at(index).getTransFileName());
    }
}

QString trans(const char *ctx, const char *text)
{
    return qApp->translate(ctx, text);
}

QString trans(const char *ctx, const QString &text)
{
    return trans(ctx, text.toLocal8Bit().constData());
}
