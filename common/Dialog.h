#ifndef DIALOG_H_
#define DIALOG_H_

#include <QDialog>
#include <QDebug>

#if defined(Q_WS_QWS)
#include "VirtualKeyboard.h"
#include "WidgetEventFilter.h"
#include "VirtualKeyboardContainer.h"
#endif

class Dialog : public QDialog
#if defined(Q_WS_QWS)
, protected VirtualKeyboardContainer
, private WidgetEventFilter
#endif
{
    Q_OBJECT

#if !defined(DESKTOP)
    Q_PROPERTY(bool fullScreen READ isFullScreen WRITE setFullScreen)
    bool fullScreen; // full screen dialog (should be set before exec)
    bool lockOrientation; // there is no need to lock orientation for some dialogs    
#endif

public:
    Dialog( QWidget* parent = 0 )
        : QDialog(parent)
#if defined(Q_WS_QWS)
        , VirtualKeyboardContainer(this)
        , WidgetEventFilter(this, this)
#endif
#if !defined(DESKTOP)
        , fullScreen(true)
        , lockOrientation(true)
#endif
    {
#if defined(Q_WS_QWS)
        showVirtualKeyboard(NULL);
#endif
    }

    int exec();

#if !defined(DESKTOP)
    bool isFullScreen() const
    { return fullScreen; }

    void setFullScreen( bool val )
    { fullScreen = val; }

    void setLockOrientation( bool val )
    { lockOrientation = val; }
#endif

#if defined(Q_WS_QWS)
    virtual bool eventFilter(QObject* obj, QEvent* event);
#endif

protected:
#if defined(DESKTOP)
    virtual void done( int r );
#endif

    virtual void init();
    void setFocusToWidget( QWidget* w );
};

#endif /* DIALOG_H_ */
