#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <cmath>

#include <string>

#include <QDir>
#include <QTextStream>
#include <QCoreApplication>

#include "QtUtils.h"

#include "KoboPlatform.h"

static void write_light_value(const std::string& file, int value)
{
    QFile f(file.c_str());
    if (f.open(QIODevice::WriteOnly))
    {
        f.write(QString::number(value).toLatin1());
        f.flush();
        f.close();
    }
}

static void set_light_value(const std::string& dir, int value)
{
    write_light_value(dir + "/bl_power", value > 0 ? 31:0);
    write_light_value(dir + "/brightness", value);
}

static void set_brightness( int value )
{
    int light = -1;
    // Open the file for reading and writing
    if ((light = ::open("/dev/ntx_io", O_RDWR)) != -1)
    {
        ::ioctl(light, 241, value);
        ::close(light);
    }
}

KoboPlatform::KoboPlatform()
    : dev(get_kobo_device())
{
}

QString KoboPlatform::getSerialNumber() const
{
    static QString serial;
    if ( serial.isEmpty() )
    {
        QString fileName("/mnt/onboard/.kobo/version");
        serial = ::str_from_file(fileName);
        QStringList tokens( serial.split(',')  );
        serial = tokens.size() >= 1 ? tokens.at(0) : "0";
    }
    return serial;
}

int KoboPlatform::getOrientation() const
{
    static int map_norm[4]  = {1,0,3,2};
    static int map_h2o2[4]  = {3,2,1,0};
    static int map_libra[4] = {0,3,2,1};
    const int* map = map_norm;
    switch(dev)
    {
    case KoboAuraH2O2_v1:
    case KoboAuraH2O2_v2:
        map = map_h2o2;
        break;
    case KoboLibra:
        map = map_libra;
        break;
    default:
        break;
    }
    return map[ QWSPlatform::getOrientation() ];
}

void KoboPlatform::setOrientation( int ori )
{
    static int map_norm[4]  = {1,0,3,2};
    static int map_h2o2[4]  = {3,2,1,0};
    static int map_libra[4] = {0,3,2,1};
    const int* map = map_norm;
    switch(dev)
    {
    case KoboAuraH2O2_v1:
    case KoboAuraH2O2_v2:
        map = map_h2o2;
        break;
    case KoboLibra:
        map = map_libra;
        break;
    default:
        break;
    }
    QWSPlatform::setOrientation( map[ori] );
}

int KoboPlatform::getBatteryLevel() const
{
    return int_from_file("/sys/devices/platform/pmic_battery.1/power_supply/mc13892_bat/capacity");
}

bool KoboPlatform::isBatteryCharging() const
{
    return str_from_file("/sys/devices/platform/pmic_battery.1/power_supply/mc13892_bat/status") == "Charging";
}

int KoboPlatform::frontlightGetMinLevel() const
{
    return 0;
}

int KoboPlatform::frontlightGetMaxLevel() const
{
    return 100;
}

int KoboPlatform::frontlightGetMinTemp() const
{
    return 0;
}

int KoboPlatform::frontlightGetMaxTemp() const
{
    if ( comfortLight() )
    {
        switch( dev ) {
            case KoboForma:
            case KoboClaraHD:
            case KoboLibra:
	            return 10;
            default:
	            return 100;
        }
    }
    return 0;
}

void KoboPlatform::frontlightSetLevel( int val, int temp )
{
    Platform::frontlightSetLevel(val,temp);
    if ( comfortLight() )
    {
        setNaturalBrightness(frontlightGetLevel(), frontlightGetTemp());
    }
    else
    {
        set_brightness(frontlightGetLevel());
    }
}

void KoboPlatform::setNaturalBrightness(int brig, int temp)
{
    const char *fWhite = NULL, *fRed = NULL, *fGreen = NULL, *fMixer = NULL;

    switch ( dev )
    {
    case KoboAuraOne:
        fWhite = "/sys/class/backlight/lm3630a_led1b";
        fRed   = "/sys/class/backlight/lm3630a_led1a";
        fGreen = "/sys/class/backlight/lm3630a_ledb";
        break;
    case KoboAuraH2O2_v1:
        fWhite = "/sys/class/backlight/lm3630a_ledb";
        fRed   = "/sys/class/backlight/lm3630a_led";
        fGreen = "/sys/class/backlight/lm3630a_leda";
        break;
     case KoboAuraH2O2_v2:
        fWhite = "/sys/class/backlight/lm3630a_ledb";
        fRed   = "/sys/class/backlight/lm3630a_leda";
        break;
     case KoboClaraHD:
        fWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        fMixer = "/sys/class/backlight/lm3630a_led/color";
        break;
     case KoboForma:
        fWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        fMixer = "/sys/class/backlight/tlc5947_bl/color";
        break;
     case KoboLibra:
        fWhite = "/sys/class/backlight/mxc_msp430.0/brightness";
        fMixer = "/sys/class/backlight/lm3630a_led/color";
     default:
        break;
    }

    if ( KoboForma == dev || KoboClaraHD == dev || KoboLibra == dev )
    {
        if ( fWhite != NULL )
            write_light_value( fWhite, brig );
        if ( fMixer != NULL )
            write_light_value( fMixer, 10 - temp );
    }
    else
    {
        static const int white_gain = 25;
        static const int red_gain = 24;
        static const int green_gain = 24;
        static const int white_offset = -25;
        static const int red_offset = 0;
        static const int green_offset = -65;
        static const double exponent = 0.25;
        double red = 0.0, green = 0.0, white = 0.0;
        if ( brig > 0 )
        {
            white = std::min(white_gain * pow(brig, exponent) * pow(frontlightGetMaxTemp()-temp, exponent) + white_offset, 255.0);
        }
        if ( temp > 0 )
        {
            red = std::min(red_gain * pow(brig, exponent) * pow(temp, exponent) + red_offset, 255.0);
            green = std::min(green_gain * pow(brig, exponent) * pow(temp, exponent) + green_offset, 255.0);
        }
        white = std::max(white, 0.0);
        red = std::max(red, 0.0);
        green = std::max(green, 0.0);

        if ( fWhite != NULL )
            set_light_value( fWhite, floor(white));
        if ( fRed != NULL )
            set_light_value( fRed, floor(red));
        if ( fGreen != NULL )
            set_light_value( fGreen, floor(green));
    }
}

bool KoboPlatform::comfortLight() const
{
    return    KoboAuraOne == dev 
           || KoboAuraH2O2_v1 == dev 
           || KoboAuraH2O2_v2 == dev 
           || KoboClaraHD == dev 
           || KoboForma == dev
           || KoboLibra == dev;
}

