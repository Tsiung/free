#include <QDir>

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#include <QDesktopWidget>
#endif

#if defined(WINDOWS)
#include <winsock2.h>
#include <iphlpapi.h>
#include <QCryptographicHash>
#endif

#include <QApplication>
#include <QClipboard>
#include <QLocale>
#include <QScreen>

#include "QtUtils.h"

#include "DesktopPlatform.h"

QString DesktopPlatform::name() const
{
#if defined(LINUX)
    return "linux";
#elif defined(WINDOWS)
    return "windows";
#elif defined(MACX)
    return "osx";
#else
    return "null";
#endif
}

#if defined(WINDOWS)
static QString get_mac_addresses()
{
    IP_ADAPTER_INFO AdapterInfo[16];
    DWORD dwBufLen = sizeof (AdapterInfo);

    DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);
    if (dwStatus != ERROR_SUCCESS) {
        return QString();
    }

    QStringList list;

    PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;
    while (pAdapterInfo) {
        QString addr;
        for (int i = 0; i < 6; ++i) {
            addr += QString::number(pAdapterInfo->Address[i], 16);
        }
        list.push_back(addr);
        pAdapterInfo = pAdapterInfo->Next;
    }

    list.sort();

    return list.join(",");
}
#endif

QString DesktopPlatform::generateSerialNumber() const
{
    QString sn;
#if defined(LINUX)
    sn += "L";
#elif defined(WINDOWS)
    sn += "W";
#elif defined(MACX)
    sn += "M";
#endif

#if defined(WINDOWS)
    QString addrs = get_mac_addresses();
    if ( !addrs.isEmpty() ) {
        QCryptographicHash hash(QCryptographicHash::Md5);
        hash.addData(addrs.toLatin1());
        sn += hash.result().toHex().left(8).toUpper();
    } else {
        sn += Platform::generateSerialNumber();
    }
#else
    sn += Platform::generateSerialNumber();
#endif

#if defined(AMAZON_STORE)
    sn += "-AMAZ";
#endif

    return sn;
}

QString DesktopPlatform::getUserSettingsPath() const
{
#if QT_VERSION > 0x050000
    QString path(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
#else
    QString path(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#endif
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}

QString DesktopPlatform::getRootPath() const
{
#if defined(MACX)
    return QCoreApplication::applicationDirPath() + "/../Resources";
#else
    return QCoreApplication::applicationDirPath();
#endif
}

QString DesktopPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

qreal DesktopPlatform::getDPI() const
{
#if QT_VERSION > 0x050000
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
#else
    return ( qApp->desktop()->logicalDpiX() + qApp->desktop()->logicalDpiY() ) / 2.0;
#endif
}

QString DesktopPlatform::getDocumentsPath() const
{
#if QT_VERSION > 0x050000
    QString path(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
#else
    QString path(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation));
#endif
    path += QDir::separator();
    path += qApp->applicationName();
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}
