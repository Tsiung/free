#include <cstdlib>

#include <QDir>
#include <QScreen>
#include <QGuiApplication>
#include <QAndroidJniObject>

#include "md5.h"

#include "AndroidPlatform.h"

AndroidPlatform::AndroidPlatform()
    : lockCounter(0)
{
}

QString AndroidPlatform::getSerialNumber() const
{
    static QString sn;
    if ( sn.isEmpty() )
    {
        sn = qgetenv("ARCH") + ::md5( qgetenv("SERIAL") ).left( 8 );
        QString suff(qgetenv("STORE"));
        if ( !suff.isEmpty() )
            sn += "-" + suff;
    }
    return sn;
}

QString AndroidPlatform::getRootPath() const
{
    return qgetenv("ROOT_PATH");
}

QString AndroidPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

int AndroidPlatform::getDefaultFontSize() const
{
    return 20;
}
    
QString AndroidPlatform::getTranslationsPath() const
{
    return qgetenv("VLASOVSOFT_I18N");
}

qreal AndroidPlatform::getDPI() const
{
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
}

void AndroidPlatform::vibrate( int ms )
{
    jint arg = ms;
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "vibrate", "(I)V", arg);
}

void AndroidPlatform::lockOrientation()
{
    if ( 0 == lockCounter )
    {
        QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "lockOrientation", "()V");
    }
    lockCounter++;
}

void AndroidPlatform::unlockOrientation()
{
    if ( lockCounter > 0 ) lockCounter--;
    if ( 0 == lockCounter )
    {   
        QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "unlockOrientation", "()V");
    }
}

void AndroidPlatform::loadAd()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "loadAd", "()V");
}

void AndroidPlatform::showAd()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "showAd", "()V");
}

void AndroidPlatform::checkLicense()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "checkLicense", "()V");
}

void AndroidPlatform::buyNow()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "buyNow", "()V");
}

void AndroidPlatform::cancelPurchase()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "cancelPurchase", "()V");
}

void AndroidPlatform::rateApp()
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "rateApp", "()V");
}

void AndroidPlatform::keepScreenOn(bool val)
{
    QAndroidJniObject::callStaticMethod<void>("com/vlasovsoft/qtandroid/MainActivity", "keepScreenOn", "(Z)V", val);
}

bool AndroidPlatform::checkReadPermission() const
{
    return static_cast<bool>(QAndroidJniObject::callStaticMethod<jboolean>("com/vlasovsoft/qtandroid/MainActivity", "checkReadPermission", "()Z"));
}

bool AndroidPlatform::checkWritePermission() const
{
    return static_cast<bool>(QAndroidJniObject::callStaticMethod<jboolean>("com/vlasovsoft/qtandroid/MainActivity", "checkWritePermission", "()Z"));
}
