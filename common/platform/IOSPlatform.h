#include "Platform.h"

class QWidget;

class IOSPlatform : public Platform
{
public:
    virtual QString name() const { return "ios"; }
    virtual QString generateSerialNumber() const;
    virtual QString getUserSettingsPath() const;
    virtual QString getDocumentsPath() const;
    virtual QString getRootPath() const;
    virtual QString getSystemLang() const;
    virtual int getDefaultFontSize() const;
    virtual qreal getDPI() const;
    virtual QMargins getSafeAreaMargins(QWidget* w) const;
};
