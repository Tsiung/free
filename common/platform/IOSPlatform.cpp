#include <QDir>

#include <QStandardPaths>
#include <QApplication>
#include <QClipboard>
#include <QLocale>
#include <QScreen>
#include <QWindow>
#include <qpa/qplatformwindow.h>

#include "QtUtils.h"

#include "IOSPlatform.h"

QString IOSPlatform::generateSerialNumber() const
{
    QString sn;
    sn += "IOS";
    return sn + Platform::generateSerialNumber();
}

QString IOSPlatform::getUserSettingsPath() const
{
    QString path(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}

QString IOSPlatform::getDocumentsPath() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString IOSPlatform::getRootPath() const
{
    return QCoreApplication::applicationDirPath();
}

QString IOSPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

int IOSPlatform::getDefaultFontSize() const
{
    return 20;
}

qreal IOSPlatform::getDPI() const
{
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
}

QMargins IOSPlatform::getSafeAreaMargins(QWidget* w) const
{
    return w->windowHandle()->handle()->safeAreaMargins();
}
