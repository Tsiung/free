#include <dlfcn.h>

#include <iostream>

#include <QFile>
#include <QTextStream>
#include <QProcess>

#include "QtUtils.h"

#include "KindlePlatform.h"

KindlePlatform::KindlePlatform()
    : dev(get_kindle_device())
    , devBatteryLevel(NULL)
    , devBatteryIsCharging(NULL)
    , devFrontLight(NULL)
    , dlHandle(NULL)
    , lipcHandle(0)
    , lipc_open(NULL)
    , lipc_get_int_prop(NULL)
    , lipc_set_int_prop(NULL)
    , lipc_close(NULL)
{
    lipcInit();

    switch(dev)
    {
    case KindleTouch:
        devBatteryLevel = "/sys/devices/system/yoshi_battery/yoshi_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/platform/fsl-usb2-udc/charging";
        break;
    case KindleTouch2:
        devBatteryLevel = "/sys/devices/system/wario_battery/wario_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/system/wario_charger/wario_charger0/charging";
        break;
    case KindleTouch3:
        devBatteryLevel = "/sys/class/power_supply/bd7181x_bat/capacity";
        devBatteryIsCharging = "/sys/class/power_supply/bd7181x_bat/charging"; 
        break;
    case KindleTouch4:
        devBatteryLevel = "/sys/class/power_supply/bd71827_bat/capacity";
        devBatteryIsCharging = "/sys/class/power_supply/bd71827_bat/charging";
        devFrontLight = "/sys/class/backlight/bl/brightness";
        break; 
    case KindlePaperWhite:
        devBatteryLevel = "/sys/devices/system/yoshi_battery/yoshi_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/platform/aplite_charger.0/charging";
        devFrontLight = "/sys/devices/system/fl_tps6116x/fl_tps6116x0/fl_intensity";
        break;
    case KindlePaperWhite2:
        devBatteryLevel = "/sys/devices/system/wario_battery/wario_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/system/wario_charger/wario_charger0/charging";
        devFrontLight = "/sys/class/backlight/max77696-bl/brightness";
        break;
    case KindlePaperWhite3:
        devBatteryLevel = "/sys/devices/system/wario_battery/wario_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/system/wario_charger/wario_charger0/charging";
        devFrontLight = "/sys/class/backlight/max77696-bl/brightness";
        break;
    case KindlePaperWhite4:
        devBatteryLevel = "/sys/class/power_supply/bd71827_bat/capacity";
        devBatteryIsCharging = "/sys/class/power_supply/bd71827_bat/charging";
        devFrontLight = "/sys/class/backlight/bl/brightness";
        break;
    case KindleVoyage:
        devBatteryLevel = "/sys/devices/system/wario_battery/wario_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/system/wario_charger/wario_charger0/charging";
        devFrontLight = "/sys/class/backlight/max77696-bl/brightness";
        break;
    case KindleOasis:
        devBatteryLevel = "/sys/devices/system/wario_battery/wario_battery0/battery_capacity";
        devBatteryIsCharging = "/sys/devices/system/wario_charger/wario_charger0/charging";
        devFrontLight = "/sys/class/backlight/max77696-bl/brightness";
        break;
    case KindleOasis2:
        devBatteryLevel = "/sys/class/power_supply/max77796-battery/capacity";
        devBatteryIsCharging = "/sys/class/power_supply/max77796-charger/charging";
        devFrontLight = "/sys/class/backlight/max77796-bl/brightness";
        break;
    default:
        break;
    }
}

KindlePlatform::~KindlePlatform()
{
    lipcCleanup();
}

QString KindlePlatform::getSerialNumber() const
{
    return ::str_from_file("/proc/usid");
}

int KindlePlatform::getBatteryLevel() const
{
    return NULL == devBatteryLevel? 50 : int_from_file_ex(devBatteryLevel);
}

bool KindlePlatform::isBatteryCharging() const
{
    return NULL == devBatteryIsCharging? false : (1 == ::str_from_file(devBatteryIsCharging).trimmed().toInt());
}

void KindlePlatform::lipcInit()
{
    dlHandle = dlopen("liblipc.so", RTLD_LAZY);
    if ( !dlHandle ) {
        std::cerr << "dlopen() error: liblipc.so" << std::endl;
        return;
    }
    lipc_open = reinterpret_cast<lipc_open_t>(dlsym(dlHandle, "LipcOpenNoName"));
    if ( !lipc_open ) {
        std::cerr << "dlsym() error: LipcOpenNoName";
        return;
    }
    lipc_get_int_prop = reinterpret_cast<lipc_get_int_prop_t>(dlsym(dlHandle, "LipcGetIntProperty"));
    if ( !lipc_get_int_prop ) {
        std::cerr << "dlsym() error: LipcGetIntProperty";
        return;
    }
    lipc_set_int_prop = reinterpret_cast<lipc_set_int_prop_t>(dlsym(dlHandle, "LipcSetIntProperty"));
    if ( !lipc_set_int_prop) {
        std::cerr << "dlsym() error: LipcSetIntProperty";
        return;
    }
    lipc_close = reinterpret_cast<lipc_close_t>(dlsym(dlHandle, "LipcClose"));
    if ( !lipc_close ) {
        std::cerr << "dlsym() error: LipcClose";
        return;
    }
    lipcHandle = lipc_open();
    if ( !lipcHandle ) {
        std::cerr << "LipcOpenNoName() error";
        return;
    }
}

void KindlePlatform::lipcCleanup()
{
    if ( lipcHandle )
        lipc_close(lipcHandle);
    if ( dlHandle )
        dlclose(dlHandle);
}

void KindlePlatform::lipcSetIntProperty(const char *publisher, const char *property, int value)
{
    if ( lipcHandle != 0 )
        lipc_set_int_prop(lipcHandle, const_cast<char*>(publisher), const_cast<char*>(property), value);
}

int KindlePlatform::lipcGetIntProperty(const char *publisher, const char *property) const
{
    int value = 0;
    if ( lipcHandle != 0 )
    {
        lipc_get_int_prop(lipcHandle, const_cast<char*>(publisher), const_cast<char*>(property), &value);
    }
    return value;
}

int KindlePlatform::frontlightGetMinLevel() const
{
    return 0;
}

int KindlePlatform::frontlightGetMaxLevel() const
{
    return 24;
}

void KindlePlatform::frontlightSetLevel( int val, int )
{
    if ( devFrontLight != NULL && lipcHandle != 0 )
    {
        lipcSetIntProperty("com.lab126.powerd", "flIntensity", val);
        if ( 0 == val )
        {
            QFile f(devFrontLight);
            if (f.open(QIODevice::WriteOnly)) {
                f.write("0");
                f.flush();
            }
        }
    }
}

int KindlePlatform::frontlightGetLevel() const
{
    return lipcGetIntProperty("com.lab126.powerd", "flIntensity");
}
