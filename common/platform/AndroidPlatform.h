#ifndef H__ANDROID_PLATFORM
#define H__ANDROID_PLATFORM

#include <QMap>

#include "Platform.h"

class AndroidPlatform : public Platform
{
    friend class Platform;

    int lockCounter;

protected:
    AndroidPlatform();

public:
    virtual QString name() const override { return "android"; }
    virtual QString getSerialNumber() const override;
    virtual QString getUserSettingsPath() const override { return getRootPath();  }
    virtual QString getDocumentsPath() const override { return getRootPath();  }
    virtual QString getRootPath() const override;
    virtual QString getSystemLang() const override;
    virtual int getDefaultFontSize() const override;
    virtual QString getTranslationsPath() const override;
    virtual qreal getDPI() const override;
    virtual void vibrate( int ms ) override;
    virtual void lockOrientation() override;
    virtual void unlockOrientation() override;
    virtual void loadAd() override;
    virtual void showAd() override;
    virtual void checkLicense() override;
    virtual void buyNow() override;
    virtual void cancelPurchase() override;
    virtual void rateApp() override;
    virtual void keepScreenOn(bool val) override;
    virtual bool checkReadPermission() const override;
    virtual bool checkWritePermission() const override;
};

#endif // H__ANDROID_PLATFORM
