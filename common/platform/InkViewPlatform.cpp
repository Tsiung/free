#include <inkview.h>
#if defined(OBREEY)
#include <inkinternal.h>
#endif

#include "InkViewPlatform.h"

InkViewPlatform::InkViewPlatform()
{
    Platform::frontlightSetLevel(frontlightGetLevel(), 0);
    Platform::frontlightSetOn(frontlightIsOn());
}

QString InkViewPlatform::getSerialNumber() const
{
    return ::GetSerialNumber();
}

int InkViewPlatform::getBatteryLevel() const
{
    return ::GetBatteryPower();
}

bool InkViewPlatform::isBatteryCharging() const
{
    return ::IsCharging();
}

#if defined(OBREEY)
int InkViewPlatform::frontlightGetMinLevel() const
{
    return 1;
}

int InkViewPlatform::frontlightGetMaxLevel() const
{
    return 100;
}

int InkViewPlatform::frontlightGetLevel() const
{
    return abs(::GetFrontlightState());    
}

void InkViewPlatform::frontlightSetLevel( int val, int temp )
{
    Platform::frontlightSetLevel(val, temp);
    bool isOn = frontlightIsOn();
    ::SetFrontlightState( isOn? frontlightLevel_:-frontlightLevel_ );
}

bool InkViewPlatform::frontlightIsOn() const
{
    return ::GetFrontlightState() >= 0;
}

void InkViewPlatform::frontlightSetOn( bool val )
{
    Platform::frontlightSetOn(val);
    ::SetFrontlightState( frontlightIsOn_? frontlightLevel_:-frontlightLevel_ );
}
#endif

void InkViewPlatform::setSleepMode( bool val )
{
    ::iv_sleepmode( val? 1:0 );
}

#if defined(OBREEY)
void InkViewPlatform::switchNetworkOn()
{
    ::WiFiPower(1);
}

void InkViewPlatform::switchNetworkOff()
{
    ::WiFiPower(0);
}
#endif
