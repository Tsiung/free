#include <QDir>
#include <QClipboard>
#include <QApplication>
#include <QDebug>

#include "QtUtils.h"

#if defined(DESKTOP) || defined(SIMPLE)
  #include "DesktopPlatform.h"
#elif defined (POCKETBOOK) || defined(OBREEY)
  #include "InkViewPlatform.h"
#elif defined (KOBO)
  #include "KoboPlatform.h"
#elif defined (ANDROID)
  #include "AndroidPlatform.h"
#elif defined (KINDLE)
  #include "KindlePlatform.h"
#elif defined (IOS)
  #include "IOSPlatform.h"
#elif defined(WINRT)
  #include "WinRtPlatform.h"
#elif defined(QVFB)
  #include "QvfbPlatform.h"
#else
  #include "QWSPlatform.h"
#endif

Platform* Platform::p = NULL;

Platform* Platform::get()
{
    if ( !p )
    {
#if defined(DESKTOP) || defined(SIMPLE)
        p = new DesktopPlatform();
#elif defined (POCKETBOOK) || defined(OBREEY)
        p = new InkViewPlatform();
#elif defined (KOBO)
        p = new KoboPlatform();
#elif defined (ANDROID)
        p = new AndroidPlatform();
#elif defined (KINDLE)
        p = new KindlePlatform();
#elif defined (IOS)
        p = new IOSPlatform();
#elif defined(WINRT)
        p = new WinRtPlatform();
#elif defined(QVFB)
        p = new QvfbPlatform();
#else
        p = new QWSPlatform();
#endif
    }
    return p;
}

void Platform::destroy()
{
    if ( p )
    {
        delete p;
        p = NULL;
    }
}

QString Platform::generateSerialNumber() const
{
    return ::v1();
}

QString Platform::getSerialNumber() const
{
    static QString serial;
    if ( serial.isEmpty() )
    {
        QString path(qgetenv("VLASOVSOFT_SERIAL"));
        if ( path.isEmpty() )
        {
            path = getUserSettingsPath() + QDir::separator() + "serial";
        }
        serial = ::str_from_file(path);
        if ( serial.isEmpty() )
        {
            serial = generateSerialNumber();
            ::str_to_file(path, serial);
        }
    }
    return serial;
}

QString Platform::getLicenseKey() const
{
    return ::str_from_file(getLicenseKeyFileName());
}

void Platform::setLicenseKey( const QString& key ) const
{
    ::str_to_file(getLicenseKeyFileName(), key);
}

QString Platform::getClipboardText() const
{
    return QApplication::clipboard()?
        QApplication::clipboard()->text() :
        QString();
}

void Platform::setClipboardText(const QString & text)
{
    if ( QApplication::clipboard() )
        QApplication::clipboard()->setText(text);
}

QString Platform::getLicenseKeyFileName() const
{
    QString fileName(qgetenv("VLASOVSOFT_KEY"));
    if ( fileName.isEmpty() )
        fileName = getUserSettingsPath() + QDir::separator() + "key";
    return fileName;
}

int Platform::frontlightGetMinLevel() const
{
    return 0;
}

int Platform::frontlightGetMaxLevel() const
{
    return 0;
}

int Platform::frontlightGetMinTemp() const
{
    return 0;
}

int Platform::frontlightGetMaxTemp() const
{
    return 0;
}

int Platform::frontlightGetLevel() const
{
    return frontlightLevel_;
}

int Platform::frontlightGetTemp() const
{
    return frontlightTemp_;
}

void Platform::frontlightSetLevel( int val, int temp )
{
    if ( val > frontlightGetMaxLevel() )
        frontlightLevel_ = frontlightGetMaxLevel();
    else
    if ( val < frontlightGetMinLevel() )
        frontlightLevel_ = frontlightGetMinLevel();
    else
        frontlightLevel_ = val;
    
    if ( temp > frontlightGetMaxTemp()  )
        frontlightTemp_ = frontlightGetMaxTemp();
    else
    if ( temp < frontlightGetMinTemp()  )
        frontlightTemp_ = frontlightGetMinTemp();
    else
        frontlightTemp_ = temp;
}

void Platform::setSleepMode( bool val )
{
    qDebug() << "setSleepMode(" << val << ")";
}
 
int Platform::getDefaultFontSize() const
{
    return 14;
}

int Platform::getBatteryLevel() const
{
    return 0;
}

bool Platform::isBatteryCharging() const
{
    return false;
}

QString Platform::getSystemLang() const
{
    return QString();
}

QString Platform::getTranslationsPath() const
{
    return getRootPath() + QDir::separator() + "i18n";
}
