#ifndef PLATFORM_H
#define PLATFORM_H

#include <QString>
#include <QMargins>

class QWidget;

class Platform
{
    static Platform* p;

protected:
    int frontlightLevel_;
    int frontlightTemp_;  // temperature

protected:
    Platform()
        : frontlightLevel_(0)
        , frontlightTemp_(0)
    {}

    virtual ~Platform() {}

public:
    static Platform* get();
    static void destroy();
    virtual QString name() const = 0;
    // License related functions
    virtual QString generateSerialNumber() const;
    virtual QString getSerialNumber() const;
    virtual QString getLicenseKey() const;
    virtual void setLicenseKey( const QString& key ) const;
    // Path functions
    virtual QString getUserSettingsPath() const = 0;
    virtual QString getDocumentsPath() const = 0;
    virtual QString getRootPath() const = 0;
    // Eink functions
    virtual void scheduleEinkFullUpdate() const {}
    // Screen orientation functions
    virtual int getOrientation() const { return 1; }
    virtual void setOrientation( int ) {}
    virtual bool canRotateScreen() const { return false; }
    virtual void lockOrientation() {}
    virtual void unlockOrientation() {}
    // Clipboard functions
    virtual QString getClipboardText() const;
    virtual void setClipboardText( const QString& );
    // Frontlight functions
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetMinTemp() const;
    virtual int frontlightGetMaxTemp() const;
    virtual int frontlightGetLevel() const;
    virtual int frontlightGetTemp() const;
    virtual void frontlightSetLevel( int val, int temp );
    // Sleep functions
    virtual void setSleepMode( bool val );
    // Translations location
    virtual QString getTranslationsPath() const;
    // Battery functions
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    // Network functions (e.g. Wifi, 3G)
    virtual bool isNetworkActive(char* ip=NULL) const
    {
        Q_UNUSED(ip)
        return true;
    }
    virtual void switchNetworkOn() {}
    virtual void switchNetworkOff() {}
    virtual void networkActivity() {}
    // Other functions 
    virtual int getDefaultFontSize() const;
    virtual QString getSystemLang() const;
    virtual qreal getDPI() const = 0;
    virtual void vibrate( int ) {}
    virtual void loadAd() {}
    virtual void showAd() {}
    virtual void checkLicense() {}
    virtual void buyNow() {}
    virtual void cancelPurchase() {}
    virtual void rateApp() {}
    virtual void keepScreenOn(bool) {}
    virtual QMargins getSafeAreaMargins(QWidget*) const { return QMargins(0,0,0,0); }
    virtual bool checkReadPermission() const { return true; }
    virtual bool checkWritePermission() const { return true; }

private:
    QString getLicenseKeyFileName() const;
};

#endif
