#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "QtUtils.h"

#include "DirSelectDialog.h"
#include "ui_DirSelectDialog.h"

DirSelectDialog::DirSelectDialog(QWidget* parent, const QString& title, const QString& path, bool hidden)
    : Dialog(parent)
    , ui(new Ui::DirSelectDialog)
    , iconFile(":/file.png")
    , iconFolder(":/folder.png")
{
    ui->setupUi(this);
    init();

    fsModel.setIconProvider(this);
    fsModel.setFilter( QDir::Dirs | QDir::AllDirs | QDir::NoDotAndDotDot );
    if ( hidden ) fsModel.setFilter(fsModel.filter() | QDir::Hidden);
    fsModel.setNameFilterDisables(false);
    fsModel.setRootPath("/");

    int s = ::mm_to_px(5);

    ui->listView->setIconSize(QSize(s,s));
    ui->listView->setModel(&fsModel);
    ui->listView->setRootIndex(fsModel.index(path));

    QObject::connect(ui->listView, SIGNAL(clicked(QModelIndex)), this, SLOT(select(QModelIndex)) );
    QObject::connect(ui->listView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)) );

    ui->lblTitle->setText("<b>" + title + "</b>");
    ui->lblPath->setText(path);
    ui->lblPath->setElideMode( Qt::ElideMiddle );
}

DirSelectDialog::~DirSelectDialog()
{
    delete ui;
}

QIcon DirSelectDialog::icon( QFileIconProvider::IconType type ) const
{
    switch ( type )
    {
    case QFileIconProvider::Folder:
        return iconFolder;
        break;
    case QFileIconProvider::File:
        return iconFile;
        break;
    default:
        return QFileIconProvider::icon( type );
    }
}

QIcon DirSelectDialog::icon ( const QFileInfo& info ) const
{
    if ( info.isFile() )
        return iconFile;
    else
    if ( info.isDir() )
        return iconFolder;
    else
        return QFileIconProvider::icon( info );
}

void DirSelectDialog::syncLabel(const QModelIndex &index)
{
    QFileInfo fInfo = fsModel.fileInfo( index );
    ui->lblPath->setText( fInfo.absoluteFilePath() );
}

void DirSelectDialog::accept()
{
    QModelIndex index = ui->listView->selectionModel()->currentIndex();
    QFileInfo fInfo = fsModel.fileInfo(index);
    if ( fInfo.isDir() )
    {
        path = fInfo.absoluteFilePath();
        Dialog::accept();
    }
}

void DirSelectDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {        
        syncLabel(index);
        ui->listView->setRootIndex( index );
    }
}

void DirSelectDialog::on_btnUp_clicked()
{
    QModelIndex oldRoot(ui->listView->rootIndex());
    QModelIndex newRoot(fsModel.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        ui->listView->setRootIndex( newRoot );
        ui->listView->setCurrentIndex( oldRoot );
        syncLabel(oldRoot);
    }
}
