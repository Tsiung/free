<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>DirSelectDialog</name>
    <message>
        <location filename="../DirSelectDialog.ui" line="14"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
</context>
<context>
    <name>FileOpenDialog</name>
    <message>
        <location filename="../FileOpenDialog.ui" line="14"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
</context>
<context>
    <name>FileSaveDialog</name>
    <message>
        <location filename="../FileSaveDialog.ui" line="14"/>
        <source>Save</source>
        <translation>Bewaar</translation>
    </message>
    <message>
        <source>File exists. Overwrite?</source>
        <translation type="vanished">Het bestand bestaat al. Overschrijven?</translation>
    </message>
</context>
<context>
    <name>LanguageDlg</name>
    <message>
        <location filename="../languagedlg.cpp" line="19"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QAndroidPlatformTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="46"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="47"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../common_i18n.cpp" line="4"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="5"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="6"/>
        <source>Save</source>
        <translation>Bewaar</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="7"/>
        <source>&amp;Save</source>
        <translation>&amp;Bewaar</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="8"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="9"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="10"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="11"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="12"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="13"/>
        <source>Apply</source>
        <translation>Toepassen</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="14"/>
        <source>Reset</source>
        <translation>Opnieuw instellen</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="15"/>
        <source>Discard</source>
        <translation>Weggooien</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="16"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="17"/>
        <source>&amp;No</source>
        <translation>&amp;Nee</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="18"/>
        <source>Abort</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="19"/>
        <source>Retry</source>
        <translation>Opnieuw proberen</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="20"/>
        <source>Ignore</source>
        <translation>Negeren</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../common_i18n.cpp" line="22"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="26"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="27"/>
        <source>&amp;Save</source>
        <translation>&amp;Bewaar</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="28"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="29"/>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../common_i18n.cpp" line="24"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <location filename="../common_i18n.cpp" line="31"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="32"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="33"/>
        <source>Save</source>
        <translation>Bewaar</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="34"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="35"/>
        <source>&amp;No</source>
        <translation>&amp;Nee</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="36"/>
        <source>Abort</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="37"/>
        <source>Retry</source>
        <translation>Opnieuw proberen</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="38"/>
        <source>Ignore</source>
        <translation>Negeren</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="39"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="40"/>
        <source>Cancel</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="41"/>
        <source>Discard</source>
        <translation>Weggooien</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="42"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="43"/>
        <source>Apply</source>
        <translation>Toepassen</translation>
    </message>
    <message>
        <location filename="../common_i18n.cpp" line="44"/>
        <source>Reset</source>
        <translation>Opnieuw instellen</translation>
    </message>
</context>
<context>
    <name>QtUtils</name>
    <message>
        <location filename="../QtUtils.cpp" line="125"/>
        <source>File exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="126"/>
        <source>File &quot;%1&quot; already exists. Do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="424"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../QtUtils.cpp" line="425"/>
        <source>No Internet connection!
Retry?</source>
        <translation>Geen verbinding!
Probeer opnieuw?</translation>
    </message>
</context>
<context>
    <name>RotationDlg</name>
    <message>
        <location filename="../rotationdlg.ui" line="14"/>
        <source>Screen rotation</source>
        <translation>Schermrotatie</translation>
    </message>
</context>
<context>
    <name>WifiDialog</name>
    <message>
        <location filename="../wifidialog.ui" line="40"/>
        <source>Cancel</source>
        <translation>Onderbreek</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../Application.cpp" line="108"/>
        <location filename="../Application.cpp" line="115"/>
        <source>Error:</source>
        <translation>Fout:</translation>
    </message>
</context>
</TS>
