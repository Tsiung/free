#include <QKeyEvent>

#include "QtUtils.h"
#include "messagebox.h"

#include "FileSaveDialog.h"
#include "ui_FileSaveDialog.h"

FileSaveDialog::FileSaveDialog(QWidget* parent, const QString& path, const QString& ext)
    : Dialog(parent)
    , ui( new Ui::FileSaveDialog )
    , fileExt(ext)
{
    ui->setupUi(this);
    init();

    QStringList filters;
    filters.append(QString("*") + ext);
    fsModel.setNameFilters(filters);
    fsModel.setNameFilterDisables(false);
    fsModel.setRootPath("/");
    ui->treeView->setModel(&fsModel);
    ui->treeView->setRootIndex(fsModel.index(path));
    ui->treeView->setHeaderHidden(true);
    ui->treeView->setColumnHidden(1,true);
    ui->treeView->setColumnHidden(2,true);
    ui->treeView->setColumnHidden(3,true);

    connect(ui->treeView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)));
    connect(ui->treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(select(QModelIndex)));
}

FileSaveDialog::~FileSaveDialog()
{
    delete ui;
}

void FileSaveDialog::setFileName(const QString &fn)
{
    ui->lineEdit->setText( fn );
}

void FileSaveDialog::keyPressEvent(QKeyEvent* e)
{
    switch( e->key() )
    {
    case Qt::Key_Menu:
        ui->btnUp->setFocus();
        break;
    default:
        break;
    }
}

void FileSaveDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
        {
            ui->treeView->setExpanded( index, !ui->treeView->isExpanded( index ) );
        }
        else
        {
            QFileInfo fInfo = fsModel.fileInfo(index);
            if ( fInfo.isFile() )
            {
                ui->lineEdit->setText( fInfo.fileName() );
            }
        }
    }
}

void FileSaveDialog::accept()
{
    if ( ui->lineEdit->text().isEmpty() ) return;

    QModelIndex index(ui->treeView->selectionModel()->currentIndex());

    if ( !index.isValid() )
        index = ui->treeView->rootIndex();

    QFileInfo fInfo = fsModel.fileInfo(index);
    if ( fInfo.isFile() )
    {
        fileName = fInfo.absolutePath() + QDir::separator() + ui->lineEdit->text();
    }
    else
    if ( fInfo.isDir() )
    {
        fileName = fInfo.absoluteFilePath() + QDir::separator() + ui->lineEdit->text();
    }

    if ( !defaultSuffix.isEmpty() && QFileInfo(fileName).suffix() != defaultSuffix )
        fileName += "." + defaultSuffix;

    if ( ::fileExistsAndNoOverwrite( this, fileName ) )
        Dialog::reject();
    else
        Dialog::accept();
}

void FileSaveDialog::onBtnUp()
{
    QModelIndex oldRoot(ui->treeView->rootIndex());
    QModelIndex newRoot(fsModel.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        ui->treeView->setRootIndex( newRoot );
        ui->treeView->setCurrentIndex( oldRoot );
    }
}
