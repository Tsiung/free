#ifndef LANGUAGEDLG_H
#define LANGUAGEDLG_H

#include "Dialog.h"

class QListWidgetItem;

namespace Ui {
class LanguageDlg;
}

class LanguageDlg : public Dialog
{
    Q_OBJECT

public:
    LanguageDlg(QWidget *parent = 0);
    ~LanguageDlg();

public slots:
    virtual void accept();
    void itemActivated(QListWidgetItem* item);

private:
    Ui::LanguageDlg *ui;
};

#endif // LANGUAGEDLG_H
