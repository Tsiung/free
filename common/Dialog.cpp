#include <QList>
#include <QLabel>
#include <QCheckBox>
#include <QGroupBox>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QListView>
#include <QTreeView>
#include <QKeyEvent>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>

#include "QtUtils.h"
#if defined(Q_WS_QWS)
 #include "VirtualKeyboard.h"
#endif
#include "Platform.h"
#include "Config.h"

#include "Dialog.h"

void Dialog::init()
{
#if defined(DESKTOP)
    // restore geometry
    QString name( objectName().toLower() + "_geometry" );
    restoreGeometry( QByteArray::fromHex( g_pConfig->readQString( name ).toLatin1() ) );
#else
    initTopLevelWidget(this);
    if ( !fullScreen )
        adjustSize();
#endif
}

void Dialog::setFocusToWidget(QWidget* w)
{
    QTimer::singleShot(0, w, SLOT(setFocus()));
}

int Dialog::exec()
{  
#if !defined(DESKTOP)
    if ( fullScreen )
    {
        // -- not working! bad QComboBox items spacing!
        // setWindowState(windowState() | Qt::WindowFullScreen);
        // -- interesting variant as QDialog title is also shown
        // setWindowState(windowState() | Qt::WindowMaximized);
        // -- working variant for Qt 4.8.5
 #if defined(ANDROID)
        move(0,0);
        setWindowState(windowState() | Qt::WindowMaximized);
 #elif defined(WINRT) || defined(IOS) || defined(SIMPLE)
        // nothing to do
 #else
        showFullScreen();
 #endif
    }
    else
    {
 #if defined(Q_WS_QWS)
        // TODO: create separate class to add it into
        // common stylesheet
        if ( windowFlags() & Qt::FramelessWindowHint )
            setStyleSheet("QDialog {border: 2px solid black}");
        // TODO: end
 #elif defined(WINRT) || defined(IOS) || defined(ANDROID) || defined(SIMPLE)
        setWindowFlags( Qt::Tool );
        resize(sizeHint());
 #endif
    }
#endif

#if !defined(DESKTOP)
    if ( lockOrientation )
        Platform::get()->lockOrientation();
#endif

    int result = QDialog::exec();

#if !defined(DESKTOP)
    if ( lockOrientation )
        Platform::get()->unlockOrientation();
#endif
    
    return result;
}

#if defined(DESKTOP)
void Dialog::done( int r )
{
    // save geometry
    QString name( objectName().toLower() + "_geometry" );
    g_pConfig->writeQString( name, QString::fromLatin1( saveGeometry().toHex() ) );
    QDialog::done( r );
}
#endif

#if defined(Q_WS_QWS)
bool Dialog::eventFilter(QObject* obj, QEvent* event)
{
    return filter(obj, event);
}
#endif

