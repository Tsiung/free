#ifndef ENTERTEXTLINEDIALOG_H_
#define ENTERTEXTLINEDIALOG_H_

#include "Dialog.h"

namespace Ui {
class EnterTextLineDialog;
}

class EnterTextLineDialog : public Dialog
{
    Q_OBJECT

    Ui::EnterTextLineDialog* ui;

public:
    EnterTextLineDialog(QWidget* parent, const QString& title, const QString& prompt, const QString& value = "");
    ~EnterTextLineDialog();

    QString getValue() const;

private slots:
    void onSelectText();
};

#endif /* ENTERTEXTLINEDIALOG_H_ */
