#include <QEvent>
#include <QKeyEvent>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QListView>
#include <QTreeView>
#include <QTextEdit>
#include <QPushButton>
#include <QToolButton>
#include <QCheckBox>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QCoreApplication>
#include <QDebug>

#if defined(Q_WS_QWS)
 #include "VirtualKeyboardContainer.h"
#endif

#include "WidgetEventFilter.h"

WidgetEventFilter::WidgetEventFilter(QWidget* t, VirtualKeyboardContainer* v)
    : tlw(t)
    , vkc(v)
{}

bool WidgetEventFilter::filter(QObject *obj, QEvent *event)
{
    QWidget* w = qobject_cast<QWidget*>(obj);

    switch ( event->type() )
    {
#if defined (Q_WS_QWS)
    case QEvent::RequestSoftwareInputPanel:
        vkc->showVirtualKeyboard( w );
        w->update();
        break;

    case QEvent::CloseSoftwareInputPanel:
        vkc->showVirtualKeyboard( NULL );
        w->update();
        break;
#endif

    default:
        break;
    }

    return false;
}
