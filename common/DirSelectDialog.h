#ifndef FILEOPENDIALOG_H_
#define FILEOPENDIALOG_H_

#include <QFileSystemModel>
#include <QTreeView>
#include <QFileIconProvider>

#include "Dialog.h"

class QKeyEvent;

namespace Ui {
class DirSelectDialog;
}

class DirSelectDialog: public Dialog, public QFileIconProvider
{
    Q_OBJECT

    Ui::DirSelectDialog* ui;

    QFileSystemModel fsModel;
    QString path;

    QIcon iconFile;
    QIcon iconFolder;

public:
    DirSelectDialog(QWidget* parent, const QString& title, const QString& path, bool hidden=false);
    ~DirSelectDialog();

    const QString& getPath() const
    { return path; }

    // QFileIconProvider
    virtual QIcon icon ( QFileIconProvider::IconType type ) const;
    virtual QIcon icon ( const QFileInfo & info ) const;

private:
    void syncLabel( const QModelIndex& index );

private slots:
    virtual void accept();
    void select( const QModelIndex& index );
    void on_btnUp_clicked();
};

#endif /* FILEOPENDIALOG_H_ */
