#ifndef SINGLEAPPGUARD_H
#define SINGLEAPPGUARD_H

#include <QObject>
#include <QSharedMemory>
#include <QSystemSemaphore>

class SingleAppGuard
{
public:
    SingleAppGuard( const QString& key );
    bool tryToRun();

private:
    const QString key;
    const QString memLockKey;
    const QString sharedMemKey;

    QSharedMemory sharedMem;
    QSystemSemaphore memLock;

    Q_DISABLE_COPY( SingleAppGuard )
};

#endif // SINGLEAPPGUARD_H
