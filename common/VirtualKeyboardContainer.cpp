#include <QTextEdit>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "VirtualKeyboardContainer.h"

void VirtualKeyboardContainer::showVirtualKeyboard(QWidget *w)
{
    if ( w != NULL )
    {
        QPlainTextEdit* pte = qobject_cast<QPlainTextEdit*>(w);
        if ( pte != NULL ) {
            vk.show();
            pte->ensureCursorVisible();
            return;
        }
        QTextEdit* te = qobject_cast<QTextEdit*>(w);
        if ( te != NULL ) {
            vk.show();
            te->ensureCursorVisible();
            return;
        }
        if ( vk.persistent() ) {
            vk.show();
            return;
        }
        if (    qobject_cast<QLineEdit*>(w)
             || qobject_cast<QSpinBox*>(w)
             || qobject_cast<QDoubleSpinBox*>(w) )
        {
            QPoint pg(w->mapToGlobal( QPoint( 0, 0 ) ) );
            QPoint pd(parent->mapFromGlobal( pg ) );

            int origin = pd.y();
            int keyboardWidth = parent->width() - 10;
            int keyboardHeight = std::min(vk.heightForWidth( keyboardWidth ), parent->height() - w->height() - 5);

            QRect kRect;
            if ( origin + w->height() + keyboardHeight <= parent->height() )
            {
                // keyboard can be placed below the widget
                kRect = QRect( 0, origin + w->height(), keyboardWidth, keyboardHeight );
            }
            else
            if ( origin - keyboardHeight >= 0 )
            {
                // keyboard can be placed above the widget
                kRect = QRect( 0, origin - keyboardHeight, keyboardWidth, keyboardHeight );
            }
            else
            {
                // we need to resize the dialog to place the keyboard below the widget
                int remainHeight = parent->height() - origin - w->height();
                extraSize = keyboardHeight - remainHeight;
                QPoint p(parent->pos());
                parent->move( p.x(), p.y() - extraSize );
                QSize s(parent->size());
                parent->resize( s.width(), s.height() + extraSize );
                kRect = QRect( 0, origin + w->height(), keyboardWidth, keyboardHeight );
            }

            if ( kRect.isValid() )
            {
                vk.setGeometry( kRect );
                vk.raise();
                vk.show();
            }
            else
            {
                vk.hide();
            }
        }
    }
    else
    {
        if ( extraSize > 0 )
        {
            QPoint p(parent->pos());
            parent->move( p.x(), p.y() + extraSize );
            QSize s(parent->size());
            parent->resize( s.width(), s.height() - extraSize );
            extraSize = 0;
        }
        vk.hide();
    }
}
