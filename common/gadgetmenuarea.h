#ifndef GADGETMENUAREA_H
#define GADGETMENUAREA_H

#include <QVector>
#include <QAbstractScrollArea>

#include "gadgetmenuitem.h"

class GadgetMenu;

class GadgetMenuArea : public QAbstractScrollArea
{
    Q_OBJECT

    QVector<GadgetMenuItem> items;

    int highlight;

public:
    GadgetMenuArea(QWidget* parent);

    void addAction(QAction* a);
    void addMenu(GadgetMenu* m);
    void addSeparator();
    void layout();

    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *);
    QSize viewportSizeHint() const;

private:
    bool checkSubmenuExists() const;
    void drawSubmenuArrow( QPainter* p, const QRectF& rect ) const;
    void drawCheckItem( QPainter* p, const QRectF& rect, bool checked ) const;
    void drawRadioItem( QPainter* p, const QRectF& rect, bool checked ) const;

signals:
    void activateAction(QAction*);
    void activateSubmenu(GadgetMenu*);
};

#endif // GADGETMENUAREA_H
