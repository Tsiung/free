#include <QAction>
#include <QPainter>
#include <QPaintEvent>
#include <QScrollBar>
#include <QStyleOption>
#if QT_VERSION >= 0x050000
#include <QtMath>
#else
#include <qmath.h>
#endif
#include <QDebug>

#include "QtUtils.h"

#include "gadgetmenuarea.h"

#define GAP_FACTOR 0.3
#define SEP_FACTOR 0.3

GadgetMenuArea::GadgetMenuArea(QWidget* parent)
    : QAbstractScrollArea(parent)
    , highlight(-1)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setAttribute(Qt::WA_StyledBackground, true);
    ::activate_scroller(this);
}

void GadgetMenuArea::addAction(QAction* a)
{
    items.push_back(GadgetMenuItem(a));
}

void GadgetMenuArea::addMenu(GadgetMenu *m)
{
    items.push_back(GadgetMenuItem(m));
}

void GadgetMenuArea::addSeparator()
{
    items.push_back(GadgetMenuItem());
}

void GadgetMenuArea::layout()
{
    QFontMetricsF met(font());
    qreal gap = GAP_FACTOR * met.height();
    qreal sep = SEP_FACTOR * met.height();
    qreal y = gap;

    bool submenu = checkSubmenuExists();
    int  sepIdx   = -1;
    int  noSepIdx = -1;

    for (int i=0; i<items.size(); ++i ) {
        GadgetMenuItem& item = items[i];
        item.rect = item.rectSuffix = item.rectPrefix = item.rectText = QRectF();
        if (    ( GadgetMenuItem::Action == item.type || GadgetMenuItem::Menu == item.type )
             && item.isVisible() )
        {
            if ( sepIdx >= 0 )
            {
                items[sepIdx].rect = QRectF(gap, y, viewport()->width()-2*gap, sep );
                y += items[sepIdx].rect.height();
                sepIdx = -1;
            }

            noSepIdx = i;

            QString text = item.text();
            item.rect = QRectF(0, y, viewport()->width(), met.height());

            // check / uncheck
            qreal prefixGap = 0.0;
            if ( GadgetMenuItem::Action == item.type )
            {
                QAction* action = static_cast<QAction*>(item.ptr);
                if ( action->isCheckable() )
                {
                    qreal prefixHeight = 0.7*item.rect.height();
                    qreal prefixWidth = prefixHeight;
                    qreal prefixX = item.rect.x() + gap;
                    qreal prefixY = item.rect.y() + 0.15 * item.rect.height();
                    prefixGap = gap;
                    item.rectPrefix = QRectF(prefixX, prefixY, prefixWidth, prefixHeight);
                }
            }

            // submenu rect if needed
            qreal suffixGap = 0.0;
            if ( submenu )
            {
                qreal suffixHeight = 0.7*item.rect.height();
                qreal suffixWidth = 0.5*suffixHeight;
                qreal suffixX = item.rect.x() + item.rect.width() - gap - suffixWidth;
                qreal suffixY = item.rect.y() + 0.15 * item.rect.height();
                suffixGap = gap;
                item.rectSuffix = QRectF(suffixX, suffixY, suffixWidth, suffixHeight);
            }

            // text rect
            item.rectText = QRectF( item.rect.x()+gap+item.rectPrefix.width()+prefixGap, y, item.rect.x()+item.rect.width()-item.rectPrefix.width()-item.rectSuffix.width()-prefixGap-suffixGap-2*gap, item.rect.height() );

            y += item.rect.height();
        }
        else
        if ( GadgetMenuItem::Separator == item.type && noSepIdx >= 0 )
        {
            sepIdx = i;
            noSepIdx = -1;
        }
    }

    qreal height = y + gap;
    QScrollBar* vbar = verticalScrollBar();
    vbar->setMinimum(0);
    int max = qCeil(height) - viewport()->height();
    vbar->setMaximum(max);
    vbar->setPageStep(viewport()->height());
    vbar->setValue(0);
}

QSize GadgetMenuArea::sizeHint() const
{
    QSize s(viewportSizeHint());
    return QSize(s.width()+2*frameWidth(), s.height()+2*frameWidth());
}

void GadgetMenuArea::paintEvent(QPaintEvent* e)
{
    QPainter p(viewport());
    QFontMetricsF met(font());
    QPalette pal(palette());
    QScrollBar* vbar = verticalScrollBar();

    p.setRenderHint(QPainter::Antialiasing, true);

    for (int i=0; i<items.size(); ++i )
    {
        GadgetMenuItem& item = items[i];
        if ( item.rect.isValid() && item.rect.translated(0, -vbar->value()).intersects( e->rect() ) )
        {
            if (    ( GadgetMenuItem::Action == item.type || GadgetMenuItem::Menu == item.type )
                 && item.isVisible() )
            {
                // draw background
                if ( i == highlight ) {
                    p.setBrush( pal.brush(QPalette::Highlight) );
                    p.setPen(Qt::NoPen);
                    p.drawRect( QRectF( item.rect.x(), item.rect.y(), item.rect.width(), item.rect.height() ).translated(0, -vbar->value()) );
                }

                QColor color(pal.color( QPalette::Text ));
                if ( i == highlight )
                {
                    color = pal.color( QPalette::HighlightedText );
                }
                else
                if ( !item.isEnabled() )
                {
                    color = pal.color(QPalette::Disabled, QPalette::Text);
                }

                p.setPen(color);
                p.setBrush(color);

                // draw prefix
                if ( GadgetMenuItem::Action == item.type )
                {
                    QAction* action = static_cast<QAction*>(item.ptr);
                    if ( !item.rectPrefix.isNull() )
                    {
                        if ( action->actionGroup() )
                            drawRadioItem(&p, item.rectPrefix.translated(0, -vbar->value()), action->isChecked());
                        else
                            drawCheckItem(&p, item.rectPrefix.translated(0, -vbar->value()), action->isChecked());
                    }
                }

                // draw suffix
                if (    GadgetMenuItem::Menu == item.type
                     && !item.rectSuffix.isNull() )
                {
                    drawSubmenuArrow(&p, item.rectSuffix.translated(0, -vbar->value()));
                }

                // draw text
                p.drawText(item.rectText.translated(0, -vbar->value()),
                           Qt::AlignLeft | Qt::AlignVCenter,
                           met.elidedText(item.text(), Qt::ElideRight, item.rectText.width() ) );
            }
            else
            if ( GadgetMenuItem::Separator == item.type )
            {
                p.setPen(pal.color( QPalette::Text ));
                p.drawLine(QLineF(item.rect.left(), item.rect.top()+item.rect.height()/2-vbar->value(), item.rect.right(), item.rect.top()+item.rect.height()/2-vbar->value()));
            }
        }
    }
}

void GadgetMenuArea::mousePressEvent(QMouseEvent* e)
{
    QScrollBar* vbar = verticalScrollBar();
    QPoint pos(e->x(), e->y()+vbar->value());
    for ( int i=0; i<items.size(); ++i ) {
        GadgetMenuItem& item = items[i];
        if ( item.rect.contains(pos) && item.isEnabled() )
        {
            if ( highlight >= 0 ) {
                viewport()->update(items[highlight].rect.toAlignedRect().translated(0, -vbar->value()));
            }
            highlight = i;
            viewport()->update(items[highlight].rect.toAlignedRect().translated(0, -vbar->value()));
        }
    }
}

void GadgetMenuArea::mouseReleaseEvent(QMouseEvent *)
{
    if ( highlight >= 0 )
    {
        GadgetMenuItem& item = items[highlight];
        highlight = -1;
        switch( item.type ) {
        case GadgetMenuItem::Action :
            emit activateAction( static_cast<QAction*>(item.ptr) );
            break;
        case GadgetMenuItem::Menu :
            emit activateSubmenu( static_cast<GadgetMenu*>(item.ptr) );
            break;
        default:
            break;
        }
    }
}

void GadgetMenuArea::resizeEvent(QResizeEvent*)
{
    layout();
}

QSize GadgetMenuArea::viewportSizeHint() const
{
    QFontMetricsF met(font());

    qreal gap = GAP_FACTOR * met.height();
    qreal sep = SEP_FACTOR * met.height();

    bool submenu = checkSubmenuExists();
    int  sepIdx   = -1;
    int  noSepIdx = -1;

    qreal height = gap;
    qreal width = 0.0;

    for (int i=0; i<items.size(); ++i ) {
        const GadgetMenuItem& item = items[i];
        if (    ( GadgetMenuItem::Action == item.type || GadgetMenuItem::Menu == item.type )
             && item.isVisible() )
        {
            if ( sepIdx >= 0 )
            {
                height += sep;
                sepIdx = -1;
            }

            noSepIdx = i;

            qreal prefixWidth = 0.0;
            qreal prefixGap = 0.0;
            if ( GadgetMenuItem::Action == item.type )
            {
                QAction* action = static_cast<QAction*>(item.ptr);
                if ( action->isCheckable() )
                {
                    prefixWidth = 0.7*met.height();
                    prefixGap = gap;
                }
            }

            qreal suffixWidth = 0.0;
            qreal suffixGap = 0.0;
            if ( submenu )
            {
                suffixWidth = 0.7*met.height();
                suffixGap = gap;
            }

            qreal textWidth = met.width(item.text());

            width = qMax(width, 2*gap + prefixWidth + prefixGap + textWidth + suffixWidth + suffixGap);
            height += met.height();
        }
        else
        if ( GadgetMenuItem::Separator == item.type && noSepIdx >= 0 )
        {
            sepIdx = i;
            noSepIdx = -1;
        }
    }

    height += gap;

    return QSize(qCeil(width), qCeil(height));
}

bool GadgetMenuArea::checkSubmenuExists() const
{
    for (int i=0; i<items.size(); ++i)
        if ( GadgetMenuItem::Menu == items[i].type )
            return true;
    return false;
}

void GadgetMenuArea::drawSubmenuArrow(QPainter *p, const QRectF &rect ) const
{
    QPainterPath path;
    path.moveTo(rect.x(), rect.y());
    path.lineTo(rect.x()+rect.width(), rect.y()+rect.height()/2);
    path.lineTo(rect.x(), rect.y()+rect.height());
    path.lineTo(rect.x(), rect.y());
    p->drawPath(path);
    p->fillPath(path, QBrush(p->pen().color()) );
}

void GadgetMenuArea::drawCheckItem(QPainter *p, const QRectF &rect, bool checked) const
{
    QBrush brush(p->brush());
    QPen pen(p->pen());
    p->setBrush(Qt::NoBrush);
    p->drawRect(rect);
    if ( checked ) {
        QPen pen1(pen);
        pen1.setWidthF(0.1*rect.width());
        p->setPen(pen1);
        p->drawLine(QLineF(rect.x()+0.2*rect.width(), rect.y()+0.5*rect.height(), rect.x()+0.5*rect.width(), rect.y()+0.8*rect.height()));
        p->drawLine(QLineF(rect.x()+0.5*rect.width(), rect.y()+0.8*rect.height(), rect.x()+0.8*rect.width(), rect.y()+0.2*rect.height()));
    }
    p->setBrush(brush);
    p->setPen(pen);
}

void GadgetMenuArea::drawRadioItem(QPainter *p, const QRectF &rect, bool checked ) const
{
    QBrush b(p->brush());
    p->setBrush(Qt::NoBrush);
    p->drawEllipse(rect);
    if ( checked ) {
        p->setBrush(b);
        p->drawEllipse( rect.adjusted( 0.3*rect.width(), 0.3*rect.height(), -0.3*rect.width(), -0.3*rect.height() ) );
    }
}
