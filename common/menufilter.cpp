#include <QKeyEvent>

#include "menufilter.h"

MenuFilter::MenuFilter(QObject *parent)
    : QObject(parent)
    , isPressed(false)
{
}

bool MenuFilter::eventFilter(QObject*, QEvent *event)
{
    bool skip = false;
    switch ( event->type() )
    {
    case QEvent::MouseButtonPress:
        isPressed = true;
        break;
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
        if ( !isPressed ) skip = true;
        break;
    default:
        return false;
        break;
    }

    return skip;
}

