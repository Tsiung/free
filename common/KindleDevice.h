#if !defined(KINDLE_DEVICE_H)
#define KINDLE_DEVICE_H

enum KindleDevice {
    KindleOther = 0,
    KindleTouch,
    KindleTouch2,
    KindleTouch3,
    KindleTouch4,
    KindlePaperWhite,
    KindlePaperWhite2,
    KindlePaperWhite3,
    KindlePaperWhite4,
    KindleVoyage,
    KindleOasis,
    KindleOasis2
};

inline KindleDevice get_kindle_device()
{
    QString d(qgetenv("DEVICE"));
    if ( d == "KT" ) return KindleTouch;
    else if ( d == "KT2" ) return KindleTouch2; 
    else if ( d == "KT3" ) return KindleTouch3;
    else if ( d == "KT4" ) return KindleTouch4;
    else if ( d == "KPW" ) return KindlePaperWhite;
    else if ( d == "KPW2" ) return KindlePaperWhite2;
    else if ( d == "KPW3" ) return KindlePaperWhite3;
    else if ( d == "KPW4" ) return KindlePaperWhite4;
    else if ( d == "KV" ) return KindleVoyage;
    else if ( d == "KOA" ) return KindleOasis;
    else if ( d == "KOA2" ) return KindleOasis2;
    return KindleOther;
}

#endif

