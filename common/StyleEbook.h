#ifndef STYLEEBOOK_H_
#define STYLEEBOOK_H_

#include <QIcon>
#include <QProxyStyle>
#include <QDebug>

class StyleEbook: public QProxyStyle
{
Q_OBJECT

    int scrollBarSize;
    int buttonIconSize;

public:
    StyleEbook( QStyle* style );
	virtual ~StyleEbook()
	{}

    void setScrollBarSize( int val )
    { scrollBarSize = val; }

    void setButtonIconSize( int val )
    { buttonIconSize = val; }

	virtual int pixelMetric(PixelMetric metric, const QStyleOption* option = 0, const QWidget* widget = 0) const
	{
        switch( metric )
        {
            case PM_ScrollBarExtent:
                return scrollBarSize;
            case PM_LayoutHorizontalSpacing:
                return 10;
            case PM_LayoutVerticalSpacing:
                return 10;
            case PM_LayoutLeftMargin:
            case PM_LayoutRightMargin:
            case PM_LayoutTopMargin:
            case PM_LayoutBottomMargin:
                return 10;
            case PM_ButtonIconSize:
                return buttonIconSize;
            default:
                return QProxyStyle::pixelMetric(metric, option, widget);
        }
	}

    int styleHint(StyleHint hint, const QStyleOption* option = 0, const QWidget* widget = 0, QStyleHintReturn * returnData = 0) const
    {
        Q_UNUSED(option)
        Q_UNUSED(widget)
        Q_UNUSED(returnData)
        switch (hint)
        {
        case SH_Menu_SubMenuPopupDelay:
            return 500;
            break;
#if QT_VERSION >= 0x050000 && !defined(DESKTOP)
        case SH_Menu_SubMenuSloppyCloseTimeout:
            return 5000;
            break;
#endif
#if defined(Q_WS_QWS)
        case SH_RequestSoftwareInputPanel:
            return QStyle::RSIP_OnMouseClick;
            break;
#endif
        default:
            break;
        }
        return QProxyStyle::styleHint(hint,option,widget,returnData);
    }
};

#endif /* STYLEEBOOK_H_ */

