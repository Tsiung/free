#ifndef COMPAT_H
#define COMPAT_H

#include <QSize>

class QString;
class QFontMetrics;
class QComboBox;

int font_metrics_width(const QFontMetrics&, const QString&);
void set_current_text(QComboBox&, const QString&);

#endif // COMPAT_H

