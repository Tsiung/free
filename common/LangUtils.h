#ifndef LANGUTILS_H
#define LANGUTILS_H

#include <QString>

int getLangIndex();
QString getLangName();
QString getLangForWebLink();
void loadTranslations();
QString trans(const char* ctx, const char* text);
QString trans(const char* ctx, const QString& text);

#endif // LANGUTILS_H
