#ifndef MENUFILTER_H
#define MENUFILTER_H

#include <QObject>

class MenuFilter : public QObject
{
    Q_OBJECT

    bool isPressed;

public:
    MenuFilter(QObject *parent = 0);

protected:
     bool eventFilter(QObject*, QEvent *event);
};

#endif // MENUFILTER_H
