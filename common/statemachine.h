#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QVector>

class StateMachineCallback;

class StateMachine
{
public:
    struct Transition
    {
        int oldState;
        int newState;
        int event;
        Transition( int os, int ns, int e )
            : oldState(os)
            , newState(ns)
            , event(e)
        {}
        Transition()
            : oldState(0)
            , newState(0)
            , event(0)
        {}
    };

    StateMachine( int state, StateMachineCallback* pCallback = NULL );

    void event( int event );

protected:
    int state_;
    StateMachineCallback* pCallback_;
    QVector<Transition> transitions;
};

class StateMachineCallback
{
public:
    virtual void onStateChanged( const StateMachine::Transition& t ) = 0;
};

#endif // STATEMACHINE_H
