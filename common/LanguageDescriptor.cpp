#include <QDir>
#include <QStringList>

#include "Platform.h"
#include "Exception.h"
#include "Config.h"
#include "Factory.h"

#include "LanguageDescriptor.h"

LanguageDescriptor::LanguageDescriptor()
{}

LanguageDescriptor::LanguageDescriptor( const QString& str )
{
    static const char* msg = "Invalid language description: %s [File: %s, Line: %d]";

    QStringList list(str.split("|"));

    if ( list.size() < 4 )
    {
        throw Exception( true, msg, str.toUtf8().constData(), __FILE__, __LINE__ );
    }

    name   = list.at(0).trimmed();
    native = list.at(1).trimmed();
    trans  = list.at(2).trimmed();
    kbd    = list.at(3).trimmed();
}

QString LanguageDescriptor::getTransFileName() const
{
    return trans;
}

QString LanguageDescriptor::getKbdFileName() const
{
    return Platform::get()->getTranslationsPath() + QDir::separator() + kbd;
}
