QT += network
INCLUDEPATH += $$PWD $$PWD/platform
DEPENDPATH  += $$PWD $$PWD/platform
VPATH       += $$PWD $$PWD/platform

debug_and_release {
    CONFIG(debug, debug|release) {
        DESTDIR = debug
    }
    else {
        DESTDIR = release
    }
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

kindle {
    ARCH = kindle
    CONFIG += eink release
    QT += dbus
    DEFINES += KINDLE LINUX
    HEADERS += QWSPlatform.h KindlePlatform.h
    lib {
        SOURCES += QWSPlatform.cpp KindlePlatform.cpp
    }
}

obreey {
    ARCH = obreey
    CONFIG += eink release
    DEFINES += OBREEY LINUX
    HEADERS += QWSPlatform.h InkViewPlatform.h
    lib {
        SOURCES += QWSPlatform.cpp InkViewPlatform.cpp
    }
    else {
        LIBS += -linkview
    }
}

kobo {
    ARCH = kobo
    CONFIG -= debug
    CONFIG += eink release
    DEFINES += KOBO LINUX
    HEADERS += QWSPlatform.h KoboPlatform.h
    lib {
        SOURCES += QWSPlatform.cpp KoboPlatform.cpp
    }
    LIBS += -ldl
}

qvfb {
    ARCH = qvfb
    CONFIG += debug
    CONFIG -= release
    DEFINES += QVFB LINUX EINK
    HEADERS += QWSPlatform.h QvfbPlatform.h
    lib {
        SOURCES += QWSPlatform.cpp QvfbPlatform.cpp
    }
}

linux-g++|linux-g++-32|linux-g++-64 {
    DEFINES += LINUX
    !qvfb {
        simple {
            DEFINES += SIMPLE
            CONFIG += simple
        }
        else {
            DEFINES += DESKTOP
            CONFIG += desktop
        }
        HEADERS += DesktopPlatform.h
        lib {
                SOURCES += DesktopPlatform.cpp
        }
    }
}

windows {
    CONFIG += desktop
    DEFINES += DESKTOP WINDOWS
    HEADERS += DesktopPlatform.h
    lib {
        SOURCES += DesktopPlatform.cpp
    }
}

macx {
    CONFIG += desktop exceptions
    DEFINES += DESKTOP MACX
    HEADERS += DesktopPlatform.h
    QMAKE_CFLAGS += -gdwarf-2
    QMAKE_CXXFLAGS += -gdwarf-2
    lib {
        SOURCES += DesktopPlatform.cpp
    }
    amazon_store {
        DEFINES += AMAZON_STORE
    }
    else {
        DEFINES += APPLE_STORE
    }
} 

ios {
    DEFINES += IOS
    DEFINES += APPLE_STORE
    CONFIG(simulator, device|simulator): DEFINES += SIMULATOR
}

android {
    QT += androidextras network
    HEADERS += AndroidPlatform.h
    DEFINES += ANDROID
    lib {
        SOURCES += AndroidPlatform.cpp
        RESOURCES += resources/gadget/gadget.qrc
    }
}



eink {
    DEFINES += EINK
}

lib {
    RESOURCES += $$PWD/resources/keyboard/keyboard.qrc $$PWD/resources/rotation/rotation.qrc
    eink|qvfb|simple {
        RESOURCES += $$PWD/resources/icons/icons.qrc
    }
}
else {
    windows {
        debug {        
            LIBS  += -L../free/common/debug
        }
        release {
            LIBS  += -L../free/common/release
        }
        LIBS  += -lcommon
    } 
    else {
        LIBS  += -L$$COMMON -lcommon
        !debug {
            QMAKE_LFLAGS += -Wl,-s
        }
    }
}

eink|qvfb {
    QT += svg
}

OBJECTS_DIR = obj
MOC_DIR = moc
RCC_DIR = rcc
UI_DIR = ui

INCLUDEPATH += $$UI_DIR
