TEMPLATE  = lib
CONFIG   += lib
CONFIG += dll plugin debug
SOURCES += SuspendManager.cpp
HEADERS += SuspendManager.h

kobo {
    ARCH = kobo
    DEFINES += KOBO
}
kindle {
    ARCH = kindle
    DEFINES += KINDLE
}   
qvfb {
    ARCH = qvfb
    DEFINES += X86
}
pb {
    ARCH = pb
    DEFINES += POCKETBOOK
}
obreey {
    ARCH = obreey
    DEFINES += OBREEY
}

OBJECTS_DIR = obj
MOC_DIR = moc
RCC_DIR = rcc
UI_DIR = ui
