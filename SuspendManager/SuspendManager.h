#include <QTimer>
#include <QVector>

#ifndef H__SUSPEND_MANAGER
#define H__SUSPEND_MANAGER

class SuspendManager: public QObject
{
    Q_OBJECT

public:
    enum State
    {
        ssActive,
        ssSuspended,
        ssLast
    };

    enum Event
    {
        evPowerBtn,
        evSleepCoverPressed,
        evSleepCoverReleased,
        evFifo,
        evTimeout,
        evTouchScreen,
        evLast
    };

    struct Transition
    {
        State oldState;
        State newState;
        Event event;
        Transition( State os, State ns, Event e )
            : oldState(os)
            , newState(ns)
            , event(e)
        {}
        Transition()
            : oldState(ssActive)
            , newState(ssActive)
            , event(evLast)
        {}
    };

private:
    SuspendManager();

public:
    static bool active();
    static SuspendManager* instanse();
    static void cleanup();

public:
    void event( Event event );
    void setSuspendTimeout( int val );
    int getSuspendTimeout() const
    { return suspendTimeout_; }
    void activity();
    void startTimer();
    void stopTimer();

    static const char* get_state_name( State state )
    {
        static const char* states[ssLast] = {"Active","Suspended"};
        return states[state];
    }

    static const char* get_event_name( Event event )
    {
        static const char* events[evLast] = {"PowerBtn","SleepCoverPressed","SleepCoverReleased","Fifo","Timeout"};
        return events[event];
    }

public slots:
    void timeout();

signals:
    void stateChanged( const SuspendManager::Transition& t );

private:
#if !defined(POCKETBOOK) && !defined(OBREEY)
    QTimer timer;
#endif
    static SuspendManager* pManager;
    QVector<Transition> transitions;
    State state;
    int suspendTimeout_;
};

#endif
