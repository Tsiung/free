#ifndef KINDLETS_H
#define KINDLETS_H

#include <fcntl.h>
#include <sys/socket.h>
#include <linux/input.h>
#include <linux/netlink.h>

#include <QWSMouseHandler>
#include <QSocketNotifier>
#include <QWSServer>
#include <QTimer>

#include "KindleDevice.h"

class KindleTS : public QObject, public QWSMouseHandler
{
    Q_OBJECT

public:
    KindleTS(const QString & driver = QString(), const QString & device = QString(), QObject* parent = 0);
    virtual ~KindleTS();

    virtual void suspend();
    virtual void resume();

private slots:
    void activity(int);
    void updateOrientation();

private:
    KindleDevice dev;

    int width, height;

    int fd;
    QSocketNotifier* sn;

    bool isDebug;
    bool isTouchActivity;
    bool isTouchPressed;
    bool isInputCaptured;
    bool isSuspended;
    bool isUpsideDown;

    int oldX, oldY;

    QPoint p, oldP;

    QTimer rotTimer;

    bool useSuspendManager;

private:
    void capture_input();
    void release_input();
    void log_input_event(const input_event& e);
};

#endif // KINDLETS_H
