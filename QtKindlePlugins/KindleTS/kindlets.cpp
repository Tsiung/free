#include <unistd.h>

#include <iostream>
#include <algorithm>

#include <QWSServer>
#include <QScreen>
#include <QFile>
#include <QTextStream>

#include "KindleDevice.h"
#include "SuspendManager.h"

#include "kindlets.h"

#define EV_SYN			0x00
#define EV_KEY			0x01
#define EV_REL			0x02
#define EV_ABS			0x03

#define BTN_TOOL_FINGER		0x145
#define BTN_TOUCH		0x14a
#define BTN_TOOL_DOUBLETAP	0x14d

#define KT_HOME 102
#define KV_PAGEUP 104
#define KV_PAGEDOWN 109

#define ABS_MT_SLOT 47
#define ABS_MT_TOUCH_MAJOR 48
#define ABS_MT_WIDTH_MAJOR 50
#define ABS_MT_POSITION_X 53
#define ABS_MT_POSITION_Y 54
#define ABS_MT_TRACKING_ID 57
#define ABS_MT_PRESSURE 58

KindleTS::KindleTS(const QString & driver, const QString & device, QObject* parent) 
    : QObject(parent)
    , QWSMouseHandler(driver, device)
    , dev(get_kindle_device())
    , width(0)
    , height(0)
    , fd(-1)
    , sn(NULL)
    , isDebug(device.contains("debug", Qt::CaseInsensitive))
    , isTouchActivity(false)
    , isTouchPressed(false)
    , isInputCaptured(false)
    , isSuspended(false)
    , isUpsideDown(false)
    , oldX(0)
    , oldY(0)
    , useSuspendManager(SuspendManager::active())
{
    QScreen* pScreen = QScreen::instance();

    width  = pScreen->width();
    height = pScreen->height();

    int ori = pScreen->transformOrientation();

    if ( 1 == ori || 3 == ori )
        std::swap( width, height );

    if (isDebug)
    {
        std::cout << "KindleTS( " << driver.toAscii().constData() << "," << device.toAscii().constData() << ")" << std::endl << std::flush;
        std::cout << "width=" << width << ", height=" << height << std::endl << std::flush;
    }

    // Touch input
    const char* f = NULL; 
    switch( dev )
    {
    case KindleTouch:
        f = "/dev/input/event3";
        break;
    case KindlePaperWhite:
    case KindleTouch3:
        f = "/dev/input/event0";
        break;
    case KindlePaperWhite2:
    case KindlePaperWhite3:
    case KindleTouch2:
    case KindleVoyage:
        f = "/dev/input/event1";
        break;
    case KindleOasis:
        f = "/dev/input/by-path/platform-imx-i2c.1-event"; // from koreader
        break;
    case KindleOasis2:
        f = "/dev/input/event5";
        break;
    case KindlePaperWhite4:
    case KindleTouch4:
        f = "/dev/input/event2";
        break;
    default:
        break;    
    }
    fd = open(f, O_RDONLY);
    if ( fd != -1 )
    {
        sn = new QSocketNotifier(fd, QSocketNotifier::Read);
        connect(sn, SIGNAL(activated(int)), this, SLOT(activity(int)));
        sn->setEnabled(true);
    }
    else
    {
        std::cerr << "fd open FAILED" << std::endl << std::flush;
    }

    capture_input();

    if ( KindleOasis2 == dev )
    {
        updateOrientation();
        connect( &rotTimer, SIGNAL(timeout()), this, SLOT(updateOrientation()) );
        rotTimer.start(1000);
    }
}

KindleTS::~KindleTS()
{
    release_input() ;
    if (fd != -1) 
    {
        delete sn;
        close(fd);
    }
}

void KindleTS::suspend()
{
    isSuspended = true;
}

void KindleTS::resume()
{
    isSuspended = false;
}

#define POS_TH 10

void KindleTS::activity(int)
{
    sn->setEnabled(false);

    int pos = 0;

    input_event in;
    read(fd, &in, sizeof(input_event));

    if ( !isSuspended )
    {
        if (isDebug)
            log_input_event(in);
    
        switch(in.type)
        {
        case EV_ABS:
            switch ( dev )
            {
            case KindleOasis2:
                pos = ABS_MT_POSITION_X == in.code ? ( isUpsideDown? width-in.value : in.value ) : 
                                                     ( isUpsideDown? height-in.value : in.value ) ;
                break;
            case KindleTouch:
                pos = (ABS_MT_POSITION_X == in.code ? width : height) * in.value / 0x1000;
                break;
            default:
                pos = in.value;
            }
            if ( in.code == ABS_MT_POSITION_X && abs(oldX - pos) > POS_TH) 
            {
                p.setX(pos);
                oldX = pos;
            }  
            else 
            if ( in.code == ABS_MT_POSITION_Y && abs(oldY - pos) > POS_TH) 
            {
                p.setY(pos);
                oldY = pos;
            }
            break ;
        case EV_KEY:
            if (in.code == BTN_TOUCH)
            {
                isTouchActivity = true;
                isTouchPressed = in.value != 0;
            }
            break;
        case EV_SYN:
            if ( isTouchActivity )
            {
                mouseChanged(p, isTouchPressed? Qt::LeftButton : 0, 0);
                if ( useSuspendManager )
                    SuspendManager::instanse()->activity();
                if ( isDebug )
                    std::cout << "mouseChanged: x=" << p.x() << ", y=" << p.y() << ", button=" << (isTouchPressed? Qt::LeftButton : 0) << std::endl << std::flush;
            }
            break;
        default:
            break;
        }
    }

    sn->setEnabled(true);
}

void KindleTS::updateOrientation()
{
    bool val = isUpsideDown;

    QString result;
    QFile file( "/sys/class/graphics/fb0/rotate" );
    if( file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream in(&file);
        result = in.readLine().trimmed();
    }

    if ( result == "0" )
        val = false;
    else
    if ( result == "2" )
        val = true;
   
    if ( val != isUpsideDown )
    {
        isUpsideDown = val;
        if ( sender() == &rotTimer )
        {
            QWSServer::instance()->refresh();
        }
    } 
}

void KindleTS::capture_input(void)
{
    int on = 1 ;

    if ( !isInputCaptured )
    {
        if (isDebug)
            std::cout << "attempting to capture input..." << std::endl << std::flush;

        if (fd != -1)
        {
            if (ioctl(fd, EVIOCGRAB, on)) {
                std::cerr << "Capture touch input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = true;
    }
}

void KindleTS::release_input(void)
{
    int off = 0 ;

    if ( isInputCaptured )
    {
        if (isDebug)
            std::cout << "attempting to release input..." << std::endl << std::flush;
        
        if (fd != -1)
        {
            if (ioctl(fd, EVIOCGRAB, off)) {
                std::cerr << "Release touch input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = false;
    }
}

void KindleTS::log_input_event(const input_event& e)
{
    const char* type = NULL;
#define EXPAND_TYPE(v) case v: type = #v; break;
    switch ( e.type )
    {
    EXPAND_TYPE(EV_ABS)
    EXPAND_TYPE(EV_SYN)
    EXPAND_TYPE(EV_KEY)
    }
#undef EXPAND_TYPE

    const char* code = NULL;
#define EXPAND_CODE(v) case v: code = #v; break;
    switch ( e.type )
    {
    case EV_ABS:
        {
            switch ( e.code )
            {
                EXPAND_CODE(ABS_X)
                EXPAND_CODE(ABS_Y)
                EXPAND_CODE(ABS_PRESSURE)
                EXPAND_CODE(ABS_MT_SLOT)
                EXPAND_CODE(ABS_MT_POSITION_X)
                EXPAND_CODE(ABS_MT_POSITION_Y)
                EXPAND_CODE(ABS_MT_TRACKING_ID)
            }
       }
       break;
       
    case EV_KEY:
       {
           switch ( e.code )
           {
               EXPAND_CODE(BTN_TOUCH)
           }
       }
       break;

    case EV_SYN:
       {
           switch ( e.code )
           {
               EXPAND_CODE(SYN_REPORT)
           }
       }
       break;
    }
#undef EXPAND_CODE
    if ( EV_SYN == e.type )
        std::cout << " ========> ";
    std::cout << "TS data: type=";
    if ( type != NULL )
        std::cout << type;
    else
        std::cout << e.type;
    std::cout << ", code=";
    if ( code != NULL )
        std::cout << code;
    else
        std::cout << e.code;
    std::cout << ", value=" << e.value;
    std::cout << std::endl << std::flush;
}

