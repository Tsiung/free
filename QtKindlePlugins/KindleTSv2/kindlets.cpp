#include <unistd.h>

#include <iostream>
#include <algorithm>

#include <QWSServer>
#include <QScreen>
#include <QFile>
#include <QTextStream>

#include "KindleDevice.h"
#include "SuspendManager.h"

#include "kindlets.h"

#define EV_SYN			0x00
#define EV_KEY			0x01
#define EV_REL			0x02
#define EV_ABS			0x03

#define ABS_MT_SLOT 47
#define ABS_MT_TOUCH_MAJOR 48
#define ABS_MT_WIDTH_MAJOR 50
#define ABS_MT_POSITION_X 53
#define ABS_MT_POSITION_Y 54
#define ABS_MT_TRACKING_ID 57
#define ABS_MT_PRESSURE 58

KindleTS::KindleTS(const QString & driver, const QString & device, QObject* parent) 
    : QObject(parent)
    , QWSMouseHandler(driver, device)
    , dev(get_kindle_device())
    , fd(-1)
    , sn(NULL)
    , isDebug(device.contains("debug", Qt::CaseInsensitive))
    , isTouchPressed(false)
    , isTouchActivity(false)
    , isInputCaptured(false)
    , isSuspended(false)
    , slot(0)
    , p(-1,-1)
    , useSuspendManager(SuspendManager::active())
{
    if (isDebug)
    {
        std::cout << "KindleTS(" << driver.toAscii().constData() << "," << device.toAscii().constData() << ")" << std::endl << std::flush;
    }

    // Touch input
    const char* f = NULL; 
    switch( dev )
    {
    case KindlePaperWhite3:
        f = "/dev/input/event1";
        break;
    case KindlePaperWhite4:
    case KindleTouch4:
        f = "/dev/input/event2"; // koreader: frontend/device/kindle/device.lua
        break;
    default:
        break;    
    }
    fd = open(f, O_RDONLY);
    if ( fd != -1 )
    {
        sn = new QSocketNotifier(fd, QSocketNotifier::Read);
        connect(sn, SIGNAL(activated(int)), this, SLOT(activity(int)));
        sn->setEnabled(true);
    }
    else
    {
        std::cerr << "fd open FAILED" << std::endl << std::flush;
    }

    capture_input();
}

KindleTS::~KindleTS()
{
    release_input() ;
    if (fd != -1) 
    {
        delete sn;
        close(fd);
    }
}

void KindleTS::suspend()
{
    isSuspended = true;
}

void KindleTS::resume()
{
    isSuspended = false;
}

void KindleTS::activity(int)
{
    sn->setEnabled(false);

    input_event in;
    read(fd, &in, sizeof(input_event));

    if ( !isSuspended )
    {
        if (isDebug)
            log_input_event(in);
    
        switch(in.type)
        {
        case EV_ABS:
            switch( in.code )
            {
            case ABS_MT_POSITION_X:
                if ( 0 == slot ) 
                {
                    p.setX(in.value);
                    isTouchActivity = true;
                }
                break;
            case ABS_MT_POSITION_Y:
                if ( 0 == slot )
                {
                    p.setY(in.value);
                    isTouchActivity = true;
                }
                break;
            case ABS_MT_TRACKING_ID:
                if ( 0 == slot ) 
                {
                    isTouchPressed = in.value >= 0;
                    isTouchActivity = true;
                }
                break;
            case ABS_MT_SLOT:
                slot = in.value;
                break;                    
            }
            break ;
        case EV_SYN:
            if ( 0 == slot && isTouchActivity && p.x() >= 0 && p.y() >= 0 )
            {
                isTouchActivity = false;
                mouseChanged(p, isTouchPressed? Qt::LeftButton : 0, 0);
                if ( useSuspendManager )
                    SuspendManager::instanse()->activity();
                if ( isDebug )
                    std::cout << "mouseChanged: x=" << p.x() << ", y=" << p.y() << ", button=" << (isTouchPressed? Qt::LeftButton : 0) << std::endl << std::flush;
                if ( !isTouchPressed ) 
                    p = QPoint(-1,-1);
            }
            break;
        default:
            break;
        }
    }

    sn->setEnabled(true);
}

void KindleTS::capture_input(void)
{
    int on = 1 ;

    if ( !isInputCaptured )
    {
        if (isDebug)
            std::cout << "attempting to capture input..." << std::endl << std::flush;

        if (fd != -1)
        {
            if (ioctl(fd, EVIOCGRAB, on)) {
                std::cerr << "Capture touch input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = true;
    }
}

void KindleTS::release_input(void)
{
    int off = 0 ;

    if ( isInputCaptured )
    {
        if (isDebug)
            std::cout << "attempting to release input..." << std::endl << std::flush;
        
        if (fd != -1)
        {
            if (ioctl(fd, EVIOCGRAB, off)) {
                std::cerr << "Release touch input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = false;
    }
}

void KindleTS::log_input_event(const input_event& e)
{
    const char* type = NULL;
#define EXPAND_TYPE(v) case v: type = #v; break;
    switch ( e.type )
    {
    EXPAND_TYPE(EV_ABS)
    EXPAND_TYPE(EV_SYN)
    EXPAND_TYPE(EV_KEY)
    }
#undef EXPAND_TYPE

    const char* code = NULL;
#define EXPAND_CODE(v) case v: code = #v; break;
    switch ( e.type )
    {
    case EV_ABS:
        {
            switch ( e.code )
            {
                EXPAND_CODE(ABS_X)
                EXPAND_CODE(ABS_Y)
                EXPAND_CODE(ABS_PRESSURE)
                EXPAND_CODE(ABS_MT_SLOT)
                EXPAND_CODE(ABS_MT_POSITION_X)
                EXPAND_CODE(ABS_MT_POSITION_Y)
                EXPAND_CODE(ABS_MT_TRACKING_ID)
            }
       }
       break;
       
    case EV_KEY:
       {
           switch ( e.code )
           {
               EXPAND_CODE(BTN_TOUCH)
           }
       }
       break;

    case EV_SYN:
       {
           switch ( e.code )
           {
               EXPAND_CODE(SYN_REPORT)
           }
       }
       break;
    }
#undef EXPAND_CODE
    if ( EV_SYN == e.type )
        std::cout << " ========> ";
    std::cout << "TS data: type=";
    if ( type != NULL )
        std::cout << type;
    else
        std::cout << e.type;
    std::cout << ", code=";
    if ( code != NULL )
        std::cout << code;
    else
        std::cout << e.code;
    std::cout << ", value=" << e.value;
    std::cout << std::endl << std::flush;
}

