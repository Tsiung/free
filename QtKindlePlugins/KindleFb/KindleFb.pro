QT += core gui
TARGET = KindleFb
TEMPLATE = lib
CONFIG += plugin
SOURCES += KindleFb.cpp screenplugin.cpp
HEADERS += KindleFb.h screenplugin.h
INCLUDEPATH += $$PWD/../../common
LIBS        += -L../../common -lcommon
