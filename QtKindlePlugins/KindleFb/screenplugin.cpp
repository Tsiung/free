#include <iostream>

#include "screenplugin.h"
#include "KindleFb.h"

ScreenPlugin::ScreenPlugin() : QScreenDriverPlugin()
{
    std::cout << "ScreenPlugin::ScreenPlugin()" << std::endl << std::flush;
}

QStringList ScreenPlugin::keys() const
{
    QStringList list;
    list << QLatin1String("KindleFb");
    return list;
}

QScreen* ScreenPlugin::create(const QString& driver, int)
{
    if (driver.toLower() == QLatin1String("kindlefb"))
    {
        return new KindleFb();
    }
    return NULL;
}

Q_EXPORT_PLUGIN2(KindleFb, ScreenPlugin)
