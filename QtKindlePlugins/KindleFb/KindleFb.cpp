#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <QTime>

#include <iostream>

#include "KindleFb.h"

KindleFb::KindleFb()
    : QLinuxFbScreen(0)
    , dev(get_kindle_device())
    , fb(-1)
    , einkFb(dev)
    , isDebug(false)
{
    setSingleShot(true);
    QObject::connect(this, SIGNAL(timeout()), this, SLOT(update()));
}

KindleFb::~KindleFb()
{
}

bool KindleFb::connect(const QString& displaySpec)
{
    const QStringList args = displaySpec.split(QLatin1Char(':'));
    isDebug = einkFb.debug = args.contains("debug");

    fb = einkFb.fbd = open("/dev/fb0", O_RDWR);
    if ( -1 == fb )
    {
        if ( isDebug )
            std::cerr << "FAILED to open framebuffer." << std::endl;
    }

    bool res = QLinuxFbScreen::connect( "" );

    int dpi = 0;
    switch ( get_kindle_device() )
    {
        case KindleTouch:
            dpi = 167;
            break;
        case KindleTouch2:
            dpi = 167;
            break;
        case KindleTouch3:
            dpi = 167;
            break;
        case KindlePaperWhite:
            dpi = 212;
            break;
        case KindlePaperWhite2:
            dpi = 212;
            break; 
        case KindlePaperWhite3:
        case KindlePaperWhite4:
        case KindleVoyage:
        case KindleOasis:
        case KindleOasis2:
            dpi = 300;
            break;
        case KindleOther:
        default:
            dpi = 0;
            break;
    }

    if ( dpi > 0 )
    {
        physWidth  = qRound(w * 25.4 / dpi); // mm
        physHeight = qRound(h * 25.4 / dpi); // mm 
    }

    if ( 8 == depth() && QImage::Format_Invalid == pixelFormat() ) {
        QScreen::setPixelFormat(QImage::Format_Indexed8);
    }

    if ( isDebug ) { 
        std::cout << "Framebuffer info:" << std::endl;
        std::cout << " size: " << width() << " x " << height() << std::endl;
        std::cout << " physical size: " << physicalWidth() << " x " << physicalHeight() << std::endl;
        std::cout << " device size: " << deviceWidth() << " x " << deviceHeight() << std::endl;
        std::cout << " depth: " << depth() << std::endl;
        std::cout << " linestep: " << linestep() << std::endl;
        std::cout << " pixel format: " << pixelFormat() << std::endl;
        std::cout << " grayscale: " << (int)grayscale << std::endl;
    }

    einkFb.width = width();
    einkFb.height = height();

    return res;
}

void KindleFb::setDirty ( const QRect& r )
{
    if ( isDebug )
        std::cout << "KindleFb::setDirty: " 
                  << r.x() << "," 
                  << r.y() << "," 
                  << r.width() << "," 
                  << r.height() 
                  << std::endl;

    updateRect = updateRect.united(r);
    start(10);
}

void KindleFb::update()
{
    if ( -1 != fb && updateRect.width() > 0 && updateRect.height() > 0 )
    {
        bool fullUpdate = updateRect.width() == width() && updateRect.height() == height();
        if ( isDebug ) {
            std::cout << "KoboFb::update(): "
                      << (fullUpdate? "full":"partial") << ","
                      << updateRect.x() << ","
                      << updateRect.y() << ","
                      << updateRect.width() << ","
                      << updateRect.height()
                      << std::endl;
        }
        if ( fullUpdate )
            einkFb.refreshFull(updateRect.x(), updateRect.y(), updateRect.width(), updateRect.height(), false);
        else
            einkFb.refreshUI(updateRect.x(), updateRect.y(), updateRect.width(), updateRect.height(), false);
    }
    updateRect = QRect();
}

QT_END_NAMESPACE
