#include <linux/input.h>
#include <unistd.h>
#include <cstdlib>
#include <fcntl.h>

#include <iostream>

#include <QtCore>
#include <QFile>
#include <QTimer>
#include <QWSServer>

#include "QtUtils.h"
#include "SuspendManager.h"

#include "kindlekeys.h"

#define MAX_PAYLOAD 1024  // maximum payload size

void KindleKeys::capture_input(void)
{
    int on = 1 ;

    if (!isInputCaptured )
    {
        if (isDebug)
            std::cout << "attempting to capture input..." << std::endl << std::flush;

        if (fdKoa2Keys != -1)
        {
            if (ioctl(fdKoa2Keys, EVIOCGRAB, on)) {
                std::cerr << "Capture fdKoa2Keys input: error" << std::endl << std::flush;
            }
        }

        if (fdKvKeys != -1)
        {
            if (ioctl(fdKvKeys, EVIOCGRAB, on)) {
                std::cerr << "Capture fdKvKeys input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = true;
    }
}

void KindleKeys::release_input(void)
{
    int off = 0 ;

    if ( isInputCaptured )
    {
        if ( isDebug )
            std::cout << "attempting to release input..." << std::endl << std::flush;

        if ( fdKoa2Keys != -1 )
        {
            if (ioctl(fdKoa2Keys, EVIOCGRAB, off)) {
                std::cerr << "Release fdKoa2Keys input: error" << std::endl << std::flush;
            }
        }

        if ( fdKvKeys != -1 )
        {
            if (ioctl(fdKvKeys, EVIOCGRAB, off)) {
                std::cerr << "Release fdKvKeys input: error" << std::endl << std::flush;
            }
        }

        isInputCaptured = false;
    }
}

bool KindleKeys::isKoa2Rotated()
{
    bool result = false;

    QString val;
    QFile file( "/sys/class/graphics/fb0/rotate" );
    if( file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream in(&file);
        val = in.readLine().trimmed();
    }

    if ( val == "0" )
        result = false;
    else
    if ( val == "2" )
        result = true;

    return result;
}

KindleKeys::KindleKeys(const QString & driver, const QString & device, QObject* parent) 
    : QObject(parent) 
    , QWSKeyboardHandler(device)
    , dev(get_kindle_device())
    , fdKoa2Keys(-1)
    , snKoa2Keys(NULL)
    , fdKvKeys(-1)
    , snKvKeys(NULL)
    , fdNetLink(-1)
    , snNetLink(NULL)    
    , isDebug(device.contains("debug", Qt::CaseInsensitive))
    , isInputCaptured(false)
    , useSuspendManager(SuspendManager::active())
{
    if (isDebug)
        std::cout << "KindleKeys( " << driver.toLatin1().constData() << "," << device.toLatin1().constData() << ")" << std::endl << std::flush;

    fdNetLink = socket(PF_NETLINK, SOCK_RAW, NETLINK_KOBJECT_UEVENT);
    if ( fdNetLink != -1 )
    {
        memset(&src_addr, 0, sizeof(src_addr));
        src_addr.nl_family = AF_NETLINK;
        src_addr.nl_pid = getpid(); // self pid
        src_addr.nl_groups = 1;     // interested in group 1<<0
        bind(fdNetLink, (struct sockaddr*)&src_addr, sizeof(src_addr));
        memset(&dest_addr, 0, sizeof(dest_addr));
        nlh = (nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD));
        memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
        iov.iov_base = (void*)nlh;
        iov.iov_len = NLMSG_SPACE(MAX_PAYLOAD);
        msg.msg_name = (void*)&dest_addr;
        msg.msg_namelen = sizeof(dest_addr);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        snNetLink = new QSocketNotifier(fdNetLink, QSocketNotifier::Read);
        connect(snNetLink, SIGNAL(activated(int)), this, SLOT(activityNetLink(int)));
        snNetLink->setEnabled(true);
    }
    else
    {
        std::cerr << "fdNetLink open FAILED" << std::endl << std::flush;
    }

    if ( KindleOasis2 == dev )
    {
        fdKoa2Keys = open("/dev/input/event4", O_RDONLY);
        if ( fdKoa2Keys != -1 )
        {
            snKoa2Keys = new QSocketNotifier(fdKoa2Keys, QSocketNotifier::Read);
            connect(snKoa2Keys, SIGNAL(activated(int)), this, SLOT(activityKoa2Keys(int)));
            snKoa2Keys->setEnabled(true);
        }
        else
        {
            std::cerr << "fdKoa2Cover open FAILED" << std::endl << std::flush;
        }
    }

    if ( KindleVoyage == dev )
    {
        fdKvKeys = open("/dev/input/event2", O_RDONLY);
        if ( fdKvKeys != -1 )
        {
            snKvKeys = new QSocketNotifier(fdKvKeys, QSocketNotifier::Read);
            connect(snKvKeys, SIGNAL(activated(int)), this, SLOT(activityKvKeys(int)));
            snKvKeys->setEnabled(true);
        }
        else
        {
            std::cerr << "fdKvKeys open FAILED" << std::endl << std::flush;
        }
    }

    isInputCaptured = false ;

    capture_input();
}

KindleKeys::~KindleKeys()
{
    if ( fdKoa2Keys != -1 )
    {
        delete snKoa2Keys;
        close(fdKoa2Keys);
    }
    if ( fdKvKeys != -1 )
    {
        delete snKvKeys;
        close(fdKvKeys);
    }
    if ( fdNetLink != -1 )
    {
        delete snNetLink;
        close(fdNetLink);
        free(nlh);
    }
}

void KindleKeys::activityKoa2Keys(int fd)
{
    input_event in;
    read(fd, &in, sizeof(input_event));
    if ( 1 == in.type )
    {
        if ( isDebug )
            std::cout << "Keyboard: type " << in.type << ", code " << in.code << ", value " << in.value << std::endl << std::flush;
 
        bool rot = isKoa2Rotated();
        switch ( in.code )
        {
        case 109: // PageDown
            processKeyEvent(0, rot? Qt::Key_PageUp:Qt::Key_PageDown, Qt::NoModifier, in.value != 0, false);
            if ( useSuspendManager )
                SuspendManager::instanse()->activity();
            break;
        case 104: // PageUp
            processKeyEvent(0, rot? Qt::Key_PageDown:Qt::Key_PageUp, Qt::NoModifier, in.value != 0, false);
            if ( useSuspendManager )
                SuspendManager::instanse()->activity();
            break;
        }
    }
}

void KindleKeys::activityKvKeys(int fd)
{
    snKvKeys->setEnabled(false);

    input_event in;

    read(fdKvKeys, &in, sizeof(input_event));

    if ( 1 == in.type )
    {
        if ( isDebug )
            std::cout << "Keyboard: type " << in.type << ", code " << in.code << ", value " << in.value << std::endl << std::flush;
    
        switch ( in.code )
        {
        case 109: // PageDown
            processKeyEvent(0, Qt::Key_PageDown, Qt::NoModifier, in.value != 0, 2 == in.value );
            if ( useSuspendManager )
                SuspendManager::instanse()->activity();
            break;
        case 104: // PageUp
            processKeyEvent(0, Qt::Key_PageUp, Qt::NoModifier, in.value != 0, 2 == in.value );
            if ( useSuspendManager )
                SuspendManager::instanse()->activity();
            break;
        }
    }

    snKvKeys->setEnabled(true);
}

void KindleKeys::activityNetLink(int)
{
    snNetLink->setEnabled(false);
    recvmsg(fdNetLink, &msg, 0);
    QString text((const char*)(nlh));
    if ( KindleTouch == dev || KindlePaperWhite == dev )
    {
        if ( text == "online@/devices/virtual/misc/yoshibutton" )
        {
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, true, false);
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, false, false);
        }
        else
        if (    text == "online@/devices/virtual/misc/hall"
             || text == "online@/devices/virtual/misc/yoshime_hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, true, false);
        }
        else
        if (     text == "offline@/devices/virtual/misc/hall"
              || text == "offline@/devices/virtual/misc/yoshime_hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, false, false);
        }
    }
    else
    if (    KindleTouch2 == dev
         || KindlePaperWhite2 == dev
         || KindlePaperWhite3 == dev
         || KindlePaperWhite4 == dev
         || KindleVoyage == dev )
    {
        if ( text == "online@/devices/platform/imx-i2c.0/i2c-0/0-003c/max77696-onkey.0" )
        {
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, true, false);
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, false, false);
        }
        else
        if ( text == "online@/devices/virtual/misc/hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, true, false);
        }
        else
        if ( text == "offline@/devices/virtual/misc/hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, false, false);
        }
    }
    else
    if ( KindleTouch3 == dev )
    {
        if ( text == "online@/devices/soc0/soc.2/2100000.aips-bus/21a0000.i2c/i2c-0/0-004b" )
        {
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, true, false);
            processKeyEvent(0, Qt::Key_PowerDown, Qt::NoModifier, false, false);
        }
        else
        if ( text == "online@/devices/virtual/misc/hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, true, false);
        }
        else
        if ( text == "offline@/devices/virtual/misc/hall" )
        {
            processKeyEvent(0, Qt::Key_PowerOff, Qt::NoModifier, false, false);
        }
    }

    if ( isDebug )
    {
        std::cout << "NETLINK: " << text.toLatin1().constData() << std::endl << std::flush;
    }

    snNetLink->setEnabled(true);
}
