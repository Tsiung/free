#ifndef WIDGET_H
#define WIDGET_H

#if !defined(DESKTOP)
#include "VirtualKeyboardContainer.h"
#endif

#include "gesturescontroller.h"
#include "WidgetCommon.h"
#include "WidgetEventFilter.h"

namespace Ui {
class Widget;
}

class Widget : public WidgetCommon
{
    Q_OBJECT
    Ui::Widget *ui;
    
public:
    Widget();
    ~Widget();

private slots:
    void on_btnExit_clicked();
    void on_btnSettings_clicked();
    void on_lineEdit_returnPressed();
};

#endif // WIDGET_H
