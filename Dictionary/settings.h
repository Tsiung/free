#ifndef SETTINGS_H
#define SETTINGS_H

#include "Dialog.h"

namespace Ui {
class Settings;
}

class Settings : public Dialog
{
    Q_OBJECT

public:
    Settings(QWidget *parent = 0);
    ~Settings();

    void accept();

private slots:
    void on_btnDictPath_clicked();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
