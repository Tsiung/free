#include <QDebug>

#include "Config.h"
#include "Platform.h"
#include "DirSelectDialog.h"
#include "DictionaryWidget.h"

#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::Settings)
{
    ui->setupUi(this);
    init();

    ui->lblDictPath->setElideMode(Qt::ElideMiddle);
    ui->lblDictPath->setText(DictionaryWidget::get_dict_data_path());
}

Settings::~Settings()
{
    delete ui;
}

void Settings::accept()
{
    DictionaryWidget::set_dict_data_path(ui->lblDictPath->text());
    Dialog::accept();
}

void Settings::on_btnDictPath_clicked()
{
    DirSelectDialog dlg(this, tr("Dictionaries path"), DictionaryWidget::get_dict_data_path(), true);
    if ( dlg.exec() )
    {
        ui->lblDictPath->setText(dlg.getPath());
    }
}
